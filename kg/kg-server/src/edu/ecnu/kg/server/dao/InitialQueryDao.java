package edu.ecnu.kg.server.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.ecnu.kg.common.SimpleNode;
import edu.ecnu.kg.server.DataSourceManager;
import edu.ecnu.kg.server.SearchTokens;
import edu.ecnu.kg.server.model.QueryResult;
import edu.ecnu.kg.tools.db.EmbeddedCypherEngine;

public class InitialQueryDao {
  
  
  /**
   * 获取查询候选的列表
   * @param query
   * @return
   * @throws SQLException 
   */
  public ArrayList<QueryResult> parseInitialQuery(String query) {
    ArrayList<QueryResult> results = new ArrayList<QueryResult>();
    
    //删除后缀
    for(String stop : SearchTokens.QUERY_STOPWORDS){
      query = query.replace(stop, "");
    }
    
    System.out.println(query);
    
    DataSourceManager dsm = DataSourceManager.getInstance();
    EmbeddedCypherEngine engine = new EmbeddedCypherEngine(dsm.getGraphDb(), dsm.getExcutionEngine());
    
    for(String label : SearchTokens.SEARCH_LABELS){
      String key[] = SearchTokens.NODE_PROPERTY_KEYS.get(label);
      
      if(key.length<=0){
        System.out.println("Search labels:"+label+" search keys NULL!!!");
        return results;
      }
      
      List<SimpleNode> nodes=(List<SimpleNode>) engine.similarSimpleNodesbyLabelandProperties(label, key, query, 10);
      for(SimpleNode node : nodes){
        String datasource = node.getProperty(SearchTokens.DATASOURCE_TOKEN);
        if(datasource == null || "".equals(datasource.trim())){
          datasource="News";
        }
        QueryResult result = new QueryResult(node.getProperty(key[0]).toString(),label,key[0],datasource);
        results.add(result);
      }
    }
    
    return results;
  }

   

}
