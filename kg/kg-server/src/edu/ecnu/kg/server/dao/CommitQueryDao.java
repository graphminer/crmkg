package edu.ecnu.kg.server.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;

import edu.ecnu.kg.server.DataSourceManager;
import edu.ecnu.kg.server.SearchTokens;
import edu.ecnu.kg.server.model.Category;
import edu.ecnu.kg.server.model.Link;
import edu.ecnu.kg.server.model.QueryResult;
import edu.ecnu.kg.server.model.ResultGraph;
import edu.ecnu.kg.server.model.Vertex;

public class CommitQueryDao {
  
  public void commitQueryLog(ArrayList<QueryResult> results){
    
  }

  /**
   * 广度优先获取所需图结构
   * 由于限制条件较多，没有使用Neo4j Traversal
   * @param results
   * @return
   * @throws InterruptedException
   */
  public ResultGraph commitQueryCorps(ArrayList<QueryResult> results) throws InterruptedException{
    //Split param into corporations
    ResultGraph rg = new ResultGraph();
    
    rg.addAll(getResultGraph(results, 80, 20, 2));
        
    return rg;
  }
  
  /*
   * Specific Queries
   */
  
  public ResultGraph getResultGraph(List<QueryResult> results, int maxnum, int maxnuminlevel, int maxlevel) throws InterruptedException{
    GraphDatabaseService graphDb = DataSourceManager.getInstance().getGraphDb();
    
    HashMap<String, Category> categorymap=new HashMap<String, Category>();
    HashMap<String, Vertex> vertexmap = new HashMap<String, Vertex>();
    HashMap<String, Link> linkmap = new HashMap<String, Link>();
    
    LinkedBlockingQueue<Node> nodequeue = new LinkedBlockingQueue<Node>();
    LinkedBlockingQueue<Relationship> relationqueue = new LinkedBlockingQueue<Relationship>();
    
    
    //Breadth first traverse
    try(Transaction tx = graphDb.beginTx()){
      for(QueryResult result : results){
 
        String key = result.getName();
        String label = result.getCategory();
        String property = result.getProperty();
        Node firstNode = null;
        
        try(ResourceIterator<Node> nodes = 
            graphDb.findNodesByLabelAndProperty
            (DynamicLabel.label(label), property, key).iterator()){
          if(nodes.hasNext()){
            firstNode = nodes.next();
            nodequeue.put(firstNode);
            
        }
      }
    }
      
       System.out.println("Initial Node Queue Size:"+nodequeue.size());
      
      BFSinsert(nodequeue, relationqueue,categorymap, vertexmap, linkmap, 0,0,maxnum,maxnuminlevel,maxlevel);
      
      
      tx.success();
    }

    ResultGraph rg=new ResultGraph();
    rg.getCategories().addAll(categorymap.values());
    rg.getNodelist().addAll(vertexmap.values());
    rg.getLinklist().addAll(linkmap.values());
    
    return rg;
  }
  
  /**
   * Breadth first traverse
   * @param nodequeue bfs插入-遍历node queue
   * @param relationqueue  bfs插入-遍历relation queue
   * @param categorymap category对应的index map
   * @param vertexmap vertex对应的index map
   * @param linkmap link对应的index map
   * @param totalcount 总共获取的节点数
   * @param level 目前遍历的深度
   * @param maxnum 节点数threshold
   * @param maxnuminlevel 除了根节点以外每个结点最多的孩子结点个数
   * @param maxlevel 返回最多的层数
   * @throws InterruptedException
   */
  private void BFSinsert(LinkedBlockingQueue<Node> nodequeue,
      LinkedBlockingQueue<Relationship> relationqueue, HashMap<String, Category> categorymap, HashMap<String, Vertex> vertexmap,
      HashMap<String, Link> linkmap, int totalcount, int level, int maxnum, int maxnuminlevel, int maxlevel) throws InterruptedException {
    
    LinkedBlockingQueue<Node> tempQueue = new LinkedBlockingQueue<Node>();
    LinkedBlockingQueue<Relationship> tempRel = new LinkedBlockingQueue<Relationship>();
    
    while(!nodequeue.isEmpty()){
      Node node=nodequeue.poll();
      String label =null;
      Iterator<Label> labels=node.getLabels().iterator();
      if(labels.hasNext()){
        label=labels.next().name();
      }
      
      //add category entry
      Category cate = categorymap.get(label);
      if( cate == null){
        cate=new Category();
        cate.setId(categorymap.size());
        cate.setName(label);
        categorymap.put(label, cate);
      }
      
      if(!vertexmap.containsKey(String.valueOf(node.getId()))){  
        Vertex vertex = convertFrom(node,cate.getId(),level,vertexmap.size());
        vertexmap.put(String.valueOf(vertex.getId()), vertex);
        totalcount++;
        
        if(totalcount>maxnum){
          break;
        }
        
        System.out.println("put node:"+node.getId()+" Level:"+level);
      }
      
      //add to node queue
      int childnum=0;
      if(node.getDegree()<10000 || level==0){
        Iterable<Relationship> relations = node.getRelationships();

        for(Relationship rel : relations){
          childnum++;
          if(childnum<maxnuminlevel){
            tempRel.put(rel);

            //判断已有结点
            if(vertexmap.containsKey(String.valueOf(rel.getEndNode().getId()))){
              tempQueue.put(rel.getStartNode());
            }
            else{
              tempQueue.put(rel.getEndNode());
            }
          }
          else{
            break;
          }
        }
      }

    }
    
    while(!relationqueue.isEmpty()){
      Relationship rel = relationqueue.poll();
      String startID=String.valueOf(rel.getStartNode().getId());
      String endID=String.valueOf(rel.getEndNode().getId());
      if(!vertexmap.containsKey(startID) || !vertexmap.containsKey(endID)){
        continue;
      }
      int start = vertexmap.get(startID).getIndex();
      int end = vertexmap.get(endID).getIndex();
      
      Link link = linkmap.get(String.valueOf(rel.getId()));
      if(link == null){
        Map<String,String> properties=new HashMap<String,String>();
        for(String key: rel.getPropertyKeys()){
          properties.put(key, rel.getProperty(key).toString());    
        }
        int linkindex = linkmap.size();
        String relID=String.valueOf(rel.getId());
        link=new Link(linkindex,relID , 
        String.valueOf(rel.getType().name()), start, end,properties);
        //link = convertFrom(rel,start,end,linkindex);  !!Neo4j的Node和Relationship太表象了，各种bug
        linkmap.put(relID, link);
      }       
    }
    
    //check
    level++;
    if(totalcount>maxnum || level>maxlevel){
      return;
    }
    
    relationqueue.clear();
    nodequeue.clear();
    
    BFSinsert(tempQueue, tempRel,categorymap, vertexmap, linkmap, totalcount,level,maxnum,maxnuminlevel/(level+1),maxlevel);
    
  }

//  @SuppressWarnings("unused")
//  private Traverser findNeighbours( final Node startNode, int depth)
//  {
//      TraversalDescription td = graphDb.traversalDescription()
//              .breadthFirst()
//              .evaluator(Evaluators.toDepth(depth));
//      return td.traverse( startNode );
//  }
//  
  public Vertex convertFrom(Node firstNode,int cateID,int level,int index){
    String id=String.valueOf(firstNode.getId());

    Map<String,String> properties=new HashMap<String,String>();
    for(String key : firstNode.getPropertyKeys()){
      String replacekey=SearchTokens.NODE_KEYS.get(key);
      if(replacekey ==null || "".equals(replacekey)){
        replacekey=key;
      }
      properties.put(replacekey, firstNode.getProperty(key).toString());
    }   
    
   return new Vertex(index, id, cateID,properties,level);
  }
  
  public Link convertFrom(Relationship relation, int start, int end, int index){
    String id=String.valueOf(relation.getId());
    String label=String.valueOf(relation.getType().name());
    Link link = new Link(index,id,start,end,label);
    
    for(String key : relation.getPropertyKeys()){
      link.addProperty(key, relation.getProperty(key).toString());
    }
    return link;
  }
  
  

}
