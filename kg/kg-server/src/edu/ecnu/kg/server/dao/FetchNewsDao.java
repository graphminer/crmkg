package edu.ecnu.kg.server.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import edu.ecnu.kg.server.DataSourceManager;
import edu.ecnu.kg.server.Queries;
import edu.ecnu.kg.server.model.News;

public class FetchNewsDao {
  
  public News fetchNewsByID(String newsID) throws SQLException{
    Connection rdb = DataSourceManager.getInstance().getRDBConnection();
    PreparedStatement ps = rdb.prepareStatement(Queries.FETCH_NEWS+" where id="+newsID);
    ResultSet rs = ps.executeQuery();
    
    News news = null;
    
    if(rs.next()){
      news=new News(rs.getString("id"),
        rs.getString("url"),rs.getString("title"),rs.getString("time"),rs.getString("media"));      
    }
    
    return news;
  }

}
