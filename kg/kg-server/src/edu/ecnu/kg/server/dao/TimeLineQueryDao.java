package edu.ecnu.kg.server.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.ecnu.kg.server.DataSourceManager;
import edu.ecnu.kg.server.model.BatchEventSeq;
import edu.ecnu.kg.server.model.BigEventRequest;
import edu.ecnu.kg.server.model.SubEvent;

public class TimeLineQueryDao {
  DataSourceManager DSM=DataSourceManager.getInstance();
  Connection conn =null;
  
  public List<String> queryCompanyList(String keywords){
    List<String> result = new ArrayList<String>();
    try {
      conn = DSM.getRDBConnection();
      String sql =
          "select companyName from timeline1 where companyName like '%"+keywords+"%' ;";
      PreparedStatement pstmt=conn.prepareStatement(sql);
      ResultSet resultSet = null;
      resultSet = pstmt.executeQuery();
      while (resultSet.next()) {
        String conmpanyName=resultSet.getString("companyName");
        result.add(conmpanyName);
//        keyWords.add(resultSet.getString("eventKeyWords"));
      }
      pstmt.close();
      conn.close();
    } catch (SQLException e) {
      
      
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return result;
  }
  public BatchEventSeq queryBatchEventSeq(String keywords){
    BatchEventSeq batchEventSeq=new BatchEventSeq();
    try {
      conn = DSM.getRDBConnection();
      String sql =
          "select * from bigevent1 where timeLineId =(select id from timeline1 where companyName='"+keywords+"') group by startTime order by startTime asc;";
      PreparedStatement pstmt=conn.prepareStatement(sql);
      ResultSet resultSet = null;
      resultSet = pstmt.executeQuery();
      List<BigEventRequest> bigEventRequests=new ArrayList<BigEventRequest>();
      while (resultSet.next()) {
        BigEventRequest beRequest =new BigEventRequest();
        beRequest.setContent(resultSet.getString("eventKeyWords"));
        beRequest.setNewsCount(Integer.valueOf(resultSet.getString("newsCount")));
        beRequest.setStartTime(resultSet.getString("startTime"));
        String bigEventId=resultSet.getString("id");
        beRequest.setId(bigEventId);
        String subSql="select * from subevent1 where bigEventId ="+bigEventId+" group by newsTime order by newsTime asc;";
        PreparedStatement pstmt1=conn.prepareStatement(subSql);
        ResultSet resultSet1 = null;
        resultSet1 = pstmt1.executeQuery();
        List<SubEvent> seqEvent=new ArrayList<SubEvent>();
        while(resultSet1.next()){
          SubEvent subEvent =new SubEvent();
          subEvent.setNewsTitle(resultSet1.getString("newsTitle"));
          subEvent.setNewsUrl(resultSet1.getString("newsUrl"));
          subEvent.setNewsMedia(resultSet1.getString("newsMedia"));
          subEvent.setNewsTime(resultSet1.getString("newsTime"));
          subEvent.setNewsSummary(resultSet1.getString("newsSummary"));
          subEvent.setBigEventId(Integer.valueOf(resultSet1.getString("bigEventId")));
          subEvent.setNewsCount(Integer.valueOf(resultSet1.getString("newsCount")));
          seqEvent.add(subEvent);
        }
        beRequest.setSeqEvent(seqEvent);
        bigEventRequests.add(beRequest);
        pstmt1.close();
      }
      batchEventSeq.setBatchEvents(bigEventRequests);
      pstmt.close();
      conn.close();
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return batchEventSeq;
  }
  
  public List<BigEventRequest> queryBigEventKeyWord(String keywords){
    List<BigEventRequest> keyWords = new ArrayList<BigEventRequest>();
    try {
      conn = DSM.getRDBConnection();
      String sql =
          "select id,eventKeyWords,startTime, newsCount from bigevent1 where timeLineId =(select id from timeline1 where companyName='"
              + keywords + "')  group by startTime order by startTime asc ;";
      PreparedStatement pstmt=conn.prepareStatement(sql);
      ResultSet resultSet = null;
      resultSet = pstmt.executeQuery();
      
      while (resultSet.next()) {
        BigEventRequest ber=new BigEventRequest();
        ber.setId(resultSet.getString("id"));
        ber.setContent(resultSet.getString("eventKeyWords"));
        String time=resultSet.getString("startTime");
        String year=time.substring(0, 4);
        String month=time.substring(4, 6);
        String day=time.substring(6, 8);
        int newsCount=Integer.valueOf(resultSet.getString("newsCount"));
        ber.setStartTime(year+","+month+","+day); 
        ber.setNewsCount(newsCount);
        keyWords.add(ber);
      }
      pstmt.close();
      conn.close();
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return keyWords;
  }
  
  public BigEventRequest queryEventSeqById(String bigEventId){
    try {
      conn =DSM.getRDBConnection();
    } catch (SQLException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    BigEventRequest bigEventReq =new BigEventRequest();
    String sql="select * from subevent1 where bigEventId ="+bigEventId+";";
    List<SubEvent> subEvents=new ArrayList<SubEvent>();
    try {
      PreparedStatement pstmt=conn.prepareStatement(sql);
      ResultSet resultSet = null;
      resultSet = pstmt.executeQuery();
      while(resultSet.next()){
          SubEvent subEvent=new SubEvent();
          subEvent.setNewsTitle(resultSet.getString("newsTitle"));
          subEvent.setNewsUrl(resultSet.getString("newsUrl"));
          subEvent.setNewsMedia(resultSet.getString("newsMedia"));
          subEvent.setNewsTime(resultSet.getString("newsTime"));
          subEvent.setNewsSummary(resultSet.getString("newsSummary"));
          subEvent.setBigEventId(Integer.valueOf(resultSet.getString("bigEventId")));
          subEvent.setNewsCount(Integer.valueOf(resultSet.getString("newsCount")));
          subEvents.add(subEvent);
      }
      
      sql="select * from bigevent1 where id ="+bigEventId+";";
      
      pstmt=conn.prepareStatement(sql);
      resultSet = null;
      resultSet = pstmt.executeQuery();
      while(resultSet.next()){
        bigEventReq.setContent(resultSet.getString("eventKeyWords"));
        bigEventReq.setId(bigEventId);
        bigEventReq.setNewsCount(Integer.valueOf(resultSet.getString("newsCount")));
        String time=resultSet.getString("startTime");
        String year=time.substring(0, 4);
        String month=time.substring(4, 6);
        String day=time.substring(6, 8);
        bigEventReq.setStartTime(year+","+month+","+day);
      }
      pstmt.close();
      conn.close();
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    bigEventReq.setSeqEvent(subEvents);
    return bigEventReq;
  }
  
  
  public ResultSet excuteQurey(String sql){
    try {
      conn =DSM.getRDBConnection();
    } catch (SQLException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    StringBuffer sbsql=new StringBuffer();
    sbsql.append(sql);
    PreparedStatement pstmt=null;
    try {
      pstmt = conn.prepareStatement(sbsql.toString());
      ResultSet resultSet = pstmt.executeQuery(sql);
      pstmt.close();
      conn.close();
      return resultSet;
  } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
  }
    return null;
  }
}
