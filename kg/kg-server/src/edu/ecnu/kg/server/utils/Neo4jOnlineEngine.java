package edu.ecnu.kg.server.utils;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;

public class Neo4jOnlineEngine {
  
  private static final Neo4jOnlineEngine instance = new Neo4jOnlineEngine();
  
  private static GraphDatabaseService graphDb;
  private static ExecutionEngine engine;

  private Neo4jOnlineEngine() {
    init();
  }
  
  private synchronized void init() {
    // 
    if(graphDb == null){
      openEmbededDb();
    }
    if(engine == null){
      //engine = new ExecutionEngine(graphDb, StringLogger.lazyLogger(new File("testlog")));
      engine = new ExecutionEngine(graphDb);
    }
    
  }

  private void openEmbededDb() {
    //
    graphDb =
        new GraphDatabaseFactory().newEmbeddedDatabaseBuilder("/Users/leyi/Projects/pingan/testgraph.db")
        .setConfig( GraphDatabaseSettings.node_keys_indexable, "product,person" ).
        setConfig( GraphDatabaseSettings.relationship_keys_indexable, "organization,area" ).
        setConfig( GraphDatabaseSettings.node_auto_indexing, "true" ).
        setConfig( GraphDatabaseSettings.relationship_auto_indexing, "true" ).
        setConfig(GraphDatabaseSettings.read_only, "false")
        .setConfig(GraphDatabaseSettings.all_stores_total_mapped_memory_size, "10240")
        .newGraphDatabase();
    registerShutdownHook(graphDb);
    
//    IndexDefinition indexDefinition;
//    try ( Transaction tx = graphDb.beginTx() )
//    {
//        Schema schema = graphDb.schema();
//        indexDefinition = schema.indexFor( DynamicLabel.label( "COMPANY" ) )
//                .on( "companyname" )
//                .create();
//        tx.success();
//    }
//    
//    try ( Transaction tx = graphDb.beginTx() )
//    {
//        Schema schema = graphDb.schema();
//        schema.awaitIndexOnline( indexDefinition, 10, TimeUnit.SECONDS );
//    }
    
  }
  
  private static void registerShutdownHook(final GraphDatabaseService graphDb) {
    Runtime.getRuntime().addShutdownHook(new Thread() {
      public void run() {
        graphDb.shutdown();
      }
    });
  }


  public static Neo4jOnlineEngine getInstance(){
    return instance;
  }

  /**
   * 取得graphDb
   * @return 返回 graphDb。
   */
  public static GraphDatabaseService getGraphDb() {
    return graphDb;
  }

  /**
   * 设置graphDb
   * @param graphDb 要设置的 graphDb。
   */
  public static void setGraphDb(GraphDatabaseService graphDb) {
    Neo4jOnlineEngine.graphDb = graphDb;
  }

  /**
   * 取得engine
   * @return 返回 engine。
   */
  public static ExecutionEngine getEngine() {
    return engine;
  }

  /**
   * 设置engine
   * @param engine 要设置的 engine。
   */
  public static void setEngine(ExecutionEngine engine) {
    Neo4jOnlineEngine.engine = engine;
  }

  

}
