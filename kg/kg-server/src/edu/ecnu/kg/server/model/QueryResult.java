package edu.ecnu.kg.server.model;

public class QueryResult {

  String name;

  String category;

  String property;

  String datasource;

  boolean isSelectable;

  /**
   * 取得name
   * 
   * @return 返回 name。
   */
  public String getName() {
    return name;
  }

  /**
   * 设置name
   * 
   * @param name 要设置的 name。
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * 取得category
   * 
   * @return 返回 category。
   */
  public String getCategory() {
    return category;
  }

  /**
   * 设置category
   * 
   * @param category 要设置的 category。
   */
  public void setCategory(String category) {
    this.category = category;
  }

  /**
   * 取得datasource
   * 
   * @return 返回 datasource。
   */
  public String getDatasource() {
    return datasource;
  }

  /**
   * 设置datasource
   * 
   * @param datasource 要设置的 datasource。
   */
  public void setDatasource(String datasource) {
    this.datasource = datasource;
  }

  /**
   * 取得property
   * 
   * @return 返回 property。
   */
  public String getProperty() {
    return property;
  }

  /**
   * 设置property
   * 
   * @param property 要设置的 property。
   */
  public void setProperty(String property) {
    this.property = property;
  }

  /**
   * 取得isSelectable
   * 
   * @return 返回 isSelectable。
   */
  public boolean isSelectable() {
    return isSelectable;
  }

  /**
   * 设置isSelectable
   * 
   * @param isSelectable 要设置的 isSelectable。
   */
  public void setSelectable(boolean isSelectable) {
    this.isSelectable = isSelectable;
  }

  public QueryResult(String name, String category, String property, String datasource,
      boolean isSelectable) {
    super();
    this.name = name;
    this.category = category;
    this.property = property;
    this.datasource = datasource;
    this.isSelectable = isSelectable;
  }

  public QueryResult(String name, String category, String property, String datasource) {
    super();
    this.name = name;
    this.category = category;
    this.property = property;
    this.datasource = datasource;
    if ("News".equals(datasource)) {
      this.isSelectable = true;
    } else {
      this.isSelectable = false;
    }
  }

  @Override
  public String toString() {
    return "QueryResult [name=" + name + ", category=" + category + ", property=" + property
        + ", datasource=" + datasource + ", isSelectable=" + isSelectable + "]";
  }

  
  
}
