package edu.ecnu.kg.server.model;

import java.util.ArrayList;

public class ResultGraph {
  
  ArrayList<Category> categories = new ArrayList<Category>();
  
  ArrayList<Vertex> nodelist = new ArrayList<Vertex>();
  
  ArrayList<Link> linklist = new ArrayList<Link>();
  
  public void addAll(ResultGraph rg2){
    nodelist.addAll(rg2.getNodelist());
    linklist.addAll(rg2.getLinklist());
    categories.addAll(rg2.getCategories());
  }
  
  public void addNode(Vertex node){
    nodelist.add(node);
  }
  
  public void addLink(Link link){
    linklist.add(link);
  }

  /**
   * 取得nodelist
   * @return 返回 nodelist。
   */
  public ArrayList<Vertex> getNodelist() {
    return nodelist;
  }

  /**
   * 设置nodelist
   * @param nodelist 要设置的 nodelist。
   */
  public void setNodelist(ArrayList<Vertex> nodelist) {
    this.nodelist = nodelist;
  }

  /**
   * 取得linklist
   * @return 返回 linklist。
   */
  public ArrayList<Link> getLinklist() {
    return linklist;
  }

  /**
   * 设置linklist
   * @param linklist 要设置的 linklist。
   */
  public void setLinklist(ArrayList<Link> linklist) {
    this.linklist = linklist;
  }

  /**
   * 取得categories
   * @return 返回 categories。
   */
  public ArrayList<Category> getCategories() {
    return categories;
  }

  /**
   * 设置categories
   * @param categories 要设置的 categories。
   */
  public void setCategories(ArrayList<Category> categories) {
    this.categories = categories;
  }
  
  

}
