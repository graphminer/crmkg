package edu.ecnu.kg.server.model;


public class SubEvent {
  
  String newsTitle="";
  String newsUrl="";
  String newsMedia="";
  String newsTime="";
  public String getNewsTitle() {
    return newsTitle;
  }
  public void setNewsTitle(String newsTitle) {
    this.newsTitle = newsTitle;
  }
  public String getNewsUrl() {
    return newsUrl;
  }
  public void setNewsUrl(String newsUrl) {
    this.newsUrl = newsUrl;
  }
  public String getNewsMedia() {
    return newsMedia;
  }
  public void setNewsMedia(String newsMedia) {
    this.newsMedia = newsMedia;
  }
  public String getNewsTime() {
    return newsTime;
  }
  public void setNewsTime(String newsTime) {
    this.newsTime = newsTime;
  }
  public String getNewsSummary() {
    return newsSummary;
  }
  public void setNewsSummary(String newsSummary) {
    this.newsSummary = newsSummary;
  }
  public int getBigEventId() {
    return bigEventId;
  }
  public void setBigEventId(int bigEventId) {
    this.bigEventId = bigEventId;
  }
  public int getNewsCount() {
    return newsCount;
  }
  public void setNewsCount(int newsCount) {
    this.newsCount = newsCount;
  }
  String newsSummary="";
  int bigEventId=0;
  int newsCount=0;
  
}
