package edu.ecnu.kg.server.model;

public class EventAsset {
  String media;
  String credit;
  String caption;
  public String getMedia() {
    return media;
  }
  public void setMedia(String media) {
    this.media = media;
  }
  public String getCredit() {
    return credit;
  }
  public void setCredit(String credit) {
    this.credit = credit;
  }
  public String getCaption() {
    return caption;
  }
  public void setCaption(String caption) {
    this.caption = caption;
  }
  
}
