package edu.ecnu.kg.server.model;

import java.util.HashMap;
import java.util.Map;

public class Vertex {
  
  int index;
  String id;
  int category;
  Map<String,String> properties;
  int level;
  
  /**
   * 取得level
   * @return 返回 level。
   */
  public int getLevel() {
    return level;
  }


  /**
   * 设置level
   * @param level 要设置的 level。
   */
  public void setLevel(int level) {
    this.level = level;
  }


  public Vertex(){
    properties = new HashMap<String,String>();
  }
  
  
  public void addProperty(String key, String value){
    properties.put(key, value);
  }
  
  /**
   * 取得id
   * @return 返回 id。
   */
  public String getId() {
    return id;
  }
  /**
   * 设置id
   * @param id 要设置的 id。
   */
  public void setId(String id) {
    this.id = id;
  }
  /**
   * 取得category
   * @return 返回 category。
   */
  public int getCategory() {
    return category;
  }
  /**
   * 设置category
   * @param category 要设置的 category。
   */
  public void setCategory(int category) {
    this.category = category;
  }

  /**
   * 取得properties
   * @return 返回 properties。
   */
  public Map<String, String> getProperties() {
    return properties;
  }
  /**
   * 设置properties
   * @param properties 要设置的 properties。
   */
  public void setProperties(Map<String, String> properties) {
    this.properties = properties;
  }

  public Vertex(String id, int category, Map<String, String> properties, int level2) {
    super();
    this.id = id;
    this.category = category;
    this.properties = properties;
    this.level=level2;
  }


  /**
   * 取得index
   * @return 返回 index。
   */
  public int getIndex() {
    return index;
  }


  /**
   * 设置index
   * @param index 要设置的 index。
   */
  public void setIndex(int index) {
    this.index = index;
  }


  public Vertex(int index, String id, int category, Map<String, String> properties, int level) {
    super();
    this.index = index;
    this.id = id;
    this.category = category;
    this.properties = properties;
    this.level = level;
  }
  
  
  
//  public void setVertex(Node neonode, boolean center){
//    this.id = String.valueOf(neonode.getId());
//    
//    for(Label label : neonode.getLabels()){
//      this.category = label.name();
//      break;
//    }
//    
//    this.isCenter=center;
//    for(Relationship relation : neonode.getRelationships()){
//      this.linkes.add(String.valueOf(relation.getId()));
//    }
//    Iterable<String> keys=neonode.getPropertyKeys();
//    for(String key : keys){
//      this.properties.put(key, neonode.getProperty(key).toString());
//    }
//  }
//  

}
