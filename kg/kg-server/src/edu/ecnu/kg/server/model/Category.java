package edu.ecnu.kg.server.model;

public class Category {
  
  String name;
  int id;
  
  
  /**
   * 取得name
   * @return 返回 name。
   */
  public String getName() {
    return name;
  }
  /**
   * 设置name
   * @param name 要设置的 name。
   */
  public void setName(String name) {
    this.name = name;
  }
  /**
   * 取得id
   * @return 返回 id。
   */
  public int getId() {
    return id;
  }
  /**
   * 设置id
   * @param id 要设置的 id。
   */
  public void setId(int id) {
    this.id = id;
  }
  
  

}
