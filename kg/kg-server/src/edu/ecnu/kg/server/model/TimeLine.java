package edu.ecnu.kg.server.model;

import java.util.ArrayList;
import java.util.List;


public class TimeLine {
  String query="";    //公司ID
  List<BigEvent> events=new ArrayList<BigEvent>();   //事件名称列表
  public String getQuery() {
    return query;
  }
  public void setQuery(String query) {
    this.query = query;
  }
  public List<BigEvent> getEvents() {
    return events;
  }
  public void setEvents(List<BigEvent> events) {
    this.events = events;
  }

}
