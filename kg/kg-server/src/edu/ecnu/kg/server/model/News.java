package edu.ecnu.kg.server.model;

public class News {
  
  String id;
  String url;
  String title;
  String time;
  String media;

  public News(String id, String url, String title, String time, String media) {
    super();
    this.id = id;
    this.url = url;
    this.title = title;
    this.time = time;
    this.media = media;
  }
  
  public String getUrl() {
    return url;
  }
  public void setUrl(String url) {
    this.url = url;
  }
  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }
  public String getTime() {
    return time;
  }
  public void setTime(String time) {
    this.time = time;
  }
  public String getMedia() {
    return media;
  }
  public void setMedia(String media) {
    this.media = media;
  }
  
  

}
