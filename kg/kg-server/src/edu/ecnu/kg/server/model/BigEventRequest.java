package edu.ecnu.kg.server.model;

import java.util.List;

public class BigEventRequest {
  
  
  List<SubEvent> seqEvent;
  public List<SubEvent> getSeqEvent() {
    return seqEvent;
  }
  public void setSeqEvent(List<SubEvent> seqEvent) {
    this.seqEvent = seqEvent;
  }
  int newsCount=0;
  public int getNewsCount() {
    return newsCount;
  }
  public void setNewsCount(int newsCount) {
    this.newsCount = newsCount;
  }
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }
  public String getContent() {
    return content;
  }
  public void setContent(String content) {
    this.content = content;
  }
  public String getStartTime() {
    return startTime;
  }
  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }
  String id;
  String content;
  String startTime;
}
