package edu.ecnu.kg.server.model;

import java.util.HashMap;
import java.util.Map;

public class Link {
  
  int index;

  String id;
  String label;
  int  source;
  int  target;
  Map<String,String> properties;

  public Link() {
    properties=new HashMap<String, String>();
  }


  public void addProperty(String key, String value){
    properties.put(key, value);
  }
  /**
   * 取得id
   * @return 返回 id。
   */
  public String getId() {
    return id;
  }

  /**
   * 设置id
   * @param id 要设置的 id。
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * 取得label
   * @return 返回 label。
   */
  public String getLabel() {
    return label;
  }

  /**
   * 设置label
   * @param label 要设置的 label。
   */
  public void setLabel(String label) {
    this.label = label;
  }

  /**
   * 取得properties
   * @return 返回 properties。
   */
  public Map<String, String> getProperties() {
    return properties;
  }

  /**
   * 设置properties
   * @param properties 要设置的 properties。
   */
  public void setProperties(Map<String, String> properties) {
    this.properties = properties;
  }

  
  /**
   * 取得source
   * @return 返回 source。
   */
  public int getSource() {
    return source;
  }


  /**
   * 设置source
   * @param source 要设置的 source。
   */
  public void setSource(int source) {
    this.source = source;
  }


  /**
   * 取得target
   * @return 返回 target。
   */
  public int getTarget() {
    return target;
  }


  /**
   * 设置target
   * @param target 要设置的 target。
   */
  public void setTarget(int target) {
    this.target = target;
  }


  public Link(int index, String id, int start, int end, String label) {
    super();
    this.index=index;
    this.id = id;
    this.label = label;
    this.source = start;
    this.target = end;
  }


  public Link(int index, String id, String label, int source, int target,
      Map<String, String> properties) {
    super();
    this.index = index;
    this.id = id;
    this.label = label;
    this.source = source;
    this.target = target;
    this.properties = properties;
  }
  
  /**
   * 取得index
   * @return 返回 index。
   */
  public int getIndex() {
    return index;
  }


  /**
   * 设置index
   * @param index 要设置的 index。
   */
  public void setIndex(int index) {
    this.index = index;
  }

}
