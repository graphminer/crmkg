package edu.ecnu.kg.server.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;

import edu.ecnu.kg.server.dao.InitialQueryDao;
import edu.ecnu.kg.server.model.QueryResult;


/** 
 * 界面查询入口 
 * @author leyi  2014年6月22日
 * @version 0.0.5
 * @modify 
 * @Copyright 华东师范大学数据科学与工程研究院版权所有
 */
@ParentPackage("json-default")
public class InitialQueryAction extends ActionSupport implements ServletRequestAware{
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private HttpServletRequest request;
  private InitialQueryDao dao=new InitialQueryDao();
  

  private ArrayList<QueryResult> searchKeys = new ArrayList<QueryResult>();
  
  @Action(value = "/search", results = @Result (type = "json"))
  public String fetchRelatedCorp() throws Exception{
    String query = "";
    query = request.getParameter("keywords");
    query = new String(query.getBytes("ISO-8859-1"),"UTF-8");
    //query = java.net.URLDecoder.decode(query,"UTF-8");
    //尝试分词后搜索，效果不佳
    searchKeys = dao.parseInitialQuery(query);
    
    return SUCCESS;
  }
  
  /**
   * 测试代码
   * @return
   * @throws Exception
   */
  @Action(value = "/show", results = @Result (type = "json"))
  public String testShow() throws Exception{
    searchKeys.add(new QueryResult("中国平安保险(集团)股份有限公司","COMPANY","companyname","网易",true));
    searchKeys.add(new QueryResult("平安银行股份有限公司","COMPANY","companyname","凤凰网，网易，深市",true));
    searchKeys.add(new QueryResult("平安证券有限责任公司","COMPANY","companyname","凤凰网",true));
    searchKeys.add(new QueryResult("平安公司","机构名","name","新闻数据",true));
    searchKeys.add(new QueryResult("平安","专有名","name","新闻数据",true));
    System.out.println("test");
    return SUCCESS;
  }  
  

  public ArrayList<QueryResult> getSearchKeys() {
    return searchKeys;
  }

  public void setSearchKeys(ArrayList<QueryResult> searchKeys) {
    this.searchKeys = searchKeys;
  }

  @Override
  public void setServletRequest(HttpServletRequest arg0) {
    request = arg0;
    
  }

}
