package edu.ecnu.kg.server.action;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.opensymphony.xwork2.ActionSupport;

import edu.ecnu.kg.server.dao.CommitQueryDao;
import edu.ecnu.kg.server.model.Category;
import edu.ecnu.kg.server.model.Link;
import edu.ecnu.kg.server.model.QueryResult;
import edu.ecnu.kg.server.model.ResultGraph;
import edu.ecnu.kg.server.model.Vertex;

@ParentPackage("json-default")
public class CommitQueryAction extends ActionSupport implements ServletRequestAware{
  
  private static final long serialVersionUID = 1L;
  private HttpServletRequest request;
  private CommitQueryDao dao = new CommitQueryDao();
  
  //返回子图内容
  private ArrayList<Category> categories = new ArrayList<Category>();
  private ArrayList<Vertex> nodes=new ArrayList<Vertex>();
  private ArrayList<Link> links=new ArrayList<Link>();
  
  @Action(value = "/commit", results = @Result (type = "json"))
  public String commitQuery() throws UnsupportedEncodingException, InterruptedException{
    
    ArrayList<QueryResult> filteredrst;
    filteredrst=doPost();
    
    dao.commitQueryLog(filteredrst);
    ResultGraph rg = dao.commitQueryCorps(filteredrst);
    
    ArrayList<Vertex>  list=rg.getNodelist();
    System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
    
    

    Collections.sort(list, new Comparator<Vertex>() {
      @Override
      public int compare(Vertex o1, Vertex o2) {
        // TODO Auto-generated method stub
        if (o1.getIndex() >= o2.getIndex()) {
          return 1;
        }
        return -1;
      }
    });
    
    rg.setNodelist(list);
    categories = rg.getCategories();
    nodes = rg.getNodelist();
    links = rg.getLinklist();
    
    
    for(int i=0;i<links.size();i++){
      Link link=links.get(i);
      String label=link.getLabel();
      switch(label){
        case "organization":
          link.setLabel("公司类别");
          break;
        case "area":
          link.setLabel("位于");
          break;
      }
    }
    rg.setLinklist(links);
    Gson gson = new Gson();
    System.out.println(gson.toJson(rg));
    
//    System.out.println("Return node size:"+nodes.size());
    
    return SUCCESS;
  }


  /**
   * 
   * @param filteredrst
   * @throws UnsupportedEncodingException 
   */
  private ArrayList<QueryResult> doPost() throws UnsupportedEncodingException {
    // 
    ArrayList<QueryResult> filteredrst = new ArrayList<QueryResult>();
//    String json="[{\"category\":\"机构名\",\"datasource\":\""
//        + "News\",\"name\":\"中石化\",\"property\":\"name\",\"selectable\":true}]";
//    String json="[{\"category\": \"COMPANY\",\"datasource\": \"IFENG\","
//        + "\"name\": \"平安证券有限责任公司\","
//        + "\"property\": \"companyname\","
//        + "\"selectable\": true"
//        + "},"
//        + "{"
//        + "\"category\": \"COMPANY\","
//        + "\"datasource\": \"IFENG,NTES,SZSE\","
//        + "\"name\": \"平安银行股份有限公司\","
//        + "\"property\": \"companyname\","
//        + "\"selectable\": true"
//        + "},"
//        + "{"
//        + "\"category\": \"COMPANY\","
//        + "\"datasource\": \"IFENG\","
//        + "\"name\": \"中国平安保险集团股份有限公司\","
//        + "\"property\": \"companyname\","
//        + "\"selectable\": true"
//        + "}]";
//    String line = null;
//    try{
//      BufferedReader reader = request.getReader();
//      while ((line = reader.readLine())!=null){
//        json.append(line);
//      }
//    }catch(Exception e){
//      System.out.println();
//    }
    String json=request.getParameter("sendInfo");
    json = java.net.URLDecoder.decode(json,"UTF-8");
    System.out.println(json);
    
    try{
      Gson gson = new Gson();
      Type listType = new TypeToken<ArrayList<QueryResult>>(){}.getType();
      filteredrst = gson.fromJson(json, listType);
      System.out.println(filteredrst.toString());
    }catch(Exception e){
      System.out.println();
    }
    
    return filteredrst;
    
  }


  @Override
  public void setServletRequest(HttpServletRequest arg0) {
    request=arg0;
  }


  public ArrayList<Vertex> getNodes() {
    return nodes;
  }

  public void setNodes(ArrayList<Vertex> nodes) {
    this.nodes = nodes;
  }

  public ArrayList<Link> getLinks() {
    return links;
  }

  public void setLinks(ArrayList<Link> links) {
    this.links = links;
  }

  public ArrayList<Category> getCategories() {
    return categories;
  }

  public void setCategories(ArrayList<Category> categories) {
    this.categories = categories;
  }
  
  public  static void main(String[] args){
    String json="[{\"category\": \"COMPANY\",\"datasource\": \"IFENG\","
        + "\"name\": \"平安证券有限责任公司\","
        + "\"property\": \"companyname\","
        + "\"selectable\": true"
        + "},"
        + "{"
        + "\"category\": \"COMPANY\","
        + "\"datasource\": \"IFENG,NTES,SZSE\","
        + "\"name\": \"平安银行股份有限公司\","
        + "\"property\": \"companyname\","
        + "\"selectable\": true"
        + "},"
        + "{"
        + "\"category\": \"COMPANY\","
        + "\"datasource\": \"IFENG\","
        + "\"name\": \"中国平安保险集团股份有限公司\","
        + "\"property\": \"companyname\","
        + "\"selectable\": true"
        + "}]";
    
    Gson gson = new Gson();
    Type listType = new TypeToken<ArrayList<QueryResult>>(){}.getType();
    ArrayList<QueryResult> filteredrst = gson.fromJson(json.toString(), listType);
    System.out.println("Test:"+filteredrst.size());
    return;
  }
  
  
}
