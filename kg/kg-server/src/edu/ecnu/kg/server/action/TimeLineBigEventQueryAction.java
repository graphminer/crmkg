package edu.ecnu.kg.server.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;

import edu.ecnu.kg.server.dao.TimeLineQueryDao;
import edu.ecnu.kg.server.model.BigEventRequest;

@ParentPackage("json-default")
public class TimeLineBigEventQueryAction extends ActionSupport implements ServletRequestAware{

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  private HttpServletRequest request;
  private TimeLineQueryDao dao=new TimeLineQueryDao();
  private List<BigEventRequest> bigEventKeyWords = new ArrayList<BigEventRequest>();
  
  
  @Override
  public void setServletRequest(HttpServletRequest arg0) {
    // TODO Auto-generated method stub
    request = arg0;
  }
  
  @Action(value = "/searchBigEvent", results = @Result (type = "json"))
  public String fetchBigEvent() throws Exception{
    String query = "";
    query = request.getParameter("keywords");
    query = new String(query.getBytes("ISO-8859-1"),"UTF-8");
    //尝试分词后搜索，效果不佳
    bigEventKeyWords=dao.queryBigEventKeyWord(query);
    return SUCCESS;
  }
  
  public List<BigEventRequest> getBigEventKeyWords() {
    return bigEventKeyWords;
  }
  
  public void setBigEventKeyWords(List<BigEventRequest> bigEventKeyWords) {
    this.bigEventKeyWords = bigEventKeyWords;
  }
  
}


