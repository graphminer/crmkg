package edu.ecnu.kg.server.action;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;

import edu.ecnu.kg.server.dao.FetchNewsDao;
import edu.ecnu.kg.server.model.News;

@ParentPackage("json-default")
public class FetchNewsAction extends ActionSupport implements ServletRequestAware{

  private static final long serialVersionUID = 1L;
  private HttpServletRequest request;
  private FetchNewsDao dao = new FetchNewsDao();
  
  private News fetchedNews;
  
  @Action(value = "/fetchnews", results = @Result (type = "json"))
  public String fetchNewsByID() {

    String query = "";
    query = request.getParameter("newsid");
    try {
      query=java.net.URLDecoder.decode(query,"UTF-8");
      fetchedNews = dao.fetchNewsByID(query);
      
    } catch (UnsupportedEncodingException | SQLException e) {
      System.out.println("Fetch news Error!"+e.toString());
    }
    
    return SUCCESS;
  }
 
  public News getFetchedNews() {
    return fetchedNews;
  }

  public void setFetchedNews(News fetchedNews) {
    this.fetchedNews = fetchedNews;
  }

  @Override
  public void setServletRequest(HttpServletRequest arg0) {
    request = arg0;
    
  }

}
