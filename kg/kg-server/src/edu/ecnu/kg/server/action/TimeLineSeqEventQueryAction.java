package edu.ecnu.kg.server.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;

import edu.ecnu.kg.server.dao.TimeLineQueryDao;
import edu.ecnu.kg.server.model.BigEventRequest;
@ParentPackage("json-default")
public class TimeLineSeqEventQueryAction extends ActionSupport implements ServletRequestAware{

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  private HttpServletRequest request;
  private TimeLineQueryDao dao=new TimeLineQueryDao();
  private BigEventRequest timeline=new BigEventRequest();
  
  
  @Override
  public void setServletRequest(HttpServletRequest arg0) {
    // TODO Auto-generated method stub
    request = arg0;
  }
  
  @Action(value = "/searchSeqEvent", results = @Result (type = "json"))
  public String fetchSeqEvent() throws Exception{
    String query = "";
    query = request.getParameter("keywords");
    query = new String(query.getBytes("ISO-8859-1"),"UTF-8");
    //尝试分词后搜索，效果不佳
    timeline=dao.queryEventSeqById(query);
    return SUCCESS;
  }

  public BigEventRequest getTimeline() {
    return timeline;
  }

  public void setTimeline(BigEventRequest seqEvents) {
    this.timeline = seqEvents;
  }
  
  
}
