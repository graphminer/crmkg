package edu.ecnu.kg.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class SearchTokens {

  public static final List<String> SEARCH_LABELS = new ArrayList<String>(){

    {
      add("COMPANY");
      //add("PRODUCT");
      add("机构名");
      add("专有名");
      }
  };
  
  public static final Map<String,String[]> NODE_PROPERTY_KEYS = new HashMap<String,String[]>(){
    {
      put("COMPANY",new String[]{"companyname","chinesename"});
      put("机构名", new String[]{"name"});
      put("专有名",new String[]{"name"});
      put("PRODUCT",new String[]{"name","product"});
      put("CUSTOMER",new String[]{"customers"});
      put("ORGANIZATION", new String[]{"organization"});
      put("人名",new String[]{"name"});
      put("PERSON",new String[]{"person"});
      put("AREA",new String[]{"area"});
      put("NEWSPAPER",new String[]{"newspaper"});
      put("BRAND",new String[]{"brandname"});
      put("地名",new String[]{"name"});
     // put();
    }
  };

   public static final List<String> QUERY_STOPWORDS = Arrays.asList(
      "集团有限公司",
      "有限责任公司",
      "股份有限公司",
      "管理有限公司",
      "贸易有限公司",
      "制造有限公司",
      "销售有限公司",
      "汽车有限公司",
      "咨询有限公司",
      "控股有限公司",
      "代理有限公司",
      "进出口公司",
      "控股集团",
      "责任公司",
      "有限公司",
      "基金公司",
      "服务公司",
      "工业集团",
      "总公司",
      "分公司",
      "公司",
      "集团",
      "商社",
      "基金"
      );
  
  public static final String DATASOURCE_TOKEN="datasource";
  
  public static final HashMap<String,String> NODE_KEYS = new HashMap<String,String>(){
    {
      put("companyname","name");
      put("person","name");
      put("organization","name");
      put("brandname","name");
      put("newspaper","name");
      put("area","name");
      put("customers","name");
    }
    
  };

}
