package edu.ecnu.kg.server;

import java.sql.Connection;
import java.sql.SQLException;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.graphdb.GraphDatabaseService;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import edu.ecnu.kg.server.utils.Neo4jOnlineEngine;

/** 
 * Manage data source connection
 * @author leyi  2014年6月21日
 * @version 0.0.5
 * @modify 
 * @Copyright 华东师范大学数据科学与工程研究院版权所有
 */
public class DataSourceManager {
  
  private static final DataSourceManager instance = new DataSourceManager();
  
  /**
   * MySQL RDB
   */
  private ComboPooledDataSource rdb;
  
  /**
   * Embedded Neo4j Query Engine
   */
  private Neo4jOnlineEngine neo4jengine;
  
  
  private DataSourceManager() {
    initRDB();
    initGraphDB();
  }
  
  public static DataSourceManager getInstance(){
    return instance;
  }
  
  private synchronized void initRDB(){
    if(rdb == null){
      try {
      rdb = new ComboPooledDataSource();
      rdb.setDriverClass("com.mysql.jdbc.Driver");
      rdb.setJdbcUrl("jdbc:mysql://10.11.1.36:3306/kg?characterEncoding=utf-8&&zeroDateTimeBehavior=convertToNull");
      rdb.setMaxPoolSize(20);
      rdb.setMinPoolSize(2);
      rdb.setInitialPoolSize(2);
      rdb.setCheckoutTimeout(1800);
      rdb.setMaxStatements(0);
      rdb.setAcquireIncrement(1);
      rdb.setIdleConnectionTestPeriod(60);
      rdb.setAcquireRetryAttempts(400);
      rdb.setAcquireRetryDelay(3000);
      } catch (Exception e) {
        throw new RuntimeException("初始化错误", e);
      }
      System.out.println("Initialize RDB...");
    }

  }
  
  private synchronized void initGraphDB(){
    if(neo4jengine == null){
      neo4jengine = Neo4jOnlineEngine.getInstance();
      System.out.println("Initialize Graph DB...");
    }
  }
  
  /**
   * 初始化connection
   * 
   * @return Connection
   * @throws SQLException
   */
  public Connection getRDBConnection() throws SQLException{
    if(rdb == null){
      initRDB();
    }
    return rdb.getConnection();
  }
  
  /**
   * 获取图数据库service
   * @return
   */
  public GraphDatabaseService getGraphDb() {
    if(neo4jengine == null){
      initGraphDB();
    }
    return Neo4jOnlineEngine.getGraphDb();
  }
  
  /**
   * 获取cypher执行引擎
   * @return
   */
  public ExecutionEngine getExcutionEngine() {
    if(neo4jengine == null){
      initGraphDB();
    }
    return Neo4jOnlineEngine.getEngine();
  }

}
