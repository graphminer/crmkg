package edu.ecnu.kg.snowball.stat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;



public class CoEntityCount {

	public static class CoEntityCountMapper extends MapReduceBase implements
		Mapper<LongWritable, Text, Text, IntWritable> {

		@Override
		public void map(LongWritable key, Text value,
				OutputCollector<Text, IntWritable> output, Reporter reporter)
				throws IOException {
			String text=value.toString();
			String[] items=text.split("\t");
			List<String> entities=new ArrayList<String>();
			for (String item:items) {
				if (item.endsWith("人名") || item.endsWith("地名") || item.endsWith("机构名") || item.endsWith("专有名")) {
					entities.add(item);
				}		
			}
			for (int i=0;i<entities.size();i++)
				for (int j=1;j<entities.size();j++) {
					String first=entities.get(i);
					String second=entities.get(j);
					String keyStr;
					if (first.compareTo(second)>0)
						keyStr=first+","+second;
					else
						keyStr=second+","+first;
					output.collect(new Text(keyStr), new IntWritable(1));
				}
		}
	}

	public static class CoEntityCountErducer extends MapReduceBase implements
		Reducer<Text, IntWritable, Text, IntWritable> {

		public void reduce(Text key, Iterator<IntWritable> values,
				OutputCollector<Text, IntWritable> output, Reporter reporter)
				throws IOException {
			int sum = 0;
			while (values.hasNext()) {
				sum+=values.next().get();
			}
			output.collect(key, new IntWritable(sum));
		}
	}

	public static void main(String[] args) throws Exception {
		JobConf conf = new JobConf(EntityCount.class);

		conf.setJobName("bigdata001@" + System.currentTimeMillis());
		conf.setMapperClass(CoEntityCountMapper.class);
		conf.setReducerClass(CoEntityCountErducer.class);
				
		conf.setNumReduceTasks(20);
		FileInputFormat.setInputPaths(conf, new Path(args[0]));
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));
		conf.setInputFormat(TextInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(IntWritable.class);

		Date startTime = new Date();
		System.out.println("Job started: " + startTime);
		JobClient.runJob(conf);
		Date end_time = new Date();
		System.out.println("Job ended: " + end_time);
		System.out.println("The job took "
				+ (end_time.getTime() - startTime.getTime()) / (float) 1000.0
				+ " seconds.");
	}
}