package edu.ecnu.kg.snowball.load;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import edu.ecnu.kg.common.SnowballConfig;
import edu.ecnu.kg.snowball.entity.RelationTuple;


public class LoadSeed {

	public static List<RelationTuple> seeds() throws IOException {
		List<RelationTuple> seeds = new ArrayList<RelationTuple>();
    	InputStreamReader isr = new InputStreamReader(new FileInputStream(SnowballConfig.getSeedPath()), "utf-8");
        BufferedReader br=new BufferedReader(isr);

		String line;
		while ((line = br.readLine()) != null) {
			String[] items = line.split("\t");
			String first = items[0];
			String firstType = items[1];
			String second = items[2];
			String secondType = items[3];
			String relationName = items[4];

			String predicates = items[5];
			String[] p = predicates.split(",");
			List<String> pList = new ArrayList<String>();
			for (String s : p)
				pList.add(s);
			
			List<String> bList = new ArrayList<String>();
			if (items.length>5) {
				String blackWords = items[6];
				String[] b = blackWords.split(",");
				for (String s : b)
					bList.add(s);
			}

			RelationTuple tuple = new RelationTuple(first, firstType, second,
					secondType, relationName, pList, bList);

			seeds.add(tuple);
		}
		br.close();
		return seeds;
	}

}