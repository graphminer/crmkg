package edu.ecnu.kg.snowball.entity;

public class TokenWeightPair implements Comparable<TokenWeightPair> {

	public String token;
	public double weight;

	public TokenWeightPair(String token, double weight) {
		super();
		this.token = token;
		this.weight = weight;
	}

	@Override
	public int compareTo(TokenWeightPair o) {
		if (weight > o.weight)
			return 1;
		else if (weight < o.weight)
			return -1;
		else
			return 0;
	}

}