package edu.ecnu.kg.snowball.bootstrap;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.lucene.queryparser.classic.ParseException;

import edu.ecnu.kg.snowball.entity.RelationTuple;
import edu.ecnu.kg.snowball.entity.Sentence;
import edu.ecnu.kg.snowball.entity.TokenTaggerPair;
import edu.ecnu.kg.snowball.load.LoadEntitySet;
import edu.ecnu.kg.snowball.load.LoadSeed;
import edu.ecnu.kg.snowball.lucene.SearchResult;
import edu.ecnu.kg.snowball.pattern.GeneralPattern;
import edu.ecnu.kg.snowball.pattern.RawPattern;
import edu.ecnu.kg.snowball.search.DoSearch;


public class Bootstrap {
	private static PrintWriter pw;
	private static Set<String> entityList;

	public static void main(String[] args) throws Exception {
		entityList = LoadEntitySet.entities();
		pw = new PrintWriter("dongshi.txt");
		bootstrap();
		pw.close();
	}

	public static void bootstrap() throws Exception {
		int iterNum = 1;

		List<RelationTuple> seeds = LoadSeed.seeds();
		List<RelationTuple> allRelations = new ArrayList<RelationTuple>(seeds);

		while (!seeds.isEmpty()) {
			List<RelationTuple> newTuples = new ArrayList<RelationTuple>();
			for (RelationTuple tuple : seeds) {
				GeneralPattern generalPattern = createGeneralPattern(tuple);
				if (generalPattern == null)
					continue;
				if (!generalPattern.validPattern())
					continue;
				generalPattern.print();
				String newQuery = createNewQuery(generalPattern);
				System.out.println(newQuery);
				if (newQuery == null)
					continue;

				List<SearchResult> newSearchOutcome = DoSearch.search(newQuery,
						100000);
				List<RelationTuple> tuples = searchCandidates(newSearchOutcome,
						generalPattern);
				for (RelationTuple tuple2 : tuples) {
					if (!allRelations.contains(tuple2)
							&& !newTuples.contains(tuple2))
						newTuples.add(tuple2);
				}
			}
			seeds = new ArrayList<RelationTuple>(newTuples);
			allRelations.addAll(newTuples);

			System.out.println(iterNum + " new tuples: " + newTuples.size());
			for (RelationTuple r : seeds) {
				System.out.println(r);
			}
			pw.println(iterNum + " new tuples: " + newTuples.size());
			for (RelationTuple r : seeds) {
				pw.print(r.getFirst() + "\t" + r.getSecond() + "\t"
						+ r.getRelationName() + "\t");
				if (r.getSentence() != null) {
					pw.println(r.getSentence().toString() + "\t" + r.getDocId());
				} else {
					pw.println("seed");
				}
				pw.flush();
			}
			iterNum++;
		}
		pw.println("all");
		for (RelationTuple t : allRelations) {
			pw.print(t.getFirst() + "\t" + t.getSecond() + "\t"
					+ t.getRelationName() + "\t");
			if (t.getSentence() != null) {
				pw.println(t.getSentence().toString() + "\t" + t.getDocId());
			} else {
				pw.println("seed");
			}
			pw.flush();
		}
	}

	public static List<RelationTuple> searchCandidates(
			List<SearchResult> searchResults, GeneralPattern generalPattern)
			throws Exception {
		double threshold = 0.3;
		List<RelationTuple> tuples = new ArrayList<RelationTuple>();
		for (SearchResult searchResult : searchResults) {
			String docId = searchResult.id;
			Sentence sen = null;
			try {
				sen = Sentence.parseTaggedSentence(searchResult.tagged);
			} catch (Exception e) {
				continue;
			}
			List<TokenTaggerPair> pairs = sen.getTaggedSentence();
			if (pairs.size() > 40)
				continue;
			for (int i = 0; i < pairs.size(); i++) {
				for (int j = i + 1; j < pairs.size(); j++) {
					if ((pairs.get(i).getNerTagger()
							.equals(generalPattern.getFirstType())/* || pairs
							.get(i).getNerTagger().equals("专有名")*/)
							&& (pairs.get(j).getNerTagger()
									.equals(generalPattern.getSecondType())/* || pairs
									.get(j).getNerTagger().equals("专有名")*/)
							&& entityList.contains(pairs.get(i).getToken())
							&& entityList.contains(pairs.get(j).getToken())) {
						RawPattern rawPattern = new RawPattern(sen, pairs
								.get(i).getToken(), pairs.get(j).getToken());
						if (rawPattern.emptyWords())
							continue;

						double weights = match(rawPattern, generalPattern);
						if (weights > threshold) {

							String firstNE = pairs.get(i).getToken();
							String secondNE = pairs.get(j).getToken();
							if (firstNE.indexOf(secondNE) >= 0
									|| secondNE.indexOf(firstNE) >= 0)
								continue;

							RelationTuple tuple = new RelationTuple(firstNE,
									generalPattern.getFirstType(), secondNE,
									generalPattern.getSecondType(),
									generalPattern.getRelationName(),
									generalPattern.getPredicates(),
									generalPattern.getBlackWords(), sen, docId);
							if (!tuples.contains(tuple))
								tuples.add(tuple);
						}
					}
				}
			}
		}

		return tuples;
	}

	private static GeneralPattern createGeneralPattern(RelationTuple tuple)
			throws ParseException, IOException {
		List<RawPattern> patterns = new ArrayList<RawPattern>();

		String first = tuple.getFirst();
		String second = tuple.getSecond();
		String firstType = tuple.getFirstType();
		String secondType = tuple.getSecondType();

		List<String> predicates = tuple.getPredicates();
		List<String> blackWords = tuple.getBlackwords();

		String predicatesString = "";
		if (predicates.size() == 1)
			predicatesString = predicates.get(0);
		else {
			for (int i = 0; i < predicates.size() - 1; i++)
				predicatesString += predicates.get(i) + " || ";
			predicatesString += predicates.get(predicates.size() - 1);
		}

		String blackWrodsString = "";
		if (blackWords.size() == 1)
			blackWrodsString = blackWords.get(0);
		else {
			for (int i = 0; i < blackWords.size() - 1; i++)
				blackWrodsString += blackWords.get(i) + " || ";
			blackWrodsString += blackWords.get(blackWords.size() - 1);
		}

		String query = first + " && " + second + " && ( " + predicatesString
				+ " ) && !( " + blackWrodsString + " )";
		System.out.println(query);

		int rawPatternNum = 0;
		final int maxRawPatternNum = 1;
		List<SearchResult> searchOutcome = DoSearch.search(query, 1000);

		for (SearchResult s : searchOutcome) {
			if (patterns.size() > maxRawPatternNum)
				break;
			try {
				Sentence sen = Sentence.parseTaggedSentence(s.tagged);
				// System.out.println(sen+"\t"+first+"\t"+second);
				RawPattern pattern = new RawPattern(sen, first, second);
				if (pattern.emptyWords()) {
				//	System.out.println("empty");
					continue;
				} else {
					System.out.println(sen);
					rawPatternNum++;
					patterns.add(pattern);
				}
			} catch (Exception e) {
				continue;
			}
		}
		// System.out.println(rawPatternNum);
		if (rawPatternNum < maxRawPatternNum)
			return null;
		GeneralPattern generalPattern = new GeneralPattern(
				tuple.getRelationName(), firstType, secondType, patterns,
				predicates, blackWords);
		return generalPattern;
	}

	private static String createNewQuery(GeneralPattern generalPattern) {
		List<String> wordsList = generalPattern.topKWords(8);
		if (wordsList.size() == 0)
			return null;

		List<String> predicates = generalPattern.getPredicates();
		List<String> blackWords = generalPattern.getBlackWords();
		String predicatesString = "";
		if (predicates.size() == 1)
			predicatesString = predicates.get(0);
		else {
			for (int i = 0; i < predicates.size() - 1; i++)
				predicatesString += predicates.get(i) + " || ";
			predicatesString += predicates.get(predicates.size() - 1);
		}

		String blackWrodsString = "";
		if (blackWords.size() == 1)
			blackWrodsString = blackWords.get(0);
		else {
			for (int i = 0; i < blackWords.size() - 1; i++)
				blackWrodsString += blackWords.get(i) + " || ";
			blackWrodsString += blackWords.get(blackWords.size() - 1);
		}

		String newQuery = "( " + predicatesString + " ) && !( "
				+ blackWrodsString + ") && ( ";
		for (int i = 0; i < wordsList.size() - 1; i++)
			newQuery += wordsList.get(i) + " || ";
		newQuery += wordsList.get(wordsList.size() - 1) + " )";

		return newQuery;
	}

	private static double match(RawPattern rawPattern,
			GeneralPattern generalPattern) {
		double weights = 0;
		Map<String, Double> leftMap = generalPattern.getLeft();
		for (String s : rawPattern.getLeft())
			if (leftMap.containsKey(s))
				weights += leftMap.get(s);

		Map<String, Double> middleMap = generalPattern.getMiddle();
		for (String s : rawPattern.getMiddle())
			if (middleMap.containsKey(s))
				weights += middleMap.get(s);

		Map<String, Double> rightMap = generalPattern.getRight();
		for (String s : rawPattern.getRight())
			if (rightMap.containsKey(s))
				weights += rightMap.get(s);

		return weights;
	}

}
