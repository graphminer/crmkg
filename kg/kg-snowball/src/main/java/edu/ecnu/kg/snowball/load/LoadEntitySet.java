package edu.ecnu.kg.snowball.load;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

import edu.ecnu.kg.common.SnowballConfig;

public class LoadEntitySet {

	public static Set<String> entities() throws IOException {
		Set<String> list = new HashSet<String>();
		InputStreamReader isr = new InputStreamReader(new FileInputStream(
				SnowballConfig.getEntityCountPath()), "utf-8");
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null) {
			String[] items = line.split("\t");
			String[] data = items[0].split("/");
			String entity = data[0];
			list.add(entity);
		}
		br.close();
		return list;
	}

}