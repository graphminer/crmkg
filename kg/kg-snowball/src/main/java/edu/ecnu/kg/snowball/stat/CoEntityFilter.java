package edu.ecnu.kg.snowball.stat;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import edu.ecnu.kg.common.SnowballConfig;

public class CoEntityFilter {

	public static void main(String[] args) throws IOException {
		PrintWriter pw=new PrintWriter(SnowballConfig.getCoEntityCountFilterPath(),"utf-8");
		InputStreamReader isr = new InputStreamReader(new FileInputStream(SnowballConfig.getCoEntityCountPath()), "utf-8");
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line=br.readLine())!=null) {
			try {
				String[] items=line.split("\t");
				String[] data=items[0].split(",");
				String[] first=data[0].split("/");
				String[] second=data[1].split("/");
				
				int count=Integer.parseInt(items[1]);
				String firstEntity=first[0];
				String secondEntity=second[0];
				String firstEntityType=first[2];
				String secondEntityType=second[2];
				if (!filter(firstEntity,secondEntity, count)) {
					pw.println(firstEntity+"\t"+firstEntityType+"\t"+secondEntity+"\t"+secondEntityType+"\t"+count);
					pw.flush();
				}
			} catch (Exception e) {
				continue;
			}
		}
		br.close();
		pw.close();
	}
	
	public static boolean filter(String first,String second,int count) {
		if (first.equals(second))
			return true;
		if (first.contains("微博") || second.contains("微博"))
			return true;
		if (first.contains("新浪") || second.contains("新浪"))
			return true;
		if (count<20)
			return true;
		if (first.length()<=1 || second.length()<=1)
			return true;
		char[] firstChars=first.toCharArray();
		for (char c:firstChars) {
			Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
		    if (!(ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
		            || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
		            || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
		            || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
		            || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
		            || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS))
		        return true;
		}
		char[] secondChars=second.toCharArray();
		for (char c:secondChars) {
			Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
		    if (!(ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
		            || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
		            || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
		            || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
		            || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
		            || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS))
		        return true;
		}
		return false;
	}

}