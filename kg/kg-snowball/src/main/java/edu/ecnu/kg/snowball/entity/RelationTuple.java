package edu.ecnu.kg.snowball.entity;

import java.util.List;


public class RelationTuple {

	private String first;
	private String firstType;
	private String second;
	private String secondType;
	private String relationName;

	private List<String> predicates;
	private List<String> blackwords;

	private Sentence sentence;
	private String docId;

	public RelationTuple(String first, String firstType, String second,
			String secondType, String relationName, List<String> predicates,
			List<String> blackwords, Sentence sentence,String docId) {
		super();
		this.first = first;
		this.firstType = firstType;
		this.second = second;
		this.secondType = secondType;
		this.relationName = relationName;
		this.predicates = predicates;
		this.blackwords = blackwords;
		this.sentence = sentence;
		this.docId = docId;
	}

	public RelationTuple(String first, String firstType, String second,
			String secondType, String relationName, List<String> predicates,
			List<String> blackwords) {
		super();
		this.first = first;
		this.firstType = firstType;
		this.second = second;
		this.secondType = secondType;
		this.relationName = relationName;
		this.predicates = predicates;
		this.blackwords = blackwords;
	}

	public String getFirstType() {
		return firstType;
	}

	public void setFirstType(String firstType) {
		this.firstType = firstType;
	}

	public String getSecondType() {
		return secondType;
	}

	public void setSecondType(String secondType) {
		this.secondType = secondType;
	}

	public List<String> getPredicates() {
		return predicates;
	}

	public void setPredicates(List<String> predicates) {
		this.predicates = predicates;
	}

	public List<String> getBlackwords() {
		return blackwords;
	}

	public void setBlackwords(List<String> blackwords) {
		this.blackwords = blackwords;
	}

	public Sentence getSentence() {
		return sentence;
	}

	public void setSentence(Sentence sentence) {
		this.sentence = sentence;
	}

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getSecond() {
		return second;
	}

	public void setSecond(String second) {
		this.second = second;
	}

	public String getRelationName() {
		return relationName;
	}

	public void setRelationName(String relationName) {
		this.relationName = relationName;
	}
	
	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	@Override
	public String toString() {
		return first + "\t" + second + "\t" + relationName;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof RelationTuple))
			return false;
		RelationTuple other = (RelationTuple) obj;
		if (first.equals(other.first) && second.equals(other.second)
				&& relationName.equals(other.relationName))
			return true;
		return false;
	}

}