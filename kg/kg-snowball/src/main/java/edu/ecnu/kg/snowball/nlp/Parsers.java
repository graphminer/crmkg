package edu.ecnu.kg.snowball.nlp;

import edu.ecnu.kg.common.SnowballConfig;
import edu.fudan.nlp.parser.dep.JointParser;
import edu.fudan.util.exception.LoadModelException;

public class Parsers {
	
	private static JointParser jointParser;

	public static JointParser getJointParser() throws LoadModelException {
		if (jointParser == null)
			jointParser = new JointParser(SnowballConfig.getModelPath()+"/dep.m");
		return jointParser;
	}
	
}
