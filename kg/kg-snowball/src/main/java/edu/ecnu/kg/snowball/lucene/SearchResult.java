package edu.ecnu.kg.snowball.lucene;

public class SearchResult {

	public String id;
	public String sentence;
	public String tagged;

	public SearchResult(String id, String sentence, String tagged) {
		super();
		this.id = id;
		this.sentence = sentence;
		this.tagged = tagged;
	}

	@Override
	public String toString() {
		return id + "\t" + sentence + "\t" + tagged;
	}

}