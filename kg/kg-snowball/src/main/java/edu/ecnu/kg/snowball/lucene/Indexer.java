package edu.ecnu.kg.snowball.lucene;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import edu.ecnu.kg.snowball.entity.Sentence;


public class Indexer {

	public void index(String indexDirectoryPath, String dataDirectoryPath)
			throws IOException {
		File indexDir = new File(indexDirectoryPath);
		File dataFolder = new File(dataDirectoryPath);

		Analyzer analyzer = new SmartChineseAnalyzer(Version.LUCENE_46, true);
		IndexWriterConfig indexWriterConfig = new IndexWriterConfig(
				Version.LUCENE_46, analyzer);

		Directory directory = FSDirectory.open(indexDir);
		if (IndexWriter.isLocked(directory)) {
			IndexWriter.unlock(directory);
		}

		IndexWriter writer = new IndexWriter(directory, indexWriterConfig);
		writer.deleteAll();

		File[] files = dataFolder.listFiles();
		for (File dataFile : files)
			indexRecords(writer, dataFile);

		writer.close();
	}

	private void indexRecords(IndexWriter writer, File dataFile)
			throws IOException {
		System.out.println(dataFile.getAbsolutePath());
		InputStreamReader isr = new InputStreamReader(new FileInputStream(dataFile), "utf-8");
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line=br.readLine())!=null) {
			try {
				int pos=line.indexOf("\t");
				String first=line.substring(0,pos);
				String second=line.substring(pos+1);
		//		System.out.println(second);
				Sentence sentence=Sentence.parseTaggedSentence(second);
				
				Document doc = new Document();
				doc.add(new StringField("id", first, Store.YES));
				doc.add(new TextField("sentence", sentence.toString(),
						Store.YES));
				doc.add(new StringField("tagged", sentence.toStringTagged(),
						Store.YES));
				writer.addDocument(doc);
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
		}
		br.close();
	}

}
