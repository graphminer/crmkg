package edu.ecnu.kg.snowball.search;

import java.io.IOException;

import edu.ecnu.kg.common.SnowballConfig;
import edu.ecnu.kg.snowball.lucene.Indexer;


public class BuildIndex {

	public static void buildIndex() throws IOException {
		Indexer indexer = new Indexer();
		indexer.index(SnowballConfig.getIndexPath(),
				SnowballConfig.getDocPath());
	}
	
}
