package edu.ecnu.kg.snowball.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import edu.ecnu.kg.snowball.nlp.Parsers;
import edu.ecnu.kg.snowball.nlp.Taggers;
import edu.fudan.nlp.cn.tag.NERTagger;
import edu.fudan.nlp.cn.tag.POSTagger;
import edu.fudan.nlp.parser.dep.DependencyTree;
import edu.fudan.nlp.parser.dep.JointParser;
import edu.fudan.util.exception.LoadModelException;

public class Sentence {

	private String line;
	private List<TokenTaggerPair> taggedSentence;
	private DependencyTree dependencyTree;

	public Sentence(String line) throws Exception {
		taggedSentence = new ArrayList<TokenTaggerPair>();
		this.line = line;
	}

	public Sentence(List<TokenTaggerPair> taggedSentence) {
		String line = "";
		this.taggedSentence = taggedSentence;
		for (TokenTaggerPair p : taggedSentence)
			line += p.getToken();
		this.line = line;
	}

	public void tagSentence() throws Exception {
		POSTagger posTagger = Taggers.getPOSTagger();
		NERTagger nerTagger = Taggers.getNERTagger();
		String[][] arr = posTagger.tag2Array(line);
		int sentenceLength = arr[0].length;
		for (int i = 0; i < sentenceLength; i++) {
			String token = arr[0][i];
			String posTag = arr[1][i];
			TokenTaggerPair tokenTaggerPair = new TokenTaggerPair(token,
					posTag, "");
			taggedSentence.add(tokenTaggerPair);
		}
		HashMap<String, String> nerMap = nerTagger.tag(line);
		for (Iterator<String> keys = nerMap.keySet().iterator(); keys.hasNext();) {
			String key = keys.next(); // token
			String value = nerMap.get(key); // nertag
			for (int j = 0; j < taggedSentence.size(); j++) {
				TokenTaggerPair tokenTaggerPair = taggedSentence.get(j);
				if (tokenTaggerPair.getToken().equals(key)) {
					tokenTaggerPair.setNerTagger(value);
					taggedSentence.set(j, tokenTaggerPair);
				}
			}
		}
	}

	public void setLine(String line) {
		this.line = line;
	}

	public void setTaggedSentence(List<TokenTaggerPair> taggedSentence) {
		this.taggedSentence = taggedSentence;
	}

	public void parseSentence() throws LoadModelException {
		POSTagger posTagger = Taggers.getPOSTagger();
		String[][] arr = posTagger.tag2Array(line);
		JointParser parser = Parsers.getJointParser();
		dependencyTree = parser.parse2T(arr[0], arr[1]);
	}

	public List<TokenTaggerPair> getTaggedSentence() {
		return taggedSentence;
	}

	public DependencyTree getDependencyTree() {
		return dependencyTree;
	}

	@Override
	public String toString() {
		return line;
	}

	public String toStringTagged() {
		String outString = "";
		for (int i = 0; i < taggedSentence.size() - 1; i++) {
			TokenTaggerPair p = taggedSentence.get(i);
			outString += p.getToken() + "/" + p.getPosTagger() + "/"
					+ p.getNerTagger() + "\t";
		}
		TokenTaggerPair p = taggedSentence.get(taggedSentence.size() - 1);
		outString += p.getToken() + "/" + p.getPosTagger() + "/"
				+ p.getNerTagger();
		return outString;
	}

	public static Sentence parseTaggedSentence(String s) {
		String[] items = s.split("\t");
		List<TokenTaggerPair> pairs = new ArrayList<TokenTaggerPair>();
		for (String item : items) {
			String[] i = item.split("/");
			String token = i[0];
			String posTagger = i[1];
			String nerTagger = "";
			if (i.length > 2)
				nerTagger = i[2];
			TokenTaggerPair p = new TokenTaggerPair(token, posTagger, nerTagger);
			pairs.add(p);
		}
		Sentence sentence = new Sentence(pairs);
		return sentence;
	}

}
