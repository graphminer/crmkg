package edu.ecnu.kg.snowball.search;

import java.io.IOException;
import java.util.List;

import org.apache.lucene.queryparser.classic.ParseException;

import edu.ecnu.kg.snowball.lucene.SearchResult;
import edu.ecnu.kg.snowball.lucene.Searcher;


public class DoSearch {

	public static List<SearchResult> search(String query, int topK)
			throws ParseException, IOException {
		Searcher searcher = Searcher.getInstance();
		List<SearchResult> outcome = searcher.search(query, topK);
		return outcome;
	}

	public static void main(String[] args) throws IOException, ParseException {
		List<SearchResult> outcome = search("董事 华谊 马云", 100);
		for (SearchResult s : outcome)
			System.out.println(s);
	}
	
}
