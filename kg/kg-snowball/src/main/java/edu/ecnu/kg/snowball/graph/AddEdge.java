package edu.ecnu.kg.snowball.graph;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.stream.XMLStreamException;

import edu.ecnu.kg.common.SimpleEdge;
import edu.ecnu.kg.common.SimpleNode;
import edu.ecnu.kg.common.graphml.GraphMLTokens;
import edu.ecnu.kg.common.graphml.GraphmlKey;
import edu.ecnu.kg.tools.writer.GraphMLWriter;

/** 
 *  
 * @author CBD_23  2014年6月20日
 * @version 0.0.3
 * @modify 
 * @Copyright 华东师范大学数据科学与工程院版权所有
 */
public class AddEdge {

    public static void main(String[] args) throws XMLStreamException, IOException {
        String weakInput="/home/bingo/data/co-relations.txt";    // 
        addWeakRelation(weakInput);
        System.out.println("weak relations done!");
    }
    
    public static void addWeakRelation(String inputPath) throws XMLStreamException, IOException{
        String relationMLPath = "/home/bingo/data/weakEdge.xml";
        InputStreamReader isr = new InputStreamReader(new FileInputStream(inputPath), "utf-8"); //
        BufferedReader br = new BufferedReader(isr);
        String line;
        int nid=1;
        int eid=1;
        GraphMLWriter writer = new GraphMLWriter(new FileOutputStream(relationMLPath));
        writer.writeHeader();
        writer.writeGraphStart();
        ArrayList<SimpleNode> nodes = new ArrayList<SimpleNode>();
        ArrayList<SimpleEdge> edges = new ArrayList<SimpleEdge>();
        ArrayList<GraphmlKey> nodekeys = new ArrayList<GraphmlKey>();  
        
        HashMap<String,String> store = new HashMap<String,String>();   // <name, nodeID>
        
        GraphmlKey key = new GraphmlKey();
        key.setId("k1");
        key.setName("name");
        key.setType(GraphMLTokens.STRING);
        nodekeys.add(key);
        
        while((line=br.readLine())!=null){
            String temp[] = line.split("\t");
            String sourceID=null;
            String targetID=null;
            
            if(store.isEmpty() ||(!store.containsKey(temp[0]))){   // new node
                store.put(temp[0], Integer.toString(nid++));
                SimpleNode node1 = new SimpleNode();
                sourceID = store.get(temp[0]);
                node1.setId(sourceID);
                node1.addProperty("name", temp[0]);
                node1.addLabel(temp[1]);
                nodes.add(node1);
            }else{//others to be identical
                System.out.println("Ignore repeat node");
                sourceID = store.get(temp[0]);
            }
            
            
            if((!store.containsKey(temp[2]))){ //new node
                store.put(temp[2], Integer.toString(nid++));
                SimpleNode node2 = new SimpleNode();
                targetID = store.get(temp[2]);
                node2.setId(targetID);
                node2.addProperty("name", temp[2]);
                node2.addLabel(temp[3]);
                nodes.add(node2);
            }else{//others to be identical
                System.out.println("Ignore repeat node");
                targetID = store.get(temp[2]);
            }
            
            SimpleEdge edge = new SimpleEdge();
            edge.setId(Integer.toString(eid++));
            edge.setLabel("RelateTo");
            edge.setSourcenode(sourceID);
            edge.setTargetnode(targetID);
            edge.addProperty("weight", temp[4]);
            edges.add(edge);
            System.out.println("Already edge "+(eid-1));
        }
        writer.writerNodes(nodes);
        writer.writeEdges(edges);

        writer.writeTail();
        System.out.println("Please check the ML file!");
    }

}
