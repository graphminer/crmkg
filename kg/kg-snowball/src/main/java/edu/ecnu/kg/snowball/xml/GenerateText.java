package edu.ecnu.kg.snowball.xml;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import edu.ecnu.kg.common.SnowballConfig;


public class GenerateText {

	private static long beginDocId=0;
	
	private static PrintWriter pw;
	
	public static void main(String[] args) throws Exception {
		pw=new PrintWriter(SnowballConfig.getDocPath(),"utf-8");
		getFile(new File(SnowballConfig.getXmlPath()));
		pw.close();
	}
	
	public static void getFile(File root) throws Exception {
		if (root.isFile()) {
			if (root.getName().endsWith("xml")) {
				try {
					insertIntoDB(root);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		else {
			File[] files = root.listFiles();
			for (File f:files)
				getFile(f);
		}
	}
	
	public static void insertIntoDB(File xmlFile) throws SAXException, IOException, ParserConfigurationException, ClassNotFoundException {	
		System.out.println(xmlFile.getAbsolutePath());
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setValidating(false);
		DocumentBuilder db = dbf.newDocumentBuilder();
		db.setEntityResolver(new EntityResolver() {
			@Override
			public InputSource resolveEntity(String arg0, String arg1)
					throws SAXException, IOException {
				return new InputSource(new ByteArrayInputStream( 
		                "<?xml version='1.0' encoding='UTF-8'?>".getBytes())); 
			}
		});
		Document document=db.parse(new FileInputStream(xmlFile));
		NodeList list = document.getElementsByTagName("news");
		for (int i = 0; i < list.getLength(); i++) {
			Node node = list.item(i);
			NodeList childNodes = node.getChildNodes();
			String url="";
			String title = "";
			String time = "";
			String plate="";
			String media="";
			String keywords="";
			String description="";
			String text = "";
			for (int j = 0; j < childNodes.getLength(); j++) {
				Node n = childNodes.item(j);
				String nodeName = n.getNodeName();
				if (nodeName.equals("url"))
					url = n.getTextContent();
				else if (nodeName.equals("title"))
					title = n.getTextContent();
				else if (nodeName.equals("time"))
					time = n.getTextContent();
				else if (nodeName.equals("plate"))
					plate = n.getTextContent();
				else if (nodeName.equals("media"))
					media = n.getTextContent();
				else if (nodeName.equals("keywords"))
					keywords = n.getTextContent();
				else if (nodeName.equals("description"))
					description = n.getTextContent();
				else if (nodeName.equals("text")) {
					text = n.getTextContent();
					String[] items=text.split("\n");
					text="";
					for (String s:items)
						text+=s;
				}
			}
			pw.println(beginDocId+"\t"+url+"\t"+title+"\t"+time+"\t"+plate+"\t"+media+"\t"+keywords+"\t"+description+"\t"+text);
			pw.flush();
			beginDocId++;
		}

	}

}
