package edu.ecnu.kg.snowball.pattern;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.ecnu.kg.snowball.entity.TokenWeightPair;


public class GeneralPattern {

	private String relationName;
	private String firstType;
	private String secondType;

	private List<RawPattern> patterns;

	private Map<String, Double> left;
	private Map<String, Double> middle;
	private Map<String, Double> right;

	private List<String> predicates;
	private List<String> blackWords;

	public GeneralPattern(String relationName, String firstType,
			String secondType, List<RawPattern> patterns,
			List<String> predicates, List<String> blackWords) {
		this.relationName = relationName;
		this.firstType = firstType;
		this.secondType = secondType;
		this.patterns = patterns;

		left = new HashMap<String, Double>();
		middle = new HashMap<String, Double>();
		right = new HashMap<String, Double>();

		this.predicates = predicates;
		this.blackWords = blackWords;

		combinePatterns();
	}

	public String getFirstType() {
		return firstType;
	}

	public void setFirstType(String firstType) {
		this.firstType = firstType;
	}

	public String getSecondType() {
		return secondType;
	}

	public void setSecondType(String secondType) {
		this.secondType = secondType;
	}

	private void combinePatterns() {
		Map<String, Integer> leftMap = new HashMap<String, Integer>();
		Map<String, Integer> middleMap = new HashMap<String, Integer>();
		Map<String, Integer> rightMap = new HashMap<String, Integer>();
		int leftCount = 0;
		int middleCount = 0;
		int rightCount = 0;

		for (RawPattern pattern : patterns) {
			if (pattern.emptyWords())
				continue;
			leftCount += pattern.getLeft().size();
			middleCount += pattern.getMiddle().size();
			rightCount += pattern.getRight().size();

			addWords(leftMap, pattern.getLeft());
			addWords(middleMap, pattern.getMiddle());
			addWords(rightMap, pattern.getRight());
		}

		calWordsWeight(leftMap, leftCount, 0.1, left);
		calWordsWeight(middleMap, middleCount, 0.8, middle);
		calWordsWeight(rightMap, rightCount, 0.1, right);
	}

	private void addWords(Map<String, Integer> map, List<String> words) {
		for (String s : words) {
			if (!map.containsKey(s))
				map.put(s, 1);
			else {
				int c = map.get(s);
				c++;
				map.put(s, c);
			}
		}
	}

	private void calWordsWeight(Map<String, Integer> map, int totalCount,
			double posWeight, Map<String, Double> outMap) {
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			String word = entry.getKey();
			double weight = (double) entry.getValue() / totalCount * posWeight;
			outMap.put(word, weight);
		}
	}

	public void print() {
		System.out.println(relationName);
		System.out.println("left");
		for (Map.Entry<String, Double> entry : left.entrySet())
			System.out.print(entry.getKey() + "," + entry.getValue() + "\t");
		System.out.println();
		System.out.println("middle");
		for (Map.Entry<String, Double> entry : middle.entrySet())
			System.out.print(entry.getKey() + "," + entry.getValue() + "\t");
		System.out.println();
		System.out.println("right");
		for (Map.Entry<String, Double> entry : right.entrySet())
			System.out.print(entry.getKey() + "," + entry.getValue() + "\t");
		System.out.println();
	}

	public List<String> topKWords(int topK) {
		Map<String, Double> mergeMap = new HashMap<String, Double>();
		merge(mergeMap, left);
		merge(mergeMap, middle);
		merge(mergeMap, right);
		List<TokenWeightPair> pairs = new ArrayList<TokenWeightPair>();
		for (Map.Entry<String, Double> entry : mergeMap.entrySet()) {
			pairs.add(new TokenWeightPair(entry.getKey(), entry.getValue()));
		}
		Collections.sort(pairs);
		Collections.reverse(pairs);
		if (pairs.size() > topK)
			pairs = pairs.subList(0, topK);
		List<String> outcome = new ArrayList<String>();
		for (TokenWeightPair p : pairs)
			outcome.add(p.token);
		return outcome;
	}

	public boolean validPattern() {
		double totalWeight=0;
		for (String s:predicates) {
			if (left.containsKey(s))
				totalWeight+=left.get(s);
			if (middle.containsKey(s))
				totalWeight+=middle.get(s);
			if (right.containsKey(s))
				totalWeight+=right.get(s);
		}
		return totalWeight>0.1;
	}
	
	private void merge(Map<String, Double> mergeMap,
			Map<String, Double> sourceMap) {
		for (Map.Entry<String, Double> entry : sourceMap.entrySet()) {
			if (!mergeMap.containsKey(entry.getKey()))
				mergeMap.put(entry.getKey(), entry.getValue());
			else {
				double d = mergeMap.get(entry.getKey());
				d += entry.getValue();
				mergeMap.put(entry.getKey(), d);
			}
		}
	}

	public String getRelationName() {
		return relationName;
	}

	public Map<String, Double> getLeft() {
		return left;
	}

	public Map<String, Double> getMiddle() {
		return middle;
	}

	public Map<String, Double> getRight() {
		return right;
	}

	public List<String> getPredicates() {
		return predicates;
	}

	public List<String> getBlackWords() {
		return blackWords;
	}
}
