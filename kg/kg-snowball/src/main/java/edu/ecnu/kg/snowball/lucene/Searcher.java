package edu.ecnu.kg.snowball.lucene;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import edu.ecnu.kg.common.SnowballConfig;


public class Searcher {

	private static Searcher instance;

	private String indexDirectoryPath = null;
	private QueryParser contentParser = null;
	private IndexSearcher searcher = null;

	public static Searcher getInstance() {
		if (instance == null)
			instance = new Searcher(SnowballConfig.getIndexPath());
		return instance;
	}

	private Searcher(String indexDirectoryPath) {
		this.indexDirectoryPath = indexDirectoryPath;
		try {
			initialize();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<SearchResult> search(String searchWords, int topK)
			throws ParseException, IOException {
		List<SearchResult> outcome = new ArrayList<SearchResult>();

		Query query = contentParser.parse(searchWords);
		TopDocs topdocs = searcher.search(query, topK);
		ScoreDoc[] scoreDocs = topdocs.scoreDocs;

		for (int i = 0; i < scoreDocs.length; i++) {
			int doc = scoreDocs[i].doc;
			Document document = searcher.doc(doc);
			String sentence = document.get("sentence");
			String tagged = document.get("tagged");
			String id = document.get("id");

			SearchResult result = new SearchResult(id, sentence, tagged);
			outcome.add(result);
		}

		return outcome;
	}

	private void initialize() throws IOException {
		Directory indexDir = FSDirectory
				.open(new File(this.indexDirectoryPath));
		IndexReader reader = DirectoryReader.open(indexDir);
		searcher = new IndexSearcher(reader);
		contentParser = new QueryParser(Version.LUCENE_46, "sentence",
				new SmartChineseAnalyzer(Version.LUCENE_46, true));
	}

}
