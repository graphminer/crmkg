package edu.ecnu.kg.snowball.graph;

/** 
 *  
 * @author CBD_23  2014年6月20日
 * @version 0.0.2
 * @modify 
 * @Copyright 华东师范大学数据科学与工程院版权所有
 */
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.xml.stream.XMLStreamException;

import edu.ecnu.kg.common.SimpleNode;
import edu.ecnu.kg.common.graphml.GraphMLTokens;
import edu.ecnu.kg.common.graphml.GraphmlKey;
import edu.ecnu.kg.tools.writer.GraphMLWriter;

public class AddNode {
    
    public static void main(String args[]) throws IOException, XMLStreamException{
        String entityFilePath = "D:/entities.txt"; //
        String nodeMLPath = "D:/node.xml";
        InputStreamReader isr = new InputStreamReader(new FileInputStream(entityFilePath), "utf-8"); //
        BufferedReader br = new BufferedReader(isr);
        String line;
        int id=1;
        GraphMLWriter writer = new GraphMLWriter(new FileOutputStream(nodeMLPath));
        writer.writeHeader();
        writer.writeGraphStart();
        ArrayList<SimpleNode> nodes = new ArrayList<SimpleNode>(); 
        ArrayList<GraphmlKey> nodekeys = new ArrayList<GraphmlKey>();  
        
        GraphmlKey key1 = new GraphmlKey();
        key1.setId("k1");
        key1.setName("name");
        key1.setType(GraphMLTokens.STRING);
        nodekeys.add(key1);

        GraphmlKey key2 = new GraphmlKey();
        key2.setId("k2");
        key2.setName("count");
        key2.setType(GraphMLTokens.STRING);
        nodekeys.add(key2);
        
        writer.writeVertexKeyset(nodekeys);
        
        while((line=br.readLine())!=null){
            String temp[] = line.split("\t");
            SimpleNode node = new SimpleNode();
            node.setId(Integer.toString(id++));
            node.addProperty("name", temp[0]);
            node.addLabel(temp[1]);
            node.addProperty("count", temp[2]);
            nodes.add(node);
            System.out.println("Already node "+(id-1));
        }
        
        writer.writerNodes(nodes);
        writer.writeTail();
        System.out.println("Please check the ML file!");
    }
}
