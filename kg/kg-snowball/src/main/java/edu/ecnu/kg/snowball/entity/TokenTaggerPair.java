package edu.ecnu.kg.snowball.entity;

public class TokenTaggerPair {

	private String token;
	private String posTagger;
	private String nerTagger;

	public TokenTaggerPair(String token, String posTagger, String nerTagger) {
		this.token = token;
		this.posTagger = posTagger;
		this.nerTagger = nerTagger;
	}

	public String getToken() {
		return token;
	}

	public String getPosTagger() {
		return posTagger;
	}

	public String getNerTagger() {
		return nerTagger;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setPosTagger(String posTagger) {
		this.posTagger = posTagger;
	}

	public void setNerTagger(String nerTagger) {
		this.nerTagger = nerTagger;
	}

}