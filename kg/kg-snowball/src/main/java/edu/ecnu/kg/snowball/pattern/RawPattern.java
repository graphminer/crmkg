package edu.ecnu.kg.snowball.pattern;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.ecnu.kg.snowball.entity.Sentence;
import edu.ecnu.kg.snowball.entity.TokenTaggerPair;
import edu.ecnu.kg.snowball.filter.StopWordsFilter;


public class RawPattern {

	private List<String> left;
	private List<String> right;
	private List<String> middle;

	public RawPattern(Sentence sentence, String first,String second)
			throws IOException {

		int leftTokens=5;
		int rightTokens=5;
		
		left = new ArrayList<String>();
		right = new ArrayList<String>();
		middle = new ArrayList<String>();

		int firstIndex = -1;
		int secondIndex = -1;
		for (int i = 0; i < sentence.getTaggedSentence().size(); i++) {
			TokenTaggerPair pair = sentence.getTaggedSentence().get(i);
			if (pair.getToken().startsWith(first)) {
				firstIndex = i;
				break;
			}
		}
		for (int i = 0; i < sentence.getTaggedSentence().size(); i++) {
			TokenTaggerPair pair = sentence.getTaggedSentence().get(i);
			if (second.endsWith(pair.getToken())) {
				secondIndex = i;
				break;
			}
		}
		if (firstIndex == -1 || secondIndex == -1)
			return;

		// constraints 1
		int e1ToE2Distance = Math.abs(secondIndex - firstIndex);
		if (e1ToE2Distance > 10)
			return;

		// constraints 2
		int middleEntityNum = 0;

		// constraints 3
		int middlePruncNum = 0;
		
		//constraints 4
		if (sentence.getTaggedSentence().size()>50)
			return;

		for (int i = 0; i>firstIndex-leftTokens && i < firstIndex; i++) {
			TokenTaggerPair pair = sentence.getTaggedSentence().get(i);
			if ((pair.getPosTagger().equals("名词") || pair.getPosTagger()
					.equals("动词"))
					&& pair.getNerTagger().equals("")
					&& !StopWordsFilter.isStopWord(pair.getToken())
					&& first.indexOf(pair.getToken()) < 0
					&& second.indexOf(pair.getToken()) < 0)
				left.add(pair.getToken());
		}
		for (int i = secondIndex + 1; i<secondIndex+rightTokens && i < sentence.getTaggedSentence().size(); i++) {
			TokenTaggerPair pair = sentence.getTaggedSentence().get(i);
			if ((pair.getPosTagger().equals("名词") || pair.getPosTagger()
					.equals("动词"))
					&& pair.getNerTagger().equals("")
					&& !StopWordsFilter.isStopWord(pair.getToken())
					&& first.indexOf(pair.getToken()) < 0
					&& second.indexOf(pair.getToken()) < 0)
				right.add(pair.getToken());
		}
		for (int i = firstIndex + 1; i < secondIndex; i++) {
			TokenTaggerPair pair = sentence.getTaggedSentence().get(i);
			if ((pair.getPosTagger().equals("名词") || pair.getPosTagger()
					.equals("动词"))
		//			&& pair.getNerTagger().equals("")
					&& !StopWordsFilter.isStopWord(pair.getToken())
					&& first.indexOf(pair.getToken()) < 0
					&& second.indexOf(pair.getToken()) < 0)
				middle.add(pair.getToken());
			if (!pair.getNerTagger().equals(""))
				middleEntityNum++;
			if (pair.getPosTagger().equals("标点"))
				middlePruncNum++;
		}

		// //constraints 2
		if (middleEntityNum > 0) {
			left.clear();
			middle.clear();
			right.clear();
			return;
		}

		// constraints 3
		if (middlePruncNum > 0) {
			left.clear();
			middle.clear();
			right.clear();
			return;
		}

	}

	public boolean emptyWords() {
		return left.size() == 0 && middle.size() == 0 && right.size() == 0;
	}

	public List<String> getLeft() {
		return left;
	}

	public List<String> getRight() {
		return right;
	}

	public List<String> getMiddle() {
		return middle;
	}

	public void print() {
		System.out.print("left\t");
		for (String s:left)
			System.out.print(s+"\t");
		System.out.println();
		System.out.print("middle\t");
		for (String s:middle)
			System.out.print(s+"\t");
		System.out.println();
		System.out.print("right.\t");
		for (String s:right)
			System.out.print(s+"\t");
		System.out.println();
	}

}