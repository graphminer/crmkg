package edu.ecnu.kg.snowball.nlp;

import edu.ecnu.kg.common.SnowballConfig;
import edu.fudan.nlp.cn.tag.CWSTagger;
import edu.fudan.nlp.cn.tag.NERTagger;
import edu.fudan.nlp.cn.tag.POSTagger;
import edu.fudan.util.exception.LoadModelException;

public class Taggers {
	
	private static CWSTagger cwsTagger;
	private static POSTagger posTagger;
	private static NERTagger nerTagger;

	public static CWSTagger getCWSTagger() throws LoadModelException {
		if (cwsTagger == null) {
			cwsTagger = new CWSTagger(SnowballConfig.getModelPath()+"/seg.m");
		}
		return cwsTagger;
	}

	public static POSTagger getPOSTagger() throws LoadModelException {
		if (posTagger == null) {
			posTagger = new POSTagger(getCWSTagger(), SnowballConfig.getModelPath()+"/pos.m");
		}
		return posTagger;
	}

	public static NERTagger getNERTagger() throws Exception {
		if (nerTagger == null) {
			nerTagger = new NERTagger(SnowballConfig.getModelPath()+"/seg.m", SnowballConfig.getModelPath()+"/pos.m");
		}
		return nerTagger;
	}
	
}
