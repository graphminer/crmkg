package edu.ecnu.kg.snowball.filter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import edu.ecnu.kg.common.SnowballConfig;


public class StopWordsFilter {

	private static Set<String> words;

	public static boolean isStopWord(String s) throws IOException {
		if (words == null) {
			words = new HashSet<String>();
			BufferedReader br = new BufferedReader(new FileReader(
					SnowballConfig.getStopWordsPath()));
			String line;
			while ((line = br.readLine()) != null)
				words.add(line);
			br.close();
		}
		return words.contains(s);
	}

}
