package edu.ecnu.kg.snowball.graph;

/** 
 *  
 * @author CBD_23  2014年6月20日
 * @version 0.0.2
 * @modify 
 * @Copyright 华东师范大学数据科学与工程院版权所有
 */
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.stream.XMLStreamException;

import edu.ecnu.kg.common.SimpleEdge;
import edu.ecnu.kg.common.SimpleNode;
import edu.ecnu.kg.common.graphml.GraphMLTokens;
import edu.ecnu.kg.common.graphml.GraphmlKey;
import edu.ecnu.kg.tools.writer.GraphMLWriter;

public class AddAll {      //node and strong relations
    
    public static void main(String args[]) throws IOException, XMLStreamException{
        String entityFilePath = "/home/bingo/data/entities.txt"; 
        String strongInput="/home/bingo/data/final.txt";
        String weakInput="/home/bingo/data/co-relations.txt";    
        String nodeMLPath = "/home/bingo/data/allRelations.xml";
        addAll(entityFilePath,strongInput,weakInput,nodeMLPath);
    }
    
    public static void addAll(String entityFilePath,String strongInput,String weakInput,String nodeMLPath) throws IOException, XMLStreamException{
        InputStreamReader isr = new InputStreamReader(new FileInputStream(entityFilePath), "utf-8"); 
        BufferedReader br = new BufferedReader(isr);
        InputStreamReader isrd = new InputStreamReader(new FileInputStream(strongInput), "utf-8"); 
        BufferedReader brd = new BufferedReader(isrd);
        InputStreamReader isrdr = new InputStreamReader(new FileInputStream(weakInput), "utf-8"); 
        BufferedReader brdr = new BufferedReader(isrdr);
        String line;
        int id=1;
        int eid=1;
        GraphMLWriter writer = new GraphMLWriter(new FileOutputStream(nodeMLPath));
        writer.writeHeader();
        writer.writeGraphStart();
        ArrayList<SimpleNode> nodes = new ArrayList<SimpleNode>();
        ArrayList<SimpleEdge> edges = new ArrayList<SimpleEdge>();
        ArrayList<GraphmlKey> nodekeys = new ArrayList<GraphmlKey>();  
        
        GraphmlKey key1 = new GraphmlKey();
        key1.setId("k1");
        key1.setName("name");
        key1.setType(GraphMLTokens.STRING);
        nodekeys.add(key1);

        GraphmlKey key2 = new GraphmlKey();
        key2.setId("k2");
        key2.setName("count");
        key2.setType(GraphMLTokens.STRING);
        nodekeys.add(key2);
        
        writer.writeVertexKeyset(nodekeys);
       
        HashMap<String,String> store = new HashMap<String,String>();
        
        while((line=br.readLine())!=null){ // first input nodes with ID
            String temp[] = line.split("\t");
            store.put(temp[0], Integer.toString(id++));
            SimpleNode node = new SimpleNode();
            node.setId(store.get(temp[0]));
            node.addProperty("name", temp[0]);
            node.addLabel(temp[1]);
            node.addProperty("count", temp[2]);
            nodes.add(node);
            System.out.println("Already node "+(id-1));
        }
        
        while((line=brd.readLine()) != null){//second strong relations with ID
            String tp[] = line.split("\t");
            String sourceID=null;
            String targetID=null;
            SimpleEdge edge = new SimpleEdge();
            if(tp.length == 5 ){// not seed
                if(store.containsKey(tp[0])){
                    sourceID = store.get(tp[0]);
                    System.out.println("Get node id "+sourceID);
                }else{
                    store.put(tp[0], Integer.toString(id++));
                    SimpleNode node = new SimpleNode();
                    sourceID = store.get(tp[0]);
                    node.setId(sourceID);
                    node.addProperty("name", tp[0]);
                    nodes.add(node);
                    System.out.println("Already node "+(id-1));
                }
                
                if(store.containsKey(tp[1])){
                    targetID=store.get(tp[1]);
                    System.out.println("Get node id "+targetID);
                }else{
                    store.put(tp[1], Integer.toString(id++));
                    SimpleNode node = new SimpleNode();
                    targetID=store.get(tp[1]);
                    node.setId(targetID);
                    node.addProperty("name", tp[1]);
                    nodes.add(node);
                    System.out.println("Already node "+(id-1));
                }
                
                edge.setId(Integer.toString(eid++));
                edge.setLabel(tp[2]);
                edge.setSourcenode(sourceID);
                edge.setTargetnode(targetID);
                edge.addProperty("source", tp[3]);
                edge.addProperty("weight", tp[4]);
                edges.add(edge);
                System.out.println("Already edge "+(eid-1));        
            }else if(tp.length==4){//seed
                if(store.containsKey(tp[0])){
                    sourceID = store.get(tp[0]);
                    System.out.println("Get node id "+sourceID);
                }else{
                    store.put(tp[0], Integer.toString(id++));
                    SimpleNode node = new SimpleNode();
                    sourceID = store.get(tp[0]);
                    node.setId(sourceID);
                    node.addProperty("name", tp[0]);
                    nodes.add(node);
                    System.out.println("Already node "+(id-1));
                }
                
                if(store.containsKey(tp[1])){
                    targetID=store.get(tp[1]);
                    System.out.println("Get node id "+targetID);
                }else{
                    store.put(tp[1], Integer.toString(id++));
                    SimpleNode node = new SimpleNode();
                    targetID=store.get(tp[1]);
                    node.setId(targetID);
                    node.addProperty("name", tp[1]);
                    nodes.add(node);
                    System.out.println("Already node "+(id-1));
                }
                
                edge.setId(Integer.toString(eid++));
                edge.setLabel(tp[2]);
                edge.setSourcenode(sourceID);
                edge.setTargetnode(targetID);
                edge.addProperty("seed", tp[3]);
                edges.add(edge);
                System.out.println("Already edge "+(eid-1)+" and it is a seed type..");
            }
        }
        
        while((line=br.readLine())!=null){//third weak relations with ID
            String temp[] = line.split("\t");
            String sourceID=null;
            String targetID=null;
            if(temp.length == 5){
                if(store.containsKey(temp[0])){   // new node
                    sourceID = store.get(temp[0]);
                    System.out.println("Get node id "+sourceID);
                }else{//others to be identical
                    store.put(temp[0], Integer.toString(id++));
                    SimpleNode node1 = new SimpleNode();
                    sourceID = store.get(temp[0]);
                    node1.setId(sourceID);
                    node1.addProperty("name", temp[0]);
                    node1.addLabel(temp[1]);
                    nodes.add(node1);
                    System.out.println("Already node "+(id-1));
                }
                
                
                if(store.containsKey(temp[2])){ //new node
                    targetID = store.get(temp[2]);
                    System.out.println("Get node id "+targetID);
                }else{//others to be identical
                    store.put(temp[2], Integer.toString(id++));
                    SimpleNode node2 = new SimpleNode();
                    targetID = store.get(temp[2]);
                    node2.setId(targetID);
                    node2.addProperty("name", temp[2]);
                    node2.addLabel(temp[3]);
                    nodes.add(node2);
                    System.out.println("Already node "+(id-1));
                }
                
                SimpleEdge edge = new SimpleEdge();
                edge.setId(Integer.toString(eid++));
                edge.setLabel("RelateTo");
                edge.setSourcenode(sourceID);
                edge.setTargetnode(targetID);
                edge.addProperty("weight", temp[4]);
                edges.add(edge);
                System.out.println("Already edge "+(eid-1));
            }
        }
        
        writer.writerNodes(nodes);
        writer.writeEdges(edges);
        writer.writeTail();
        System.out.println("Please check the ML file!");
    }
}

