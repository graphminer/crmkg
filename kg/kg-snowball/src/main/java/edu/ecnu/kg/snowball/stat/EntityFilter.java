package edu.ecnu.kg.snowball.stat;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import edu.ecnu.kg.common.SnowballConfig;

public class EntityFilter {

	public static void main(String[] args) throws IOException {
		PrintWriter pw=new PrintWriter(SnowballConfig.getEntityCountFilterPath(),"utf-8");
		InputStreamReader isr = new InputStreamReader(new FileInputStream(SnowballConfig.getEntityCountPath()), "utf-8");
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line=br.readLine())!=null) {
			String[] items=line.split("\t");
			String[] data=items[0].split("/");
			String entity=data[0];
			String type=data[2];
			int count=Integer.parseInt(items[1]);
			if (!filter(entity, count)) {
				pw.println(entity+"\t"+type+"\t"+count);
				pw.flush();
			}
		}
		br.close();
		pw.close();
	}
	
	public static boolean filter(String entity,int count) {
		if (entity.contains("微博"))
			return true;
		if (entity.contains("新浪"))
			return true;
		if (count<=5)
			return true;
		if (entity.length()<=1)
			return true;
		char[] chars=entity.toCharArray();
		for (char c:chars) {	
			Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
		    if (!(ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
		            || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
		            || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
		            || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
		            || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
		            || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS))
		        return true;
		}
		return false;
	}

}