package edu.ecnu.kg.similarity;

public class EditDistance {
  /**
   * Levenshtein Distance(LD)
   * 
   * @param str1 str1
   * @param str2 str2
   * @return ld
   */
  public static int editDistance(String str1, String str2) {
    // Distance
    int[][] d;
    int n = str1.length();
    int m = str2.length();
    int i; // iterate str1
    int j; // iterate str2
    char ch1; // str1
    char ch2; // str2
    int temp;
    if (n == 0) {
      return m;
    }
    if (m == 0) {
      return n;
    }
    d = new int[n + 1][m + 1];
    for (i = 0; i <= n; i++) {
      d[i][0] = i;
    }
    for (j = 0; j <= m; j++) {
      d[0][j] = j;
    }
    for (i = 1; i <= n; i++) {
      ch1 = str1.charAt(i - 1);
      // match str2
      for (j = 1; j <= m; j++) {
        ch2 = str2.charAt(j - 1);
        if (ch1 == ch2) {
          temp = 0;
        } else {
          temp = 1;
        }
        d[i][j] = min(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + temp);
      }
    }
    return d[n][m];
  }

  private static int min(int one, int two, int three) {
    int min = one;
    if (two < min) {
      min = two;
    }
    if (three < min) {
      min = three;
    }
    return min;
  }


  /**
   * @param str1 str1
   * @param str2 str2
   * @return sim
   */
  public static double sim(String str1, String str2) {
    int ld = editDistance(str1, str2);
    return 1 - (double) ld / Math.max(str1.length(), str2.length());
  }

  /**
   * 
   * @param args
   */
  public static void main(String[] args) {
    double alpha=0.99;
    double beta=0.01;
//    String[] sentences =
//        {"阿里巴巴集团业务架构将拆分为25个事业部", "阿里巴巴否认马云今年退休：那是没影儿的事", "阿里巴巴宣布马云辞任CEO 专任阿里董事局主席",
//            "马云辞阿里巴巴CEO 专任集团董事会主席","马云将于5月10日卸任阿里巴巴CEO","马云将辞阿里巴巴CEO 继任者需有牺牲精神","阿里巴巴宣布：马云5月10日起不再任CEO职务",
//            "马云将辞去阿里巴巴CEO一职","马云辞去阿里巴巴CEO","阿里巴巴重组5天后马云卸任CEO 新CEO人选隐现","阿里巴巴高管层或大换血 整体上市加速"};
    
    
    
    String[] sentences =
      {"消息称阿里巴巴将以70亿美元向雅虎回购20%股权", "阿里巴巴集团与雅虎签订回购20%股份协议", "阿里巴巴71亿美元回购雅虎所持20%股权",
          "阿里巴巴宣布71亿美元回购雅虎所持20%股权","雅虎和阿里巴巴达成股份回购协议","雅虎和阿里巴巴达成股份回购协议","马云高价拿回阿里巴巴控制权 估值下限350亿美元",
          "马云统一阿里巴巴治权和股权 创始人团队完胜","阿里巴巴宣布回购雅虎所持20%股权","阿里巴巴71亿美元拿回控制权","阿里巴巴71亿美元回购雅虎所持股权","分析称阿里巴巴一年内或整体上市","阿里巴巴71亿美元回购雅虎所持1/2股份"};
    int k=0;
    double[] scores=new double[sentences.length];
    for (int i = 0; i <sentences.length; i++) {
      k++;
      double score=0.0;
      for (int j = 0; j < sentences.length; j++) {
        if (i != j) {
          score += EditDistance.sim(sentences[i], sentences[j]);
        }
      }
      score +=alpha*score/sentences.length+beta*(1/k);
      scores[i]=score;
    }
   
    double max=0;
    int index=0;
    for(int i=0;i<scores.length;i++){
      if(scores[i]>max){
        max=scores[i];
        index=i;
      }
    }
    System.out.println("index :"+index+"\ttitle"+sentences[index]);
  }
}
