package edu.ecnu.kg.clustering.datapoint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import edu.ecnu.kg.clustering.datapoint.DataPoint;

public class Cluster {
	private List<DataPoint> dataPoints = new ArrayList<DataPoint>(); // 类簇中的样本点
    private String clusterName;
    private DataPoint centerPoint;
    private long earlistTime;
    
    public long getEarlistTime() {
      return earlistTime;
    }
    public void setEarlistTime(long earlistTime) {
      this.earlistTime = earlistTime;
    }
    public List<DataPoint> getDataPoints() {
        return dataPoints;
    }
    public void setDataPoints(List<DataPoint> dataPoints) {
        this.dataPoints = dataPoints;
    }
    public String getClusterName() {
        return clusterName;
    }
    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }
    public DataPoint getCenterPoint(){
    	centerPoint=new DataPoint();
    	HashMap<Integer,Double> map=new HashMap<Integer,Double>();
    	for(int i=0;i<dataPoints.size();i++){
    		DataPoint tempPoint =dataPoints.get(i);
    		Map<Integer,Double> vector= tempPoint.getVector();
    		Set<Entry<Integer, Double>> entrySet=vector.entrySet();
    		for(Entry<Integer, Double> entry : entrySet){
    			if(map.containsKey(entry.getKey())){
    				map.put(entry.getKey(), map.get(entry.getKey())+entry.getValue());
    			}else{
    				map.put(entry.getKey(), entry.getValue());
    			}
    		}
    	}
    	centerPoint.setCluster(this);
    	centerPoint.setDataPointName(clusterName);
    	centerPoint.setVector(map);
    	return centerPoint;
    }
}
