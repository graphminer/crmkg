package edu.ecnu.kg.timeline;

import java.util.ArrayList;
import java.util.List;

public class TimeLine {
  @Override
  public String toString() {
    
    StringBuffer sb=new StringBuffer();
    sb.append("TimeLine [companyName=" + companyName + ", events=");
    for(int i=0;i<events.size();i++){
      Event event=events.get(i);
      sb.append(event.toString());
    }
    sb.append("]");
    return sb.toString();
  }
  String companyName="";    //公司ID

  public String getCompanyName() {
    return companyName;
  }
  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }
  public List<Event> getEvents() {
    return events;
  }
  public void setEvents(List<Event> events) {
    this.events = events;
  }
  List<Event> events=new ArrayList<Event>();   //事件列表
}
