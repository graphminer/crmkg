package edu.ecnu.kg.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import edu.ecnu.kg.common.StringHelper;
import edu.ecnu.kg.data.process.XmlDomReader;
import edu.ecnu.kg.timeline.Event;
import edu.ecnu.kg.timeline.SubEvent;
import edu.ecnu.kg.timeline.TimeLine;

public class EventDAO {


  public void insertTimeLine(TimeLine timeline) {

    String companyName = timeline.getCompanyName();
    List<Event> bigEvents = timeline.getEvents();
    boolean isTimeline = false;
    int timelineId = 0;
    int bigEventId = 0;
    if (bigEvents.size() == 0) {
      return;
    } else {
      for (int i = 0; i < bigEvents.size(); i++) {
        Event event = bigEvents.get(i);
        List<SubEvent> seqEvents = event.getSeqEvents();
        String eventKeyWords = event.getKeywords();
        if (seqEvents.size() > 0) {
          if (!isTimeline) {
            timelineId =
                insertWithSqlGetId("INSERT INTO `timeline1` (`companyName`) VALUES ('"
                    + companyName + "'); ");
            isTimeline = true;
          }
          bigEventId =
              insertWithSqlGetId("INSERT INTO `bigevent1` (`timeLineId`, `eventKeyWords`,`startTime`,`newsCount`) VALUES ("
                  + timelineId + ",'" + eventKeyWords + "','"+event.getStartTime()+"',"+event.getNewsCount()+");");
          for (int j = 0; j < seqEvents.size(); j++) {
            SubEvent subEvent = seqEvents.get(j);
            insertWithSqlGetId("INSERT INTO `subevent1` (`newsTitle`, `newsUrl`,`newsMedia`,`newsTime`,`newsSummary`,`bigEventId`,`newsCount`) "
               + "VALUES ('"+subEvent.getNewsTitle()+"','"+subEvent.getNewsUrl()+"','"+subEvent.getNewsMedia()+"','"+subEvent.getNewsTime()+"','"+subEvent.getSummary()+"',"+bigEventId+","+subEvent.getNewsCount()+"); ");
          }
        }
      }
    }
  }

  public void queryTimeLineByKeyWords(String keywords) {
    String sql =
        "select eventKeyWords from bigevent where timeLineId =(select id from timeline where companyName='"
            + keywords + "');";
    ResultSet resultSet = excuteQurey(sql);

    List<String> keyWords = new ArrayList<String>();
    try {
      while (resultSet.next()) {
        keyWords.add(resultSet.getString("eventKeyWords"));

      }
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

  public ResultSet excuteQurey(String sql) {
    Connection conn = DBconnector.getConnection();
    StringBuffer sbsql = new StringBuffer();
    sbsql.append(sql);
    PreparedStatement pstmt = null;
    try {
      pstmt = conn.prepareStatement(sbsql.toString());
      ResultSet resultSet = pstmt.executeQuery(sql);
      pstmt.close();
      conn.close();
      return resultSet;
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }

  public int insertWithSqlGetId(String sql) {

    int risultato = 0;
    Connection conn = DBconnector.getConnection();
    StringBuffer sbsql = new StringBuffer();
    sbsql.append(sql);

    PreparedStatement pstmt = null;
    try {
      pstmt = conn.prepareStatement(sbsql.toString(), PreparedStatement.RETURN_GENERATED_KEYS);
      int rows = pstmt.executeUpdate();
      if (rows != 1) {
        throw new SQLException("executeUpdate return value: " + rows);
      }
      ResultSet rs = pstmt.getGeneratedKeys();
      if (rs.next()) {
        risultato = rs.getInt(1);
      }
      pstmt.close();
      conn.close();
      return risultato;
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return -1;
  }


  public void insertQuery(String sql) {

  }

  public void batchInsertNews(String xmlpath, int batchcount) {
    NodeList nl;
    XmlDomReader xdr;
    try {
      int startRowNum = 0;
      String title = "";
      String time = "";
      String media = "";
      String url = "";
      String description = "";
      xdr = new XmlDomReader(xmlpath);
      nl = xdr.getElementsByTagName("news");
      System.out.println(nl.getLength());
      Connection conn = DBconnector.getConnection();
      for (int i = 0; i < nl.getLength(); i++) {

        Element element = (Element) nl.item(i);
        if (element.getElementsByTagName("title") != null
            && element.getElementsByTagName("title").item(0) != null) {
          title =
              StringHelper.null2String(element.getElementsByTagName("title").item(0)
                  .getTextContent());
        }
        if (element.getElementsByTagName("time") != null
            && element.getElementsByTagName("time").item(0) != null) {
          time =
              StringHelper.null2String(element.getElementsByTagName("time").item(0)
                  .getTextContent());
        }
        if (element.getElementsByTagName("media") != null
            && element.getElementsByTagName("media").item(0) != null) {
          media =
              StringHelper.null2String(element.getElementsByTagName("media").item(0)
                  .getTextContent());
        }
        if (element.getElementsByTagName("url") != null
            && element.getElementsByTagName("url").item(0) != null) {
          url =
              StringHelper
                  .null2String(element.getElementsByTagName("url").item(0).getTextContent());
        }
        if (element.getElementsByTagName("description") != null
            && element.getElementsByTagName("description").item(0) != null) {
          description =
              StringHelper.null2String(element.getElementsByTagName("description").item(0)
                  .getTextContent());
        }

        conn.setAutoCommit(false);
        PreparedStatement pstmt =
            conn.prepareStatement("insert into new_table (newsTitle,newsTime,newsMedia,newsUrl,newsDescription) values(?,?,?,?,?)");
        pstmt.setString(1, title);
        pstmt.setString(2, time);
        pstmt.setString(3, media);
        pstmt.setString(4, url);
        pstmt.setString(5, description);
        pstmt.executeUpdate();
        startRowNum++;
        if (startRowNum % batchcount == 0) {// 如果数据条数达到10000条，则提交事务
          conn.commit();
          conn.close();
          // 重开链接
          conn = DBconnector.getConnection();
          conn.setAutoCommit(false);
          pstmt =
              conn.prepareStatement("insert into new_table (newsTitle,newsTime,newsMedia,newsUrl,newsDescription) values(?,?,?,?,?)");
          System.out.println("数据量达到" + batchcount + "条，提交事务完成。");
        }
      }
      // insert the last 10000
      conn.commit();
      conn.close();
    } catch (Throwable e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
}
