package edu.ecnu.kg.clustering.algorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import edu.ecnu.kg.clustering.datapoint.Cluster;
import edu.ecnu.kg.clustering.datapoint.DataPoint;
import edu.ecnu.kg.similarity.Similarity;

public class HierarchyAnalysis {

	private HashMap<String, HashMap<String, Double>> pairwiseMap = new HashMap<String, HashMap<String, Double>>();
	HashMap<String, Cluster> finalClusters = new HashMap<String, Cluster>();

	public List<Cluster> startAnalysis(List<DataPoint> dataPoints,
			int ClusterNum) {
		List<Cluster> result = new ArrayList<Cluster>();
		finalClusters = initialCluster(dataPoints);
		double minDis = 1.0;
		int iterNum = finalClusters.size();
		double min = Double.MAX_VALUE;
		String mergeCnameA = "";
		String mergeCnameB = "";
		// init pairwiseMap
		Set<Entry<String, Cluster>> entrySet = finalClusters.entrySet();
		for (Entry<String, Cluster> entry : entrySet) {
			String entryName = entry.getKey();
			HashMap<String, Double> targetMaper = new HashMap<String, Double>();
			for (Entry<String, Cluster> entry1 : entrySet) {
				String entry1Name = entry1.getKey();
				if (!entryName.equals(entry1Name)) {
					minDis = Similarity.getClusterDis(
							finalClusters.get(entryName),
							finalClusters.get(entry1Name));
					targetMaper.put(entry1Name, minDis);
					if (minDis < min) {
						min = minDis;
						mergeCnameA = entryName;
						mergeCnameB = entry1.getKey();
					}
				}
			}
			pairwiseMap.put(entryName, targetMaper);
		}

		// start to iterate
		while (iterNum > ClusterNum) {
			min = Double.MAX_VALUE;
			if (finalClusters.size() % 10 == 0) {
				System.out.println("cluster Num:" + finalClusters.size()
						+ "\tmin distance:" + minDis);
			}
			Cluster newCluster = mergeCluster(mergeCnameA, mergeCnameB);
			HashMap<String, Double> targetMaper = new HashMap<String, Double>();
			Set<String> keySet = finalClusters.keySet();
			for (String key : keySet) {
				if (!key.equals(newCluster.getClusterName())) {
					minDis = Similarity.getClusterDis(finalClusters.get(key),
							newCluster);
					pairwiseMap.get(key).put(newCluster.getClusterName(),
							minDis);
					targetMaper.put(key, minDis);
				}
			}
			pairwiseMap.put(String.valueOf(newCluster.getClusterName()),
					targetMaper);
			// 合并cluster[mergeIndexA]和cluster[mergeIndexB]
			mergeCnameA = searchHashMinVal()[0];
			mergeCnameB = searchHashMinVal()[1];
			iterNum--;
		}// end while

		entrySet = finalClusters.entrySet();
		for (Entry<String, Cluster> entry : entrySet) {
			result.add(entry.getValue());
		}
		return result;
	}

	public String[] searchHashMinVal() {
		String[] indexs = new String[2];
		String indexA = "";
		String indexB = "";
		double min = Double.MAX_VALUE;
		Set<Entry<String, HashMap<String, Double>>> entrySet = pairwiseMap
				.entrySet();
		for (Entry<String, HashMap<String, Double>> entry : entrySet) {
			String i = entry.getKey();
			Set<Entry<String, Double>> hashSet = entry.getValue().entrySet();
			for (Entry<String, Double> hash : hashSet) {
				String j = hash.getKey();
				double score = hash.getValue();
				if (score < min) {
					min = score;
					indexA = i;
					indexB = j;
				}
			}
		}
		indexs[0] = indexA;
		indexs[1] = indexB;
		return indexs;
	}
	
    public List<Cluster> doHierarchy(List<DataPoint> dataPoints){
      
      //事件序列的时间窗口，此处设置为5天
      double timeWindow=5;
      int miniCount=4;
      List<Cluster> finalClusters = initialEventCluster(dataPoints);
      while(true){
        double minSim=Double.MAX_VALUE;
        int mini=0;
        int minj=0;
        for (int i = 0; i < finalClusters.size(); i++) {
          for (int j = 0; j < finalClusters.size() && j!=i; j++) {
              double minDis=Similarity.getClusterSim(finalClusters.get(i),finalClusters.get(j));
              if(minDis<minSim){
                minSim=minDis;
                mini=i;
                minj=j;
              }
          }
        }
        
        if(minSim <= timeWindow){
          finalClusters = mergeCluster(finalClusters, mini,
            minj);
        }else{
          break;
        }
      }
      
      //如果事件聚类出来的新闻报道小于miniCount
      for(int i=0;i<finalClusters.size();i++){
        if(finalClusters.get(i).getDataPoints().size()<miniCount){
          finalClusters.remove(i);
        }
      }
      return finalClusters;
    }
   
	 private List<Cluster> mergeCluster(List<Cluster> clusters, int mergeIndexA,
         int mergeIndexB) {
     if (mergeIndexA != mergeIndexB) {
         // 将cluster[mergeIndexB]中的DataPoint加入到 cluster[mergeIndexA]
         Cluster clusterA = clusters.get(mergeIndexA);
         Cluster clusterB = clusters.get(mergeIndexB);
         List<DataPoint> dpA = clusterA.getDataPoints();
         List<DataPoint> dpB = clusterB.getDataPoints();
         for (DataPoint dp : dpB) {
             DataPoint tempDp = new DataPoint();
             tempDp.setDataPointName(dp.getDataPointName());
             tempDp.setVector(dp.getVector());
             tempDp.setCluster(clusterA);
             dpA.add(tempDp);
         }
         clusterA.setDataPoints(dpA);
         // List<Cluster> clusters中移除cluster[mergeIndexB]
         clusters.remove(mergeIndexB);
     }
     return clusters;
	}

	private Cluster mergeCluster(String mergeCnameA, String mergeCnameB) {
		Cluster result = new Cluster();
		if (mergeCnameA != mergeCnameB) {
			// 将cluster[mergeIndexB]中的DataPoint加入到 cluster[mergeIndexA]
			Cluster clusterA = finalClusters.get(mergeCnameA);
			Cluster clusterB = finalClusters.get(mergeCnameB);
			List<DataPoint> dpA = clusterA.getDataPoints();
			List<DataPoint> dpB = clusterB.getDataPoints();
			for (DataPoint dp : dpB) {
				DataPoint tempDp = new DataPoint();
				tempDp.setDataPointName(dp.getDataPointName());
				tempDp.setVector(dp.getVector());
				tempDp.setCluster(clusterA);
				dpA.add(tempDp);
			}
			clusterA.setDataPoints(dpA);
			result = clusterA;

			// List<Cluster> clusters中移除cluster[mergeIndexB];
			finalClusters.remove(mergeCnameA);
			finalClusters.remove(mergeCnameB);
			finalClusters.put(mergeCnameA, clusterA);

			// update the pairwiseMap value
			Set<Entry<String, HashMap<String, Double>>> entrySet = pairwiseMap
					.entrySet();
			for (Entry<String, HashMap<String, Double>> entry : entrySet) {
				HashMap<String, Double> entryValue = entry.getValue();
				entryValue.remove(mergeCnameB);
				entryValue.remove(mergeCnameA);
			}
			pairwiseMap.remove(mergeCnameB);
			pairwiseMap.remove(mergeCnameA);

		}
		return result;
	}

	// 初始化类簇
	private HashMap<String, Cluster> initialCluster(List<DataPoint> dataPoints) {
		HashMap<String, Cluster> originalClusters = new HashMap<String, Cluster>();
		for (int i = 0; i < dataPoints.size(); i++) {
			DataPoint tempDataPoint = dataPoints.get(i);
			List<DataPoint> tempDataPoints = new ArrayList<DataPoint>();
			tempDataPoints.add(tempDataPoint);
			Cluster tempCluster = new Cluster();
			tempCluster.setClusterName(String.valueOf(i));
			tempCluster.setDataPoints(tempDataPoints);
			tempDataPoint.setCluster(tempCluster);
			originalClusters.put(String.valueOf(i), tempCluster);
		}
		return originalClusters;
	}

	// 初始化事件类簇
    private List<Cluster> initialEventCluster(List<DataPoint> dataPoints) {
        List<Cluster> originalClusters = new ArrayList<Cluster>();
        for (int i = 0; i < dataPoints.size(); i++) {
            DataPoint tempDataPoint = dataPoints.get(i);
            List<DataPoint> tempDataPoints = new ArrayList<DataPoint>();
            tempDataPoints.add(tempDataPoint);
            Cluster tempCluster = new Cluster();
            tempCluster.setClusterName(String.valueOf(i));
            tempCluster.setDataPoints(tempDataPoints);
            tempDataPoint.setCluster(tempCluster);
            originalClusters.add(tempCluster);
        }
        return originalClusters;
    }
}
