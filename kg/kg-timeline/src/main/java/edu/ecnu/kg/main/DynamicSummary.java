package edu.ecnu.kg.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.lucene.document.Document;

import edu.ecnu.kg.clustering.datapoint.Cluster;
import edu.ecnu.kg.clustering.datapoint.DataPoint;
import edu.ecnu.kg.common.SenSegmentation;
import edu.ecnu.kg.data.process.SmartChineseSegmenter;
import edu.ecnu.kg.data.process.TFIDFAgorithm;
import edu.ecnu.kg.lucene.search.Search;

public class DynamicSummary {
    static double lamda=0.2;
    static double mu=0.5;
    static double epsilon=0.5;
    static Map<String, Document> searchResult=null;
    
   
    public static String summariseContent(Cluster event,Map<String, Document> searchresult,int TopK){

      searchResult=searchresult;

      List<String> result=getSentenceCandaitate(event);
      HashMap<String,Double> SDMap =getSDMap(result);
      HashMap<String,Double> impMap=getImpMap(result);
      Set<Entry<String, Double>> SDSet=SDMap.entrySet();
      Set<Entry<String, Double>> impSet=impMap.entrySet();

      //计算每条句子最终的得分
      HashMap<String,Double> scoreMap=new HashMap<String,Double>();
      for(Entry<String, Double> SD:SDSet){
        String docNameSD=SD.getKey();
        double score=0.0;
        for(Entry<String, Double> imp:impSet){
          String docNameimp=imp.getKey();
          if(docNameSD.equals(docNameimp)){
            score=lamda*SD.getValue()+(1-lamda)*imp.getValue();
          }
        }
        scoreMap.put(docNameSD, score);
      }
      
      
      //每个类别都提取出三条句子最为摘要
      Set<Entry<String, Double>> scoreSet=scoreMap.entrySet();
      int count=0;
      HashSet<String> set=new HashSet<String>();
      while(count<TopK){
        double max=0;
        String maxDocName="";
      
        for(Entry<String, Double> score:scoreSet){
          String docName=score.getKey();
          if(!set.contains(docName)){
              double sco=score.getValue();
              if(sco>max){
                max=sco;
                maxDocName=docName;
              }
          }
        }
        set.add(maxDocName);
        String docName=maxDocName.substring(0, maxDocName.lastIndexOf("_"));
        int  sentenceId=Integer.valueOf(maxDocName.substring(maxDocName.lastIndexOf("_")+1,maxDocName.length()));
        Document doc=searchResult.get(docName);
        String summary=SenSegmentation.segSentence(doc.get("text")).get(sentenceId);
        String time=doc.get("time");
        String title=doc.get("title");
        String url=doc.get("url");
        System.out.println("title:"+title+"\ttime:"+time+"\tsummary:"+summary+"\turl:"+url);
        count++;
      }
      
      return "";
 }
    
    
    
    /***
     * 计算每条句子的重要度，分别从和全文相似度，和查询词相似度，和标题相似度，句子长度，以及句子的位置
     * @param list
     * @return
     */
    private static HashMap<String,Double> getImpMap(List<String> list){
      
      /***
       * parameters
       */
      double alpha=0.6;
      double beta=0.05;
      double upsilon=0.2;
      double delta=0.05;
      double gamma=0.1;
      
      /***
       * scores
       */
      double docSim=0.0;
      double querySim=0.0;
      double titleSim=0.0;
      int position=0;
      double score=0.0;
      HashMap<String,Double> result=new HashMap<String,Double>();
      for(int i=0;i<list.size();i++){
        String docName_Id=list.get(i);
        String docName=docName_Id.substring(0, docName_Id.lastIndexOf("_"));
        int Id=Integer.valueOf(docName_Id.substring(docName_Id.lastIndexOf("_")+1, docName_Id.length()));
        List<String[]> docSentences=TFIDFAgorithm.sentencesMap.get(docName);
        
        //得到句子
        String[] sentence=docSentences.get(Id);
        //得到全文
        String[] doc=TFIDFAgorithm.getAllSentenceByDocName(docName);
        
        //计算和全文的相似度
        docSim=sentenceCosSim(sentence,doc);
        
        
        //计算和query词的相似度
        String query=Search.query;
        String [] querySegs=SmartChineseSegmenter.smartChineseAnalyzerSegment(query);
        querySim=sentenceCosSim(sentence,querySegs);
        
        //计算和title的相似度
        String title= searchResult.get(docName).get("title");
        String [] titleSegs=SmartChineseSegmenter.smartChineseAnalyzerSegment(title);
        titleSim=sentenceCosSim(sentence,titleSegs);
        //计算句子的长度,就是sentence.length
        
        //计算句子的位置,第一条句子和最后一条句子
        if(Id==0 || Id==docSentences.size()-1){
          position=1;
        }else{
          position=0;
        }
        score=alpha*docSim+beta*querySim+upsilon*titleSim+delta*sentence.length+gamma*Double.valueOf(position);
        result.put(docName_Id, score);
      }
      return result;
    }
    
    /***
     * 计算每条句子的SD值
     * @param result
     * @return
     */
    private static HashMap<String,Double> getSDMap(List<String> result){
      //计算每条句子的SD值
      HashMap<String,Double> SDMap=new  HashMap<String,Double>();
      for(int i=0;i<result.size();i++){
        String docName_IdA=result.get(i);
        String docNameA=docName_IdA.substring(0, docName_IdA.lastIndexOf("_"));
        int IdA=Integer.valueOf(docName_IdA.substring(docName_IdA.lastIndexOf("_")+1, docName_IdA.length()));

        if(TFIDFAgorithm.sentencesMap.get(docNameA).size()==0){
          continue;
        }
        String[] sentenceA=TFIDFAgorithm.sentencesMap.get(docNameA).get(IdA);
        double sumWeight=0.0;
        for(int j=0;j<result.size();j++){
            if(i!=j){
              String docName_IdB=result.get(j);
              String docNameB=docName_IdB.substring(0, docName_IdB.lastIndexOf("_"));
              int IdB=Integer.valueOf(docName_IdB.substring(docName_IdB.lastIndexOf("_")+1, docName_IdB.length()));
              
              String[] sentenceB=TFIDFAgorithm.sentencesMap.get(docNameB).get(IdB);
              
              double sim=sentenceCosSim(sentenceA,sentenceB);
              if(sim>epsilon){
                sumWeight+=sim;
              }
            }
        }
        SDMap.put(docName_IdA, sumWeight);
      }
      return SDMap;
    }
    
    /***
     * 对于给定的cluster得到这个cluster中重要的句子所在的文档编号和在文档中的id
     * @param cluster
     * @return List<文档名称+句子ID>
     */
    private static List<String> getSentenceCandaitate(Cluster cluster){
      HashMap<String, List<String[]>> map=new HashMap<String, List<String[]>>();
      List<String> result=new ArrayList<String>();
      //给定Cluster,得到对应的sentence
      List<DataPoint> dataPoints=cluster.getDataPoints();
      for(int i=0;i<dataPoints.size();i++){
        DataPoint dp=dataPoints.get(i);
        String docName=dp.getDataPointName();
        
        List<String[]> list=TFIDFAgorithm.sentencesMap.get(docName);
        map.put(docName,list);
      }
     
      HashSet<String> set=new HashSet<String>();
      Set<Entry<String, List<String[]>>> newsSet=map.entrySet();
      
      HashMap<String, List<String[]>> alreadySet=new HashMap<String, List<String[]>>();
     
      for(Entry<String, List<String[]>> news : newsSet){
       
        String newsName=news.getKey();

        List<String[]> newsSentences=new ArrayList<String[]>();
        newsSentences=news.getValue();
        
        //取TopK个句子,这个TopK动态生成
        int TopK=(int) (mu*newsSentences.size())+1;
        
        //如果是第一个文档的话，取所有的句子
        List<Double> MaxValues=new ArrayList<Double>();
        
        Set<Entry<String, List<String[]>>> areadyNewsSet=alreadySet.entrySet();
        for(int i=0;i<newsSentences.size();i++){
          MaxValues.add(i,0.0);
          for(Entry<String, List<String[]>> alreadyentry : areadyNewsSet){
            List<String[]> preNewsSentences=new ArrayList<String[]>();
            preNewsSentences=alreadyentry.getValue();
                for(int j=0;j<preNewsSentences.size();j++){
                  double sim=getKLDivergence(newsSentences.get(i),preNewsSentences.get(j));
                  if(sim>MaxValues.get(i)){
                    MaxValues.set(i, sim);
                  }
                }
            }
        }
        List<Integer> indexs=new ArrayList<Integer>();
        //排序，并且选择Topk的句子
       int k=0;
       boolean[] flag=new boolean[MaxValues.size()];
       while(k<TopK){
        double min=Double.MAX_VALUE;
        int j=0;
        for(int i=0;i<MaxValues.size();i++){
          if(!flag[i]){
            if(MaxValues.get(i)<min){
              min=MaxValues.get(i);
              j=i;
            }
          }
        }
        flag[j]=true;
        indexs.add(j);
        List<String[]> alreadySentence=new ArrayList<String[]>();
        alreadySentence.add(newsSentences.get(j));
        alreadySet.put(newsName, alreadySentence);
        k++;
       }
       set.add(newsName);
       for(int i=0;i<indexs.size();i++){
         result.add(newsName+"_"+i);
       }
     }
      return result;
    }
    
    /***
     * 计算两个sentence的KL距离,此处是计算两个句子之间文本分布的差异情况，在过滤相似句子的时候使用
     * @param sentenceA
     * @param sentenceB
     * @return
     */
    private static double getKLDivergence(String[] sentenceA,String[] sentenceB){
      double klvalue=0.0;
      for(int i=0;i<sentenceA.length;i++){
        for(int j=0;j<sentenceB.length;j++){
          if(sentenceA[i].equals(sentenceB[j])){
            double Pwsa=Pw_s(sentenceA[i],sentenceA);
            double Pwsb= Pw_s(sentenceB[j],sentenceB);
            klvalue+=Pwsa*(Math.log(Pwsa/Pwsb)/Math.log(10));
          }
        }
      }
      return 1/(1+Math.pow(Math.E, klvalue));
    }
    
    /***
     * 计算一个词在一条句子中的概率
     * @param word
     * @param sentence
     * @return
     */
    private static double Pw_s(String word,String[] sentence){
      int wordFeq=0;
      for(int i=0;i<sentence.length;i++){
         if(word.equals(sentence[i])){
           wordFeq++;
         }
      }
      double result=Double.valueOf(wordFeq)/sentence.length;
      return result;
    }
    
    /***
     * 计算两个sentence之间的cos相似度，此处是计算两个句子之间的文本相似度
     * @param sentenceA
     * @param sentenceB
     * @return
     */
    private static double sentenceCosSim(String[] sentenceA,String[] sentenceB){
      double ModeA=0.0;
      double ModeB=0.0;
      double product=0.0;
      for(int i=0;i<sentenceA.length;i++){
        ModeA+=Math.pow(iftdfVariation(sentenceA[i],sentenceA),2);
        for(int j=0;j<sentenceB.length;j++){
          if(sentenceA[i].equals(sentenceB[j])){
            product+=iftdfVariation(sentenceA[i],sentenceA)*iftdfVariation(sentenceB[j],sentenceB);
          }
        }
      }
      for(int j=0;j<sentenceB.length;j++){
        ModeB+=Math.pow(iftdfVariation(sentenceB[j],sentenceB),2);
      }
      if(ModeA==0.0 || ModeB==0.0){
        return 0;
      }
      return product/(Math.sqrt(ModeA)*Math.sqrt(ModeB));
    }
    
    /***
     * 计算某一个词的改进的tfidf=(TFw,s)*IDFw,其中TFw,s代表word在句子s中出现的次数
     * @param word
     * @return
     */
    private static double iftdfVariation(String word,String[] sentence){
      int TFws=0;
      for(int i=0;i<sentence.length;i++){
        if(sentence[i].equals(word)){
            TFws++;
        }
      }
      if(TFIDFAgorithm.features.get(word)==null){
        return 0;
      }
      double idf=TFIDFAgorithm.wordIdfMap.get(TFIDFAgorithm.features.get(word));
      double result=Double.valueOf(TFws)*idf;
      return result;
      
    }
    
}
