package edu.ecnu.kg.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.lucene.document.Document;

import edu.ecnu.kg.clustering.algorithm.HierarchyAnalysis;
import edu.ecnu.kg.clustering.algorithm.KneansAnalysis;
import edu.ecnu.kg.clustering.datapoint.Cluster;
import edu.ecnu.kg.clustering.datapoint.DataPoint;
import edu.ecnu.kg.data.process.TFIDFAgorithm;
import edu.ecnu.kg.lucene.search.Search;
import edu.ecnu.kg.similarity.EditDistance;
import edu.ecnu.kg.timeline.Event;
import edu.ecnu.kg.timeline.SubEvent;
import edu.ecnu.kg.timeline.TimeLine;

public class ExtractEvent {

  
  static double alpha=0.9;
  static double beta=0.1;
  Map<String, Document> searchResult = new HashMap<String, Document>();
  Map<String, Map<Integer, Double>> tfidf =new HashMap<String, Map<Integer, Double>>();
  
  public TimeLine extractEvents(String keyword){
    
    TimeLine timeline=new TimeLine();
    //1、搜索文档  Search.searchDocuments("万科");
    searchResult=Search.searchDocuments(keyword);
    System.out.println("total result count is "+searchResult.size());
    if(searchResult.size()==0){
      return null;
    }
    
    //2、对文档的新闻进行TFIDF的计算
    tfidf=TFIDFAgorithm.tfidf(searchResult,"text");
    
    //3、将TFIDF对应的新闻稿map到DataPoint
    ArrayList<DataPoint> dataPoints=Search.builtDataPointSet(tfidf);
    
    //4、对DataPoint的集合做kmeans聚类,并且返回eventList
    List<Event> bigEventList= getEvents(dataPoints);
    
    timeline.setEvents(bigEventList);
    timeline.setCompanyName(keyword);
    return timeline;
  }
  
  
  private List<Event> getEvents(ArrayList<DataPoint> dataPointSet) {
    KneansAnalysis KmeansAnalysis = new KneansAnalysis();
    
    boolean isSmallEvent=false;
    
    List<Event> bigEventList=new ArrayList<Event>();
    //1、提取大事件，每一个蔟都是一个事件
    List<Cluster> bigEvents = KmeansAnalysis.doKmeans(dataPointSet);
    
    int newsCount=4;
    if(dataPointSet.size()<20){
      newsCount=0;
    }else{
      if(dataPointSet.size()<60){
        newsCount=2;
      }
    }
    
    
    //大事件
    for (Cluster bigEvent : bigEvents) {
      
      
      long MinTime=Long.MAX_VALUE;
      List<DataPoint> dps=bigEvent.getDataPoints();
      if (dps.size() < newsCount) continue;
      
      //提取大事件的关键字
//        extractKeyWords(dps);
        String keywords=searchResult.get(dps.get(0).getDataPointName()).get("title");
      //2、对于每一个大事件，使用时间序列提取时间发展过程，得到的结果List<Cluster>就是一个一个小的事件发展阶段,每一个Cluster就是一个小的事件阶段
        Event bEvent=new Event();
        bEvent.setNewsCount(bigEvent.getDataPoints().size());
        //给定一个重大事件，提取重大事件中的事件序列，使用基于时间窗口的聚类
        HierarchyAnalysis HAnalysis=new HierarchyAnalysis();
        List<Cluster> seqEvents=HAnalysis.doHierarchy(dps);
        
        
        List<SubEvent> subEvents=new ArrayList<SubEvent>();
        
        
        //小事件
        for(int i=0;i<seqEvents.size();i++){
          
         Cluster event=seqEvents.get(i);
            
         //如果某个事件的报道文章数小于阈值，则不认为是事件发展过程
         if(event.getDataPoints().size()<newsCount) continue;
         isSmallEvent=true;
         
         //3、对时间序列按照时间进行排序
         List<DataPoint> list=event.getDataPoints();
         System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
         Collections.sort(list, new Comparator<DataPoint>() {
           @Override
           public int compare(DataPoint o1, DataPoint o2) {
             // TODO Auto-generated method stub
             Document doc1 = searchResult.get(o1.getDataPointName());
             long time1 = Long.valueOf(doc1.get("time"));

             Document doc2 = searchResult.get(o2.getDataPointName());
             long time2 = Long.valueOf(doc2.get("time"));
             if (time1 >= time2) {
               return 1;
             }
             return -1;
           }
         });
//         System.out.println("-----subEventList-------"+event.getClusterName());
         long startTime=Long.valueOf(searchResult.get(list.get(0).getDataPointName()).get("time"));
         if(startTime<MinTime){
           MinTime=startTime;
         }

//         for(int j=0;j<list.size();j++){
//           Document doc=searchResult.get(list.get(j).getDataPointName());
//           System.out.println(doc.get("title")+"\t"+doc.get("time")+"\t"+doc.get("description")+"\t");
//         }
         event.setDataPoints(list);
         SubEvent subEvent=extractEvent(event);
         subEvents.add(subEvent);
         
       }
//        System.out.println("------------------------------------"+MinTime);
        if(isSmallEvent==true){
          bEvent.setKeywords(keywords);
          bEvent.setStartTime(String.valueOf(MinTime));
          bEvent.setSeqEvents(subEvents);
          bigEventList.add(bEvent);
        }
    }
    return bigEventList;
  }
  
  
  
  public SubEvent extractEvent(Cluster event){
    
    //1、将cluster转变成为Map<String, Document> docs
    Map<String, Document> docs=new HashMap<String, Document>();
    List<DataPoint> list=event.getDataPoints();
    for(int j=0;j<list.size();j++){
      DataPoint dp=list.get(j);
      String dpname=dp.getDataPointName();
      docs.put(dpname, searchResult.get(dpname));
    }
    SubEvent subEvent=new SubEvent();
    //计算每个新闻标题排名的时候，同时考虑某一个标题和所有其他标题的平均相似度和标题的时间这两个因素，alpha代表平均相似度的权值，beta代表新闻事件的权值
    double alpha=0.7;
    double beta=0.3;
    Set<Entry<String, Document>> docSet=docs.entrySet();
    HashSet<String> set =new HashSet<String>();
    int i=0;
    double MaxScore=0;
    String indexDocName="";
    
    //2、获取得分最高的标题，作为展示时间的新闻
    for(Entry<String, Document> docA:docSet){
      i++;
      String docNameA=docA.getKey();
      Document docContentA=docA.getValue();
      String titleA =docContentA.get("title");
      set.add(docNameA);
      double score=0.0;
      double tempscore=0.0;
      for(Entry<String, Document> docB:docSet){
        String docNameB=docB.getKey();
        if(!set.contains(docNameB)){
          Document docContentB=docB.getValue();
          String titleB =docContentB.get("title");
          tempscore+=EditDistance.sim(titleA,titleB);
        }
      }
      score=alpha*tempscore+beta*(1/i);
      if(score>MaxScore){
        MaxScore=score;
        indexDocName=docNameA;
      }
    }
    //3、构建事件，对新闻的 description字段再次提取摘要，此处得到摘要使用最简单的摘要实现版本，即SummariserAlgorithm.summarise
    Document doc=docs.get(indexDocName);
    subEvent.setNewsId(doc.get("id"));
    subEvent.setNewsTitle(doc.get("title"));
    subEvent.setNewsMedia(doc.get("media"));
    subEvent.setNewsUrl(doc.get("url"));
    subEvent.setNewsTime(doc.get("time"));
    subEvent.setSummary(SummariserAlgorithm.summarise(indexDocName, docs,"description",3));
    subEvent.setNewsCount(event.getDataPoints().size());
    return subEvent;
  }
  
  public void printSentences(HashMap<String, List<String[]>> sentencesMap){
    Set<Entry<String, List<String[]>>> entrySet=sentencesMap.entrySet();
    for(Entry<String, List<String[]>> entry : entrySet){
      String docName=entry.getKey();
      List<String[]> sentences=entry.getValue();
      System.out.println("----------"+docName+"----------");
      for(int i=0;i<sentences.size();i++){
        for(int j=0;j<sentences.get(i).length;j++){
          System.out.println(sentences.get(i)[j]);
        }
      }
    }
  }
}
