package edu.ecnu.kg.clustering.algorithm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.Vector;

import edu.ecnu.kg.clustering.datapoint.Cluster;
import edu.ecnu.kg.clustering.datapoint.DataPoint;
import edu.ecnu.kg.similarity.Similarity;


public class KneansAnalysis {

  /**
   * k-means
   * 
   * @param List<DataPoint> dataPoints 所有的点
   * @param int K 聚类的数量
   * @return List<Cluster> 聚类的结果
   * @throws IOException
   */


  
  public List<Cluster> doKmeans(List<DataPoint> dataPoints) {
    
    
    int tsLength = dataPoints.size();
    int step=4;
    int K = 1;
    if(dataPoints.size()<20){
      step=1;
    }
    else{
      if(dataPoints.size()<60){
        step=2;
    }
    }
  
    double gradientThreshold=1.5;
    double sumSqure = 1000;
//    boolean flag = false; // 是否开始进行二分查找
    List<Cluster> clusters;
    List<Cluster> preClusters = null;
    while (true) {
      
      
      // 1、使用随机初始化一个点，然后逐个最大化
      clusters = getRandomInitPoint(dataPoints, K);// 保存K个中心点
      
      double[][] distance = new double[tsLength][K];
      // 2、初始化K个聚类
      int[] assignMeans = new int[tsLength];// 记录所有点属于的聚类序号，初始化全部为0

      Map<Integer, Vector<Integer>> clusterMember = new TreeMap<Integer, Vector<Integer>>();// 记录每个聚类的成员点序号
      Vector<Integer> mem = new Vector<Integer>();
      while (true) {
        // 3、计算每个点和每个聚类中心的距离
        for (int i = 0; i < tsLength; i++) {
          for (int j = 0; j < K; j++) {
            distance[i][j] = getDistance(dataPoints.get(i), clusters.get(j));
          }
        }
        
        // 4、找出每个点最近的聚类中心
        int[] nearestMeans = new int[tsLength];
        for (int i = 0; i < tsLength; i++) {
          nearestMeans[i] = findNearestMeans(distance, i);
        }

        // 5、如果前面条件不满足，那么需要重新聚类再进行一次迭代，需要修改每个聚类的成员和每个点属于的聚类信息
        clusterMember.clear();
        for (int i = 0; i < tsLength; i++) {
          assignMeans[i] = nearestMeans[i];
          if (clusterMember.containsKey(nearestMeans[i])) {
            clusterMember.get(nearestMeans[i]).add(i);
          } else {
            mem.clear();
            mem.add(i);
            Vector<Integer> tempMem = new Vector<Integer>();
            tempMem.addAll(mem);
            clusterMember.put(nearestMeans[i], tempMem);
          }
        }
        // 6、重新计算每个聚类的中心点!
        double tempSumMoveDis = 0.0;
        for (int i = 0; i < K; i++) {
          if (!clusterMember.containsKey(i)) {// kmeans可能产生空的聚类
            continue;
          }
          Cluster newCluster = computeNewMean(clusterMember.get(i), dataPoints, i);
          // 计算所有聚类的中心点移动距离之和
          
          
          tempSumMoveDis +=
              (1 - Similarity.getCosSim(newCluster.getCenterPoint(), clusters.get(i)
                  .getCenterPoint()));
          clusters.set(i, newCluster);
         
        }
        // 7、评价是否需要退出,当本次所有中心点的移动距离之和和上次移动距离之和的差别小于阈值0.001，就停止K=k情况下的聚类
        System.out.println("K:"+K+"\t avg move"+tempSumMoveDis/K);
        if (tempSumMoveDis < 0.1) {
          break;
        }
        
      }
      // 8、对K=k取值进行总体评估，计算所有蔟的失真函数值之和
      double sumSq = 0.0;
      for (int i = 0; i < K; i++) {
        sumSq += distortionFunction(clusters.get(i));
      }
      
      double currGradient=0.0;
      if(K==1){
        currGradient = Math.abs(sumSqure - sumSq) / 1;
      }
      else{
        currGradient = Math.abs(sumSqure - sumSq) / (step);
      }
        System.out.println(K + "\t" + sumSq + "\t" + currGradient);
        if (currGradient < gradientThreshold) {
          sumSqure = sumSq;
         break;
        } else {
          preClusters=clusters;
          sumSqure = sumSq;
          if (K+step > dataPoints.size()) {
            break;
          }
         K=K+step;
        }
    }
    System.out.println("the K is between " + (K-step) + "~" + K);
    System.out.println("the sum distortion of the result clusters is " + sumSqure);
    return preClusters;
  }

  
  /**
   * 采用平均法更新 新的聚类
   * 
   * @param clusterM 该聚类所包含的所有的点的ID Vector<ID>
   * @param dataPoints 所有测试样例的<DataPoint>
   * @param index 该cluster的ID
   * @return Cluster 新的聚类
   * @throws IOException
   */
  private Cluster computeNewMean(Vector<Integer> clusterM, List<DataPoint> dataPoints, int index) {
    Cluster newCluster = new Cluster();
    List<DataPoint> dps = new ArrayList<DataPoint>();
    for (Iterator<Integer> it = clusterM.iterator(); it.hasNext();) {
      int me = it.next();
      dps.add(dataPoints.get(me));
    }
    newCluster.setDataPoints(dps);
    newCluster.setClusterName(String.valueOf(index));
    return newCluster;
  }

  /**
   * 找出距离当前点最近的聚类中心
   * 
   * @param double[][] 点到所有聚类中心的距离
   * @return i 最近的聚类中心的序 号
   * @throws IOException
   */
  private int findNearestMeans(double[][] distance, int m) {
    // TODO Auto-generated method stub
    double minDist = 10;
    int j = 0;
    for (int i = 0; i < distance[m].length; i++) {
      if (distance[m][i] < minDist) {
        minDist = distance[m][i];
        j = i;
      }
    }
    return j;
  }

  /**
   * 计算一个点和某一个聚类的中心点的距离
   * 
   * @param DataPoint 点dp
   * @param cluster 某一个聚类
   * @return double 1-Cos(d1,d2);
   */
  private double getDistance(DataPoint dp, Cluster cluster) {
    // TODO Auto-generated method stub
    DataPoint centerPoint = cluster.getCenterPoint();
    return 1 - Similarity.getCosSim(dp, centerPoint);
  }

  /***
   * 此处需要修改
   * 
   * @param dataPoints
   * @param K
   * @return
   */
  @SuppressWarnings("unused")
  private List<Cluster> getInitPoint(List<DataPoint> dataPoints, int K) {
    // TODO Auto-generated method stub
    int index = 0;
    Random r = new Random();
    List<Cluster> result = new ArrayList<Cluster>();
    for (int i = 0; i < dataPoints.size(); i++) {
      if (i == index * dataPoints.size() / K) {
        DataPoint tempDataPoint = dataPoints.get(i);
        List<DataPoint> tempDataPoints = new ArrayList<DataPoint>();
        tempDataPoints.add(tempDataPoint);
        Cluster tempCluster = new Cluster();
        tempCluster.setClusterName(String.valueOf(index));
        tempCluster.setDataPoints(tempDataPoints);
        tempDataPoint.setCluster(tempCluster);
        result.add(index, tempCluster);
        index++;
      }
    }
    return result;
  }


  /***
   * 算法描述： 首先随机选择一个点作为第一个初始类簇中心点，然后选择距离该点最远的那个点作为第二个初始类簇中心点，
   * 然后再选择距离前两个点的最近距离最大的点作为第三个初始类簇的中心点，以此类推，直至选出K个初始类簇中心点。
   * 
   * @param dataPoints
   * @param K
   * @return
   */
  private List<Cluster> getRandomInitPoint(List<DataPoint> dataPoints, int K) {
    // TODO Auto-generated method stub
    List<Cluster> result = new ArrayList<Cluster>();
    Random r = new Random();
    int startIndex = Math.abs(r.nextInt(dataPoints.size()));
    int[] flag = new int[K];
    int l = 0;
    flag[l++] = startIndex;
    startIndex = 0;
    int i = 0;
    int j = 0;
    int h = 0;
    HashSet<Integer> set = new HashSet<Integer>();
    set.add(startIndex);
    // 迭代次数
    for (h = 0; h < K - 1; h++) {

      double Max_Value = Double.MIN_VALUE;

      // 从点集中选择一个节点作为下一个新的聚类中心点
      for (i = 0; i < dataPoints.size(); i++) {
        if (set.contains(i)) continue;
        double Min_Value = Double.MAX_VALUE;
        for (j = 0; j < flag.length; j++) {
          if (i == j) {
            break;
          } else {
            if (flag[j] != 0) {
              double dis = 1 - Similarity.getCosSim(dataPoints.get(j), dataPoints.get(i));
              if (dis < Min_Value) {
                Min_Value = dis;
              }
            }
          }
        }
        if (j < flag.length) continue;
        if (Min_Value > Max_Value) {
          startIndex = i;
          Max_Value = Min_Value;
        }
      }
      flag[l++] = startIndex;
      set.add(startIndex);

    }

    for (i = 0; i < K; i++) {
      DataPoint tempDataPoint = dataPoints.get(i);
      List<DataPoint> tempDataPoints = new ArrayList<DataPoint>();
      tempDataPoints.add(tempDataPoint);
      Cluster tempCluster = new Cluster();
      tempCluster.setClusterName(String.valueOf(i));
      tempCluster.setDataPoints(tempDataPoints);
      tempDataPoint.setCluster(tempCluster);
      result.add(i, tempCluster);
    }
    return result;
  }

  /***
   * distortion function J(c,mu)=sum(dis(dps,cdp)^2)/K
   * 
   * @param cluster
   * @return
   */
  private double distortionFunction(Cluster cluster) {
    double sum = 0;
    DataPoint cdp = cluster.getCenterPoint();
    List<DataPoint> dps = cluster.getDataPoints();
    for (int i = 0; i < dps.size(); i++) {
      sum += Math.pow(1 - Similarity.getCosSim(cdp, dps.get(i)), 2);
    }
    return sum;
  }


}
