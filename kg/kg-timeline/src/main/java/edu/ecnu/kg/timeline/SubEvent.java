package edu.ecnu.kg.timeline;

public class SubEvent {
  String summary="";
  String newsId="";
  int newsCount=0;
  public int getNewsCount() {
    return newsCount;
  }
  public void setNewsCount(int newsCount) {
    this.newsCount = newsCount;
  }
  public String getNewsId() {
    return newsId;
  }
  public void setNewsId(String newsId) {
    this.newsId = newsId;
  }
  String newsTime="";
  String newsTitle="";
  String newsUrl="";
  String newsMedia="";
  public String getSummary() {
    return summary;
  }
  public void setSummary(String summary) {
    this.summary = summary;
  }

  @Override
  public String toString() {
    return "SubEvent [summary=" + summary + ", newsTime=" + newsTime + ", newsTitle=" + newsTitle
        + ", newsUrl=" + newsUrl + ", newsMedia=" + newsMedia + "]";
  }
  public String getNewsTitle() {
    return newsTitle;
  }
  public void setNewsTitle(String newsTitle) {
    this.newsTitle = newsTitle;
  }
  public String getNewsUrl() {
    return newsUrl;
  }
  public void setNewsUrl(String newsUrl) {
    this.newsUrl = newsUrl;
  }
  public String getNewsTime() {
    return newsTime;
  }
  public void setNewsTime(String newsTime) {
    this.newsTime = newsTime;
  }
  public String getNewsMedia() {
    return newsMedia;
  }
  public void setNewsMedia(String newsMedia) {
    this.newsMedia = newsMedia;
  }
 
  
}
