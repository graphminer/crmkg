package edu.ecnu.kg.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.lucene.document.Document;

import edu.ecnu.kg.common.SenSegmentation;
import edu.ecnu.kg.data.process.TFIDFAgorithm;

public class SummariserAlgorithm {
  /**
   * 
  * @Title: summarise
  * @Description: 文章摘要实现
  * @param @param input
  * @param @param numSentences
  * @param @return    
  * @return String   
  * @throws
   */
  public static String summarise(String docName,Map<String, Document> searchResult,String field,int numSentences) {
      // get the frequency of each word in the input
      
      Map<Integer, Double> wordFrequencies = TFIDFAgorithm.wordFrequencyMap.get(docName);
      
      // now create a set of the X most frequent words
      Set<String> mostFrequentWords = getMostFrequentWords(100, wordFrequencies).keySet();
      
      // break the input up into sentences
      // workingSentences is used for the analysis, but
      // actualSentences is used in the results so that the 
      // capitalisation will be correct.
      String content=searchResult.get(docName).get(field);
      List<String> sentences=SenSegmentation.segSentence(content);
      
      // iterate over the most frequent words, and add the first sentence 
      // that includes each word to the result
      Set<String> outputSentences = new LinkedHashSet<String>();
      Iterator<String> it = mostFrequentWords.iterator();
      while (it.hasNext()) {
          String word = (String) it.next();
          for (int i = 0; i < sentences.size(); i++) {
              if (sentences.get(i).indexOf(word) >= 0) {
                  outputSentences.add(sentences.get(i));
                  break;
              }
              if (outputSentences.size() >= numSentences) {
                  break;
              }
          }
          if (outputSentences.size() >= numSentences) {
              break;
          }
      }

      List<String> reorderedOutputSentences = reorderSentences(outputSentences, content);

      StringBuffer result = new StringBuffer("");
      it = reorderedOutputSentences.iterator();
      while (it.hasNext()) {
          String sentence = (String) it.next();
          result.append(sentence);
          if (it.hasNext()) {
              result.append(" ");
          }
      }

      return result.toString();
  }
  
  
  /**
   * 
  * @Title: reorderSentences
  * @Description: 将句子按顺序输出
  * @param @param outputSentences
  * @param @param input
  * @param @return    
  * @return List<String>   
  * @throws
   */
  private static List<String> reorderSentences(Set<String> outputSentences, final String input) {
      // reorder the sentences to the order they were in the 
      // original text
      ArrayList<String> result = new ArrayList<String>(outputSentences);

      Collections.sort(result, new Comparator<String>() {
          public int compare(String arg0, String arg1) {
              String sentence1 = (String) arg0;
              String sentence2 = (String) arg1;

              int indexOfSentence1 = input.indexOf(sentence1.trim());
              int indexOfSentence2 = input.indexOf(sentence2.trim());
              int result = indexOfSentence1 - indexOfSentence2;

              return result;
          }

      });
      return result;
      
      
      
  }
  
  
  /**
   * 
  * @Title: getMostFrequentWords
  * @Description: 对分词进行按数量排序,取出前num个
  * @param @param num
  * @param @param words
  * @param @return    
  * @return Map<String,Integer>   
  * @throws
   */
  public static Map<String, Double> getMostFrequentWords(int num,Map<Integer, Double> words){
      
      Map<String,Double> workWords=new LinkedHashMap<String,Double>();
      
      Set<Entry<Integer, Double>> entrySet=words.entrySet();
      Set<Entry<String, Integer>> featureSet=TFIDFAgorithm.features.entrySet();
      for(Entry<String, Integer> feature:featureSet){
        String word=feature.getKey();
        int wid=feature.getValue();
        for(Entry<Integer, Double> entry:entrySet){
            if(entry.getKey()==wid){
              workWords.put(word, entry.getValue());
            }
        }
      }
      int count=0;
      
      Map<String, Double> keywords = new LinkedHashMap<String, Double>();
      
      // 词频统计
      List<Map.Entry<String, Double>> info = new ArrayList<Map.Entry<String, Double>>(workWords.entrySet());
      Collections.sort(info, new Comparator<Map.Entry<String, Double>>() {
          public int compare(Map.Entry<String, Double> obj1, Map.Entry<String, Double> obj2) {
              return (int) (obj2.getValue() - obj1.getValue());
          }
      });
      
      // 高频词输出
      for (int j = 0; j < info.size(); j++) {
          // 词-->频
          if(info.get(j).getKey().length()>1){
              if(num >count){
                keywords.put(info.get(j).getKey(), info.get(j).getValue());
                  count++;
              }else{
                  break;
              }
          }
      }
      return keywords;
  }
  
}
