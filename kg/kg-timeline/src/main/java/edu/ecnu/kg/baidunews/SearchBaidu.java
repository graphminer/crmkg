package edu.ecnu.kg.baidunews;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SearchBaidu {
	public static void main(String[] args) {
		getNewsFromBaidu("����");
	}
	
	public static String getNewsFromBaidu(String Keywords){
		try {
			String url="http://news.baidu.com/ns?word="+Keywords+"&tn=newstitle&from=news&cl=1&rn=100&ct=0&rsv_page=1&pn=";
			Document doc = Jsoup.connect(url).get();
			System.out.println(doc.title());
			int sum = Integer.valueOf(doc
					.getElementsByAttributeValue("class", "nums").get(0).text()
					.replace("�ҵ��������Լ", "").replace(",", "").replace("ƪ", ""));
			System.out.println(sum);
			for (int k = 0; k < 9; k++) {
			 doc = Jsoup
						.connect(
								url+k*100).get();
				Elements elements = doc.getElementsByTag("li");
				for (int i = 0; i < elements.size(); i++) {
					Element element = elements.get(i);
					Elements links = element.getElementsByTag("a");
					for (int j = 0; j < links.size(); j++) {
						Element link = links.get(j);
						if (links.get(j).toString().contains("<em>")) {
							System.out.print("news url :" + link.attr("href")
									+ "\t");
							System.out.print("news title :" + link.text()
									+ "\t");
						} else {
							System.out.print("same news count :"
									+ links.get(j).text() + "\t");
						}
					}
					Elements spans = element.getElementsByTag("span");
					for (int j = 0; j < spans.size(); j++) {
						String spanContent = spans.get(j).html()
								.replace("&nbsp;", " ").trim();
						String[] sttrs = new String[3];
						sttrs = spanContent.split(" ");
						System.out.print("news source:" + null2String(sttrs[0])
								+ "\t");
						if (sttrs.length == 3) {
							System.out.print("news time:"
									+ null2String(sttrs[1]) + "\t"
									+ null2String(sttrs[2]) + "\t");
						} else {
							System.out.print("news time:"
									+ null2String(sttrs[1]) + "\t");
						}
					}
					System.out.println();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
	public static String getStringbyRegex(String string, String regexp) {
		Pattern pattern = Pattern.compile(regexp);
		Matcher matcher = pattern.matcher(string);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			sb.append(matcher.group(1)).append("\t");
		}
		return sb.toString();
	}

	public static ArrayList<String> getStringsbyRegex(String string,
			String regexp) {
		Pattern pattern = Pattern.compile(regexp);
		Matcher matcher = pattern.matcher(string);
		ArrayList<String> arraylist = new ArrayList<String>();
		while (matcher.find()) {
			if (matcher.group(1) != null && !matcher.group(1).equals("")) {
				arraylist.add(matcher.group(1));
			}
		}
		return arraylist;
	}

	public static String null2String(String str) {
		if (str == null) {
			return "";
		} else
			return str;
	}
}
