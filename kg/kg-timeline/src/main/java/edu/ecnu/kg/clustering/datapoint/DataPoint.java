package edu.ecnu.kg.clustering.datapoint;

import java.util.HashMap;
import java.util.Map;

import org.apache.lucene.document.Document;

public class DataPoint {
	String dataPointName; // 样本点名
	String time;
	Document orignalDoc;
    Cluster cluster; // 样本点所属类簇
    int newsCount=0;
    public int getNewsCount() {
      return newsCount;
    }

    public void setNewsCount(int newsCount) {
      this.newsCount = newsCount;
    }
    private Map<Integer,Double> vector=new HashMap<Integer,Double>(); // 样本点的维度
    public DataPoint(){
    	
    }
    
    public Document getOrignalDoc() {
      return orignalDoc;
    }

    public void setOrignalDoc(Document orignalDoc) {
      this.orignalDoc = orignalDoc;
    }

    public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public DataPoint(Map<Integer,Double> vector,String dataPointName){
         this.dataPointName=dataPointName;
         this.vector=vector;
    }
    
    public String getDataPointName() {
		return dataPointName;
	}
    
	public void setDataPointName(String dataPointName) {
		this.dataPointName = dataPointName;
	}
	public Cluster getCluster() {
		return cluster;
	}
	public void setCluster(Cluster cluster) {
		this.cluster = cluster;
	}
	public Map<Integer,Double> getVector() {
		return vector;
	}
	public void setVector(Map<Integer,Double> vector) {
		this.vector = vector;
	}
}
