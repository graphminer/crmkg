package edu.ecnu.kg.timeline;

import java.util.ArrayList;
import java.util.List;

public class Event {
  
 
  @Override
  public String toString() {
    StringBuffer sb=new StringBuffer();
    sb.append("Event [keywords=" + keywords + ", seqEvents=");
    for(int i=0;i<seqEvents.size();i++){
      SubEvent subEvent=seqEvents.get(i);
      sb.append(subEvent.toString());
    }
    sb.append("]");
    return sb.toString();
  }
  int newsCount=0;
  String keywords;
  public int getNewsCount() {
    return newsCount;
  }
  public void setNewsCount(int newsCount) {
    this.newsCount = newsCount;
  }
  List<SubEvent> seqEvents=new ArrayList<SubEvent>();
  String startTime;
  public String getKeywords() {
    return keywords;
  }
  public void setKeywords(String keywords) {
    this.keywords = keywords;
  }
  public String getStartTime() {
    return startTime;
  }
  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }
  public List<SubEvent> getSeqEvents() {
    return seqEvents;
  }
  public void setSeqEvents(List<SubEvent> seqEvents) {
    this.seqEvents = seqEvents;
  }
  
}
