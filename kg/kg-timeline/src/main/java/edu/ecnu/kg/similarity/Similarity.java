package edu.ecnu.kg.similarity;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import edu.ecnu.kg.clustering.datapoint.Cluster;
import edu.ecnu.kg.clustering.datapoint.DataPoint;

public class Similarity {
  

	
	
	
	// 两个DataPoint之间的cos相似度
    public static double getVectorCosSim(Map<Integer, Double> vectorA, Map<Integer, Double> vectorB) {        
        // 计算相似度
        double vectorFirstModulo = 0.00;// 向量1的模
        double vectorSecondModulo = 0.00;// 向量2的模
        double vectorProduct = 0.00; // 向量积
        
        
        Set<Entry<Integer, Double>> entryAset = vectorA.entrySet();
        if(vectorA.equals(vectorB)){
            return 1;
        }
        for (Entry<Integer, Double> entryA : entryAset) {
            vectorFirstModulo += entryA.getValue() * entryA.getValue();
            if (vectorB.containsKey(entryA.getKey())) {
                vectorProduct += entryA.getValue()
                        * vectorB.get(entryA.getKey());
            }
        }
        
        Set<Entry<Integer, Double>> entryBset = vectorB.entrySet();
        for (Entry<Integer, Double> entryB : entryBset) {
            vectorSecondModulo += entryB.getValue() * entryB.getValue();
        }
        
        return vectorProduct
                / (Math.sqrt(vectorFirstModulo) * Math.sqrt(vectorSecondModulo));
    }

    
	// 两个DataPoint之间的cos相似度
	public static double getCosSim(DataPoint dpA, DataPoint dpB) {

		Map<Integer, Double> vectorA = dpA.getVector();
		Map<Integer, Double> vectorB = dpB.getVector();
		return getVectorCosSim(vectorA,vectorB);
	}

	/***
	 * 得到两个蔟之间的文本相似度距离，使用两个蔟之间任意两点之间的平均文本相似度距离
	 * @param clusterA
	 * @param clusterB
	 * @return
	 */
	public static double getClusterDis(Cluster clusterA, Cluster clusterB) {
		List<DataPoint> dataPointsA = clusterA.getDataPoints();
		List<DataPoint> dataPointsB = clusterB.getDataPoints();
		double result = 0.0;
		for (int i = 0; i < dataPointsA.size(); i++) {
			for (int j = 0; j < dataPointsB.size(); j++) {
				result += getCosSim(dataPointsA.get(i), dataPointsB.get(j));
			}
		}
		return 1 - result / (dataPointsA.size() * dataPointsB.size());
	}

	
	/***
	 * 计算两个蔟之间的时间相似度,使用两个蔟之间任意两点之间的平均时间相似度
	 * @param clusterA
	 * @param clusterB
	 * @return
	 */
	public static double getClusterSim(Cluster clusterA, Cluster clusterB) {
		List<DataPoint> dataPointsA = clusterA.getDataPoints();
		List<DataPoint> dataPointsB = clusterB.getDataPoints();
		double result = 0.0;
		for (int i = 0; i < dataPointsA.size(); i++) {
			for (int j = 0; j < dataPointsB.size(); j++) {
				result += getTimeSim(dataPointsA.get(i), dataPointsB.get(j));
			}
		}
		return result / (dataPointsA.size() * dataPointsB.size());
	}

	
	/***
	 * 计算两个点之间的时间相似度
	 * @param dataPointA
	 * @param dataPointB
	 * @return
	 */
	private static double getTimeSim(DataPoint dataPointA, DataPoint dataPointB) {
		double timeA = Double
				.valueOf(dataPointA.getDataPointName().split("_")[1].substring(
						0, 8));
		double timeB = Double
				.valueOf(dataPointB.getDataPointName().split("_")[1].substring(
						0, 8));
		return Math.abs(timeA - timeB);
	}
	
	

}
