package edu.ecnu.kg.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DBconnector {
	private static final Connection conn = null;
//	private static final String URL = "jdbc:mysql://localhost:3306/timelinedb?useUnicode=true&characterEncoding=utf-8";
	private static final String URL="jdbc:mysql://10.11.1.36:3306/kg?characterEncoding=utf-8&&zeroDateTimeBehavior=convertToNull";
	private static final String DBUSER = "root";
	private static final String DBPWD = "";

	private DBconnector() {

	}

	public synchronized static Connection getConnection() {
		if (conn == null) {
			try {
				try {
					Class.forName("com.mysql.jdbc.Driver");
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return  DriverManager.getConnection(DBconnector.URL,
						DBconnector.DBUSER, DBconnector.DBPWD);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println(e.toString());
			}
		}
		else{
			return conn;
		}
		return null;
		
	}
}
