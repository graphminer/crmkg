package edu.ecnu.kg.db;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import edu.ecnu.kg.common.StringHelper;
import edu.ecnu.kg.data.process.XmlDomReader;


public class NewsDAO {

	public void insertNews(String news, String time) {

		Connection conn = DBconnector.getConnection();
			try {
				news =new String(news.getBytes("UTF-8"),"utf-8");
				time =new String(time.getBytes("UTF-8"),"utf-8");
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			StringBuffer sbInsert = new StringBuffer();
			sbInsert.append("INSERT INTO ");
			sbInsert.append("`sinanewsbase`.`new_table`");
			sbInsert.append(" (newsTitle, newstime) ");
			sbInsert.append(" VALUES ('"+news+"','"+time+"')");
			PreparedStatement pstmt=null;
			try {
				pstmt = conn.prepareStatement(sbInsert.toString());
				int rows = pstmt.executeUpdate();
				if (rows != 1) {
					throw new SQLException("executeUpdate return value: " + rows);
				}
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	
	public void batchInsertNews(String xmlpath,int batchcount){
		NodeList nl;
		XmlDomReader xdr;
		try {
			int startRowNum=0;
			String title="";
			String time="";
			String media="";
			String url="";
			String description="";
			xdr = new XmlDomReader(xmlpath);
			nl = xdr.getElementsByTagName("news");
			System.out.println(nl.getLength());
			Connection conn = DBconnector.getConnection();
			for (int i = 0; i < nl.getLength(); i++) {
				
				Element element = (Element) nl.item(i);
				if(element.getElementsByTagName("title")!=null && element.getElementsByTagName("title").item(0)!=null){
					title =StringHelper.null2String(element.getElementsByTagName("title").item(0)
						.getTextContent()); 
				}
				if(element.getElementsByTagName("time")!=null && element.getElementsByTagName("time").item(0)!=null){
					time =StringHelper.null2String(element.getElementsByTagName("time").item(0)
						.getTextContent()); 
				}
				if(element.getElementsByTagName("media")!=null && element.getElementsByTagName("media").item(0)!=null){
					media =StringHelper.null2String(element.getElementsByTagName("media").item(0)
						.getTextContent()); 
				}
				if(element.getElementsByTagName("url")!=null && element.getElementsByTagName("url").item(0)!=null){
					url =StringHelper.null2String(element.getElementsByTagName("url").item(0)
						.getTextContent()); 
				}
				if(element.getElementsByTagName("description")!=null && element.getElementsByTagName("description").item(0)!=null){
					description =StringHelper.null2String(element.getElementsByTagName("description").item(0)
						.getTextContent()); 
				}
				
				conn.setAutoCommit(false);  
				PreparedStatement pstmt=conn.prepareStatement("insert into new_table (newsTitle,newsTime,newsMedia,newsUrl,newsDescription) values(?,?,?,?,?)");
				pstmt.setString(1, title);
				pstmt.setString(2, time);
				pstmt.setString(3, media);
				pstmt.setString(4, url);
				pstmt.setString(5, description);
                 pstmt.executeUpdate();   
                 startRowNum ++;  
                 if(startRowNum % batchcount == 0){//如果数据条数达到10000条，则提交事务  
                	 conn.commit();  
                	 conn.close();  
                     //重开链接  
                	 conn =DBconnector.getConnection();
                	 conn.setAutoCommit(false);  
                     pstmt = conn.prepareStatement("insert into new_table (newsTitle,newsTime,newsMedia,newsUrl,newsDescription) values(?,?,?,?,?)");
                     System.out.println("数据量达到"+batchcount+"条，提交事务完成。");  
                 }  
			}
			//insert the last 10000 
			 conn.commit();  
        	 conn.close(); 
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
