package edu.ecnu.kg.tools;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.stream.XMLStreamException;

import edu.ecnu.kg.common.SimpleEdge;
import edu.ecnu.kg.common.SimpleNode;
import edu.ecnu.kg.common.graphml.GraphMLTokens;
import edu.ecnu.kg.common.graphml.GraphmlKey;
import edu.ecnu.kg.tools.writer.GraphMLWriter;

/**
 * Hello world!
 * 
 * GraphMLWriter示例代码
 * 
 */
public class App {
  public static void main(String[] args) throws XMLStreamException, IOException {
    GraphMLWriter writer = new GraphMLWriter(new FileOutputStream("output/graphtest.xml"));

    // write header
    writer.writeHeader();

    // writer start
    writer.writeGraphStart();

    // write node key set
    ArrayList<GraphmlKey> nodekeys = new ArrayList<GraphmlKey>();
    GraphmlKey key1 = new GraphmlKey();
    key1.setId("k1");
    key1.setName("attr1");
    key1.setType(GraphMLTokens.STRING);
    nodekeys.add(key1);

    GraphmlKey key2 = new GraphmlKey();
    key2.setId("k2");
    key2.setName("attr2");
    key2.setType(GraphMLTokens.STRING);
    nodekeys.add(key2);
    writer.writeVertexKeyset(nodekeys);

    // write nodes
    ArrayList<SimpleNode> nodes = new ArrayList<SimpleNode>();

    SimpleNode node1 = new SimpleNode();
    node1.setId("n1");
    node1.addLabel("yes");
    node1.addProperty("k1", "what");
    nodes.add(node1);

    SimpleNode node2 = new SimpleNode();
    node2.setId("n2");
    node2.addLabel("ans");
    node2.addProperty("k2", "is");
    nodes.add(node2);

    writer.writerNodes(nodes);

    // write edges
    ArrayList<SimpleEdge> edges = new ArrayList<SimpleEdge>();
    SimpleEdge edge1 = new SimpleEdge();
    edge1.setId("e1");
    edge1.setLabel("answer");
    edge1.setSourcenode("n1");
    edge1.setTargetnode("n2");
    edges.add(edge1);

    writer.writeEdges(edges);

    // write tail
    writer.writeTail();

    System.out.println("Hello World!");
  }
}
