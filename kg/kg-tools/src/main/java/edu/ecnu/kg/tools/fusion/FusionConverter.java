package edu.ecnu.kg.tools.fusion;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.stream.XMLStreamException;

import org.apache.log4j.Logger;
import org.mapdb.DB;
import org.mapdb.DBMaker;

import edu.ecnu.kg.common.ConvertSchema;
import edu.ecnu.kg.common.PathCfg;
import edu.ecnu.kg.common.SimpleEdge;
import edu.ecnu.kg.common.SimpleNode;
import edu.ecnu.kg.common.SplitTokens;
import edu.ecnu.kg.common.graphml.GraphMLTokens;
import edu.ecnu.kg.common.graphml.GraphmlKey;
import edu.ecnu.kg.tools.writer.GraphMLWriter;


/**
 * Convert fusion result to GraphML data using convert schema
 * 
 * @author leyi 2014年5月28日
 * @version 0.0.5
 * @modify
 * @Copyright 华东师范大学数据科学与工程研究院版权所有
 */
public class FusionConverter {

  Map<String, ConvertSchema> convertRules;
  static Logger logger = Logger.getLogger(FusionConverter.class.getName());
  GraphMLWriter writer;

  Map<String, SimpleNode> uniqueNodes;
  long nodeId = 0;
  long edgeId = 0;
  String countkeyID;

  public FusionConverter() throws IOException, XMLStreamException {
    convertRules = new LinkedHashMap<String, ConvertSchema>();
    writer = new GraphMLWriter(new FileOutputStream("output/graph.xml"));
    DB db = DBMaker.newMemoryDB().transactionDisable().make();
    uniqueNodes = db.getHashMap("uniqueNodes");
    loadRules();
  }

  public void convert(String file) throws IOException {
    writer.writeHeader();

    // generate keyset
    HashMap<String, String> kids = generatekeyset();

    writer.writeGraphStart();

    int convertedlines = 1;

    BufferedReader br = null;

    try {
      br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));

      String line;
      String[] header = null;

      line = br.readLine();
      header = line.split(SplitTokens.COLUMN_SPLIT);
      Map<Integer, ArrayList<Integer>> template = graphmlTemplate(header);

      while ((line = br.readLine()) != null) {

        convertedlines++;
        if (convertedlines % 1000 == 0) {
          logger.info("Finished convert companies: " + convertedlines);
        }

        String[] cols = line.split(SplitTokens.COLUMN_SPLIT);

        convertLine(template, header, cols, kids);

      }

      writer.writeTail();
    } catch (UnsupportedEncodingException e) {
      logger.error(e);
    } catch (FileNotFoundException e) {
      logger.error(e);
    } catch (IOException e) {
      logger.error(e);
    }
  }



  private Map<Integer, ArrayList<Integer>> graphmlTemplate(String[] header) {
    Map<Integer, ArrayList<Integer>> template = new LinkedHashMap<Integer, ArrayList<Integer>>();
    HashMap<Integer, Integer> nodekeypos = new HashMap<Integer, Integer>();

    for (int i = 0; i < header.length; i++) {
      ConvertSchema schema = convertRules.get(header[i]);
      if (schema != null) {
        if (schema.isNode() || schema.isNodes()) {
          template.put(i, new ArrayList<Integer>());
          nodekeypos.put(schema.getNodekey(), i);
        } else if (schema.isAttr()) {
          int keynodeid = nodekeypos.get(schema.getNodekey());
          ArrayList<Integer> attrs = template.get(keynodeid);
          if (attrs != null) {
            attrs.add(i);
            template.put(keynodeid, attrs);
          }
        }
      }
    }
    return template;
  }


  private void convertLine(Map<Integer, ArrayList<Integer>> template, String[] header,
      String[] cols, HashMap<String, String> kids) throws IOException {
    List<SimpleNode> nodelist = new ArrayList<SimpleNode>();
    List<SimpleEdge> edgelist = new ArrayList<SimpleEdge>();
    String datasources;
    SimpleNode keynode = null;

    datasources = convertSources(cols);
    if ("".equals(datasources)) {
      return;
    }

    Iterator<Entry<Integer, ArrayList<Integer>>> it = template.entrySet().iterator();
    while (it.hasNext()) {
      Entry<Integer, ArrayList<Integer>> entry = it.next();
      if (entry.getKey() >= cols.length) break;

      String key = header[entry.getKey()];
      String value = cols[entry.getKey()];
      String nodekey = key;
      if (value == null || "".equals(value.trim())) {
        continue;
      }

      ArrayList<Integer> attrs = entry.getValue();
      ConvertSchema schema = convertRules.get(key);

      if ("PERSON".equals(schema.getLabel())) {
        nodekey = "person";
      }

      if (schema.isNode()) {
        SimpleNode newnode = null;
        if (schema.isUnique()) {
          newnode = uniqueNodes.get(key + value);
        }
        if (newnode == null) {
          newnode = new SimpleNode();
          nodeId++;
          newnode.setId("n" + String.valueOf(nodeId));
          newnode.addLabel(schema.getLabel());

          newnode.addProperty(kids.get(nodekey), value);

          for (Integer attrpos : attrs) {
            if (attrpos >= cols.length) break;
            String attrvalue = cols[attrpos];
            String attrname = header[attrpos];

            if (attrvalue != null && !"".equals(attrvalue.trim()))
              newnode.addProperty(kids.get(attrname), attrvalue);
          }
          newnode.addProperty(kids.get(GraphMLTokens.DATASOURCE), datasources);
          nodelist.add(newnode);

          if (schema.isUnique()) {
            uniqueNodes.put(key + value, newnode);
          }
        }

        if ("companyname".equals(key)) {
          keynode = newnode;
        } else {
          // add edge
          SimpleEdge edge = new SimpleEdge();
          edgeId++;
          String eid = "e" + String.valueOf(edgeId);
          edge.setId(eid);
          edge.setLabel(key);
          edge.setSourcenode(keynode.getId());
          edge.setTargetnode(newnode.getId());
          edgelist.add(edge);
        }

      } else if (schema.isNodes()) {
        // new nodes and add edges
        String[] values = value.split(SplitTokens.VALUE_SPLIT);
        SimpleNode newnode = null;
        if (schema.isUnique()) {
          for (String onevalue : values) {
            SimpleEdge newedge = new SimpleEdge();
            edgeId++;
            String eid = "e" + String.valueOf(edgeId);
            newedge.setId(eid);
            newedge.setLabel(key);
            newedge.setSourcenode(keynode.getId());

            newnode = uniqueNodes.get(key + onevalue);
            if (newnode != null) {
              newedge.setTargetnode(newnode.getId());
            } else {
              newnode = new SimpleNode();
              nodeId++;
              newnode.setId("n" + String.valueOf(nodeId));
              newnode.addLabel(schema.getLabel());
              newnode.addProperty(kids.get(key), onevalue);
              uniqueNodes.put(key + onevalue, newnode);
              newedge.setTargetnode(newnode.getId());
              nodelist.add(newnode);
            }
            edgelist.add(newedge);
          }
        } else {

          for (String onevalue : values) {
            SimpleEdge newedge = new SimpleEdge();
            edgeId++;
            String eid = "e" + String.valueOf(edgeId);
            newedge.setId(eid);
            newedge.setLabel(key);
            newedge.setSourcenode(keynode.getId());

            newnode = new SimpleNode();
            nodeId++;
            newnode.setId("n" + String.valueOf(nodeId));
            newnode.addLabel(schema.getLabel());
            newnode.addProperty(kids.get(key), onevalue);
            newedge.setTargetnode(newnode.getId());
            nodelist.add(newnode);
            edgelist.add(newedge);
          }
        }

      }
    }

    if (nodelist.size() > 0) {
      writer.writerNodes(nodelist);
    }
    if (edgelist.size() > 0) {
      writer.writeEdges(edgelist);
    }

  }

  private String convertSources(String[] cols) {
    //
    Set<String> sourceset = new HashSet<String>();
    String datasources = "";
    if (cols == null || cols.length <= 0) {
      return datasources;
    }
    for (int i = 0; i < cols.length; i++) {
      String[] values = cols[i].split(SplitTokens.LABEL_SPLIT);
      if (values.length > 0) {
        cols[i] = values[0];
      }
      if (values.length > 1) {
        String[] sources = values[1].split(SplitTokens.SOURCE_SPLIT);
        if (sources.length > 0) {
          for (String source : sources) {
            sourceset.add(source);
          }
        }
      }

    }

    Iterator<String> it = sourceset.iterator();
    int count = 0;
    while (it.hasNext()) {
      if (count != 0) {
        datasources = datasources + SplitTokens.COMMA_SPLIT;
      }
      datasources = datasources + it.next();
      count++;
    }
    return datasources;
  }

  private HashMap<String, String> generatekeyset() throws IOException {
    HashMap<String, String> keyids = new HashMap<String, String>();
    List<GraphmlKey> keyset = new ArrayList<GraphmlKey>();
    int keyId = 0;

    Iterator<Entry<String, ConvertSchema>> it = convertRules.entrySet().iterator();
    while (it.hasNext()) {
      Entry<String, ConvertSchema> entry = it.next();
      keyId++;
      GraphmlKey key = new GraphmlKey();
      String kid = "k" + keyId;
      key.setId(kid);
      key.setName(entry.getKey());
      key.setType(entry.getValue().getType());

      keyids.put(entry.getKey(), kid);
      keyset.add(key);
    }
    // add person key
    keyId++;
    GraphmlKey key = new GraphmlKey();
    String kid = "k" + keyId;
    key.setId(kid);
    key.setName("person");
    key.setType("STRING");
    keyids.put("person", kid);
    keyset.add(key);

    // add datasource key
    keyId++;
    key = new GraphmlKey();
    kid = "k" + keyId;
    key.setId(kid);
    key.setName(GraphMLTokens.DATASOURCE);
    key.setType("STRING");
    keyids.put(GraphMLTokens.DATASOURCE, kid);
    keyset.add(key);

    writer.writeVertexKeyset(keyset);

    return keyids;
  }

  private void loadRules() throws IOException {
    BufferedReader br = null;
    try {
      br =
          new BufferedReader(new InputStreamReader(new FileInputStream(PathCfg.getConvertSchema()),
              "UTF-8"));
    } catch (FileNotFoundException e) {
      System.out.println("Cannot found the convertutf8.csv file!");
    } catch (UnsupportedEncodingException e) {
      System.out.println("Encode Error!");
    }
    try {
      String line;
      br.readLine();
      while ((line = br.readLine()) != null) {
        String[] temp = line.split(SplitTokens.COMMA_SPLIT);
        if (temp.length == 5) {
          ConvertSchema schema = new ConvertSchema();

          String rule = temp[1];
          if (rule.contains("ATTR_NODE")) {
            schema.setAttr(true);
            schema.setNodekey(Integer.valueOf(rule.substring(rule.indexOf("E") + 1)));
          } else if (rule.contains("NODES"))
            schema.setNodes(true);
          else if (rule.contains("NODE")) {
            schema.setNode(true);
            schema.setNodekey(Integer.valueOf(rule.substring(rule.indexOf("E") + 1)));
          }

          String label = temp[2];
          if (label != null && !"".equals(label)) {
            schema.setLabel(label);
          }

          String type = temp[3];

          if (type != null && !"".equals(type)) {
            schema.setType(type);;
          }

          String uniqu = temp[4];
          if (uniqu != null && "1".equals(uniqu.trim())) {
            schema.setUnique(true);
          }

          convertRules.put(temp[0], schema);
        }

      }
    } catch (IOException e) {
      logger.error("Parse schema.csv Error!");
    }

    br.close();
  }



  public static void main(String[] args) throws IOException, XMLStreamException {
    FusionConverter convert = new FusionConverter();
    convert.convert("output/fusionresult.txt");

  }

}
