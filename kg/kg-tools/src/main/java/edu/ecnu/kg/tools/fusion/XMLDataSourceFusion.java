package edu.ecnu.kg.tools.fusion;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.apache.log4j.Logger;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;

import edu.ecnu.kg.common.CompanyInfo;
import edu.ecnu.kg.common.DataSource;
import edu.ecnu.kg.common.FusionRules;
import edu.ecnu.kg.common.SplitTokens;
import edu.ecnu.kg.tools.reader.SchemaMapper;

public class XMLDataSourceFusion {
  static Logger logger = Logger.getLogger(XMLDataSourceFusion.class.getName());

  // all record in memory key:company id
  DB db = DBMaker.newMemoryDB().transactionDisable().make();
  HTreeMap<String, CompanyInfo> allrecord = db.getHashMap("allrecord");
  // Map<String, CompanyInfo> allrecord = new LongHashMap<String, CompanyInfo>();

  SchemaMapper schemaMapper = SchemaMapper.getInstance();

  static int count = 0; // data source计数，避免每个company的重复

  public void addXMLSource(String sourcepath, DataSource ds, String encoding)
      throws XMLStreamException, FileNotFoundException {

    count++;

    File inputfile = new File(sourcepath);
    if (inputfile.isDirectory()) {
      for (File fileEntry : inputfile.listFiles()) {
        if (fileEntry.isFile()) {
          addSourceFile(inputfile + "/" + fileEntry.getName(), ds, encoding);
        }
      }
    } else {
      addSourceFile(sourcepath, ds, encoding);
    }
  } // end of add XML Datasource

  public void addTXTSource(String sourcepath, DataSource ds, String encoding) throws IOException {
    count++;
    HashMap<String, String> mapper = schemaMapper.getSchema().get(ds);
    BufferedReader br = null;
    br = new BufferedReader(new InputStreamReader(new FileInputStream(sourcepath), encoding));
    String line;
    String[] header = null;
    // load common schema keys
    line = br.readLine();
    if (line != null) {
      header = line.split(SplitTokens.COLUMN_SPLIT);
      for (int i = 0; i < header.length; i++) {
        String common;
        if ((common = mapper.get(header[i])) != null) {
          header[i] = common;
        }
      }
    }

    while ((line = br.readLine()) != null) {
      CompanyInfo info = new CompanyInfo();
      String[] cominfo = line.split(SplitTokens.COLUMN_SPLIT);
      String comID = null;
      for (int j = 0; j < cominfo.length; j++) {
        if (CompanyInfo.COMPANYKEY.equals(header[j])) {
          comID = cominfo[j];
          comID = comID.replaceAll("\\p{P}", "");
          info.setID(comID);
        } else {
          info.put(header[j], cominfo[j] + SplitTokens.LABEL_SPLIT + ds, count);
        }
      }

      if (comID != null && !"".equals(comID.trim())) {
        allrecord.put(comID, info);
      }
    }

    br.close();
  }


  public void addSourceFile(String sourcepath, DataSource ds, String encoding)
      throws FileNotFoundException, XMLStreamException {

    // check xml suffix
    if (!sourcepath.endsWith("xml")) {
      logger.info("Invalid file: " + sourcepath);
      return;
    }
    if (sourcepath.endsWith("41.xml") || sourcepath.endsWith("42.xml")
        || sourcepath.endsWith("43.xml") || sourcepath.endsWith("44.xml")
        || sourcepath.endsWith("45.xml") || sourcepath.endsWith("46.xml")
        || sourcepath.endsWith("47.xml") || sourcepath.endsWith("48.xml")
        || sourcepath.endsWith("49.xml")) {
      return;
    }
    logger.info("Read file:" + sourcepath);

    HashMap<String, String> mapper = schemaMapper.getSchema().get(ds);
    XMLInputFactory factory = XMLInputFactory.newInstance();
    factory.setProperty("javax.xml.stream.isCoalescing", true);
    InputStream in = new FileInputStream(sourcepath);
    XMLEventReader reader = factory.createXMLEventReader(in, encoding);

    HashMap<String, String> tempcom = null;
    String comID = null;

    while (reader.hasNext()) {
      XMLEvent event = reader.nextEvent();

      if (event.isStartElement()) {
        StartElement startElement = event.asStartElement();
        String tag = startElement.getName().getLocalPart();

        if (tag.equals("company") || tag.equals("Company") || tag.equals("公司")) {
          tempcom = new HashMap<String, String>();
          comID = null;
          continue;
        }

        if (mapper.containsKey(tag) && tempcom != null) {
          event = reader.nextEvent();
          String commonschema = mapper.get(tag);
          if (event.isCharacters()) {
            String newvalue = event.asCharacters().getData();
            if (newvalue == null || "".equals(newvalue)) {
              continue;
            }
            if (CompanyInfo.COMPANYKEY.equals(commonschema)) {
              comID = newvalue;
              comID = comID.replaceAll("\\p{P}", ""); // 去掉公司名中的标点符号
            } else {
              if (!"--".equals(newvalue) && !newvalue.contains("无法显示")) {
                tempcom.put(commonschema, newvalue + SplitTokens.LABEL_SPLIT + ds);
              }

            }
          }
          continue;
        }
      }

      if (event.isEndElement()) {
        EndElement endElement = event.asEndElement();
        String tag = endElement.getName().getLocalPart();
        if (tag.equals("company") || tag.equals("Company") || tag.equals("公司")) {
          if (tempcom != null && comID != null && !"".equals(comID) && tempcom.size() > 1) {
            CompanyInfo info = allrecord.get(comID);
            if (info == null) {
              info = new CompanyInfo(comID);
            }
            info.putMap(tempcom, count);

            allrecord.put(comID, info);

            // TODO 写入其他数据到备份

          }
          tempcom = null;
          continue;
        }
      }
    }
  }


  // 不同字段的fusion
  public void fusion() {
    logger.info("Start fusion of all company data...");

    int linecount = 0;

    String[] schemas = schemaMapper.getCommonSchema();
    ArrayList<String> fusionresult = new ArrayList<String>();

    logger.info("Total companies:" + allrecord.size());

    Iterator<Entry<String, CompanyInfo>> it = allrecord.entrySet().iterator();
    while (it.hasNext()) {
      linecount++;
      if (linecount % 100000 == 0) {
        logger.info("Finished lines of company:" + linecount);
      }
      Entry<String, CompanyInfo> entry = it.next();
      String comID = entry.getKey();
      CompanyInfo info = entry.getValue();
      HashMap<String, ArrayList<String>> attrs = info.getAttributes();

      String line = comID;
      if (line != null && "".equals(line.trim())) {
        continue;
      }

      for (String head : schemas) {
        if (CompanyInfo.COMPANYKEY.equals(head)) {
          continue;
        }
        ArrayList<String> values = attrs.get(head);
        FusionRules rule = schemaMapper.getRulesbySchema(head);

        String merged = fusionByRule(values, rule, head);
        // merged=merged.replace("\t", " ").replace("\n", "  ").replace("\r", " ");
        merged = merged.replaceAll("[\\t\\n\\r]", "  ");

        line = line + SplitTokens.COLUMN_SPLIT + merged;
      }

      fusionresult.add(line);
      it.remove();
    }
    allrecord.clear();

    // 将fusion结果写入文件

    try {
      writeFusionResult(fusionresult, schemas);
    } catch (IOException e) {
      logger.error("Write fusion result failed!", e);
    }

  }


  private String fusionByRule(ArrayList<String> values, FusionRules rule, String title) {
    //
    String result = "";
    if (values == null || values.size() == 0) {
      return "";
    } else {
      HashMap<String, Double> valueweight = new HashMap<String, Double>();
      HashMap<String, String> valuesource = new HashMap<String, String>();
      HashSet<String> sources = new HashSet<String>();

      for (String value : values) {
        String[] onesource = value.split(SplitTokens.LABEL_SPLIT);
        if (onesource.length == 2) {
          String newsource = onesource[1];
          // To check duplicate
          if (sources.contains(newsource)) {
            continue;
          }
          if (newsource == null) {
            newsource = "";
          }
          sources.add(newsource);
          String source = valuesource.get(onesource[0]);
          if (source == null) {
            source = newsource;
          } else {
            source = source + SplitTokens.SOURCE_SPLIT + newsource;
          }
          valuesource.put(onesource[0], source);

          Double weight = valueweight.get(onesource[0]);
          if (weight == null || weight <= 0.0) {
            weight = new Double(0.0);
          }
          weight = weight + DataSource.parse(onesource[1]).getWeight();
          valueweight.put(onesource[0], weight);
        }
      }

      ArrayList<Entry<String, Double>> ordered = sortMapByValue(valueweight);

      // rules applied
      result = "";
      if (ordered == null || ordered.size() <= 0) {
        return result;
      }
      switch (rule) {

        case VOTE:
          String key1 = ordered.get(0).getKey();
          result = key1 + SplitTokens.LABEL_SPLIT + valuesource.get(key1);
          break;

        case MERGE:
          int count1 = 0;
          for (Entry<String, Double> entry : ordered) {
            if (count1 > 0) {
              result = result + SplitTokens.VALUE_SPLIT;
            }
            String key2 = entry.getKey();
            result = key2 + SplitTokens.LABEL_SPLIT + valuesource.get(key2);
          }
          break;

        case SPLIT_MERGE:
          HashSet<String> segments = new HashSet<String>();
          for (Entry<String, Double> entry : ordered) {
            String key4 = entry.getKey();
            String[] splited;
            // if ("business".equals(title) || "range".equals(title)) {
            // splited = key4.split(";");
            // } else {
            splited = key4.split("[\\t\\n\\r\\s+.,\"\\?!:'：。！；，、;]");
            // }
            for (String split : splited) {
              if (split != null && !"".equals(split.trim())) {
                segments.add(split);
              }
            }
          }

          Iterator<String> it4 = segments.iterator();
          int count4 = 0;
          while (it4.hasNext()) {
            if (count4 > 0) {
              result = result + SplitTokens.VALUE_SPLIT;
            }
            result = result + it4.next();
            count4++;
          }

          break;

        default:
          break;
      }

    }
    return result;
  }


  private ArrayList<Entry<String, Double>> sortMapByValue(HashMap<String, Double> map) {
    int size = map.size();
    ArrayList<Entry<String, Double>> list = new ArrayList<Entry<String, Double>>(size);
    list.addAll(map.entrySet());

    Collections.sort(list, new Comparator<Entry<String, Double>>() {

      public int compare(Entry<String, Double> o1, Entry<String, Double> o2) {
        if (o1.getValue() >= o2.getValue()) {
          return 1;
        }
        return -1;
      }

    });

    map.clear();
    return list;
  }


  private void writeFusionResult(ArrayList<String> fusionresult, String[] schemas)
      throws IOException {

    File file = new File("output/fusionresult.txt");
    if (file.exists()) {
      file.renameTo(new File("output/fusionresultback.tmp"));
    }

    BufferedWriter bw =
        new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));

    int column = 0;
    for (String schema : schemas) {
      if (column > 0) {
        bw.write(SplitTokens.COLUMN_SPLIT);
      }
      bw.write(schema);
      column++;
    }

    int rows = 0;
    for (String s : fusionresult) {
      bw.newLine();
      bw.write(s);
      rows++;
    }

    bw.flush();
    bw.close();

    logger.info("Write fusion result success! Total rows:" + rows);

  }


  public static void main(String[] args) throws XMLStreamException, IOException {
    XMLDataSourceFusion fusion = new XMLDataSourceFusion();
    // fusion.addXMLSource("/home/graph/mnt/平安项目数据/上市公司/3073家上市公司信息.xml", DataSource.PUBLIC, "GBK");
    // fusion.addXMLSource("/Volumes/zhangqy/平安项目数据/上市公司/3073家上市公司信息.xml", DataSource.PUBLIC,"GBK");
    String szsefile = "/home/graph/mnt/平安项目数据/上市公司/SZSE.txt";
    fusion.addTXTSource(szsefile, DataSource.SZSE, "GBK");
    fusion.addXMLSource("/home/graph/mnt/平安项目数据/门户网站/网易公司信息/data.xml", DataSource.NTES, "GBK");
    fusion.addXMLSource("/home/graph/mnt/平安项目数据/门户网站/凤凰财经/公司信息/Company.xml", DataSource.IFENG,
        "GBK");
     fusion.addXMLSource("/home/graph/mnt/平安项目数据/worldcompany", DataSource.WORLDCOMPANY, "UTF-8");
     fusion.addXMLSource("/home/graph/mnt/平安项目数据/阿里巴巴", DataSource.ALIBABA, "UTF-8");
    // fusion.addXMLSource("/Volumes/zhangqy/平安项目数据/worldcompany/worldcompany_4.xml",
    // DataSource.WORLDCOMPANY,"UTF-8");

    fusion.fusion();

  }

}
