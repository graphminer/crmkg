package edu.ecnu.kg.tools.db;

import java.util.List;
import java.util.Map;

import edu.ecnu.kg.common.Direction;
import edu.ecnu.kg.common.SimpleNode;

/**
 * Neo4j Query Engine
 * 
 * @author leyi 2014年6月12日
 * @version 1.0.0
 * @modify
 * @Copyright 华东师范大学数据科学与工程研究院版权所有
 */
public interface Neo4jQueryEngine {

  /**
   * Add Properties to Edge by edge ID
   * 
   * @param edgeID edge ID
   * @param properties property map
   * @return
   */
  public void addEdgeProperties(String edgeID, Map<String, String> properties);

  /**
   * Add edge between two nodes
   * 
   * @param originID origin node ID
   * @param targetID target node ID
   * @param label relation label
   * @return edgeID
   */
  public String addEdge(String originID, String targetID, String label);

  public void addNodes(Iterable<SimpleNode> nodelist);

  public String addNode(SimpleNode node);

  /**
   * Add Property to Edge by edge ID
   * 
   * @param edgeID edge ID
   * @param propertylabel property label
   * @param propertyvalue property value
   * @return
   */
  public void addEdgeProperty(String edgeID, String propertylabel, String propertyvalue);

  /**
   * Add Properties to Edge by origin node ID & target node ID Better to use addEdgeProperty(String
   * edgeID, String propertylabel, String propertyvalue)
   * 
   * @param originNodeID origin node ID
   * @param targetNodeID target node ID
   * @param properties property map
   * @return
   */
  public void addEdgeProperties(String originNodeID, String targetNodeID,
      Map<String, String> properties);

  /**
   * Add Property to Edge by origin node ID & target node ID
   * 
   * @param originNodeID origin node ID
   * @param targetNodeID target node ID
   * @param propertylabel property label
   * @param propertyvalue property value
   * @return
   */
  public void addEdgeProperty(String originNodeID, String targetNodeID, String propertylabel,
      String propertyvalue);

  /**
   * Find DB Raw Nodes by node label and property
   * 
   * @param nodelabel
   * @param propertylabel
   * @param propertyvalue
   * @param maxresult max number of returned node
   * @return
   */
  public Iterable<?> findRawNodesbyLabelandProperty(String nodelabel, String propertylabel,
      String propertyvalue, int maxresult);

  /**
   * Find SimpleNodes by node label and property
   * 
   * @param nodelabel
   * @param propertylabel
   * @param propertyvalue
   * @param maxresult
   * @return list of nodes
   */
  public Iterable<SimpleNode> findSimpleNodesbyLabelandProperty(String nodelabel,
      String propertylabel, String propertyvalue, int maxresult);

  /**
   * Fuzzy match for node query
   * 
   * @param nodelabel
   * @param propertylabel
   * @param propertyvalue
   * @param maxresult
   * @return list of nodes
   */
  public Iterable<?> similarRawNodesbyLabelandProperty(String nodelabel, String propertylabel,
      String propertyvalue, int maxresult);

  public Iterable<SimpleNode> similarSimpleNodesbyLabelandProperty(String nodelabel,
      String propertylabel, String propertyvalue, int maxresult);

  public Object getRawNodeByID(String nodeID);

  public SimpleNode getSimpleNodeByID(String nodeID);

  /**
   * Query neighbour nodes by node ID
   * 
   * @param nodeID query node ID
   * @param relationlabel label for relation
   * @param targetnodelabel label for target node
   * @param direction relation label
   * @return list of neighbour nodes
   */
  public Iterable<?> getRawNeighbourNodes(String nodeID, String relationlabel,
      String targetnodelabel, Direction direction);


  Iterable<SimpleNode> similarSimpleNodesbyLabelandProperties(String nodelabel, String[] key,
      String propertyvalue, int maxresult);

}
