package edu.ecnu.kg.tools.reader;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import edu.ecnu.kg.common.DataSource;
import edu.ecnu.kg.common.FusionRules;
import edu.ecnu.kg.common.PathCfg;

/**
 * SchemaMapper单例工具类，读取schema.csv文件
 * 
 * @author leyi 2014年5月20日
 * @version 0.1.0
 * @modify
 * @Copyright 华东师范大学数据科学与工程研究院版权所有
 */
public class SchemaMapper {

  private static SchemaMapper instance;
  private static final int reductionCol = 2;

  private static String[] commonSchema; // 统一schema
  private static FusionRules[] fusionRules; // 映射规则
  private static Map<String, FusionRules> schemaRules;
  private static Map<DataSource, HashMap<String, String>> schema; // 不同数据源schema映射表

  private SchemaMapper() {
    schemaRules = new HashMap<String, FusionRules>();
    schema = new EnumMap<DataSource, HashMap<String, String>>(DataSource.class);
  }

  /**
   * Singleton Class SchemaMapper
   * 
   * @return instance
   */
  public synchronized static SchemaMapper getInstance() {
    if (instance == null) {
      instance = new SchemaMapper();
      initializeSchema(PathCfg.getFusionSchema());
    }
    return instance;
  }

  private static void initializeSchema(String schemafile) {
    // Read Schema Rules from file
    BufferedReader br = null;
    try {
      br = new BufferedReader(new InputStreamReader(new FileInputStream(schemafile), "UTF-8"));
    } catch (FileNotFoundException e) {
      System.out.println("Cannot found the schema.csv file!");
    } catch (UnsupportedEncodingException e) {
      System.out.println("Encode Error!");
    }

    ArrayList<String> lines = new ArrayList<String>();
    try {
      String line;
      while ((line = br.readLine()) != null) {
        lines.add(line);
      }
    } catch (IOException e) {
      System.out.println("Parse schema.csv Error!");
    }

    // add to schema
    if (lines.size() <= 1) {
      return;
    }
    commonSchema = new String[lines.size() - 1];
    fusionRules = new FusionRules[lines.size() - 1];
    DataSource[] datasources = DataSource.parse(lines.get(0).split(","));
    for (int i = 1; i < lines.size(); i++) {
      String[] values = lines.get(i).split(",");
      commonSchema[i - 1] = values[0];
      fusionRules[i - 1] = FusionRules.parse(values[1]);
      schemaRules.put(commonSchema[i - 1], fusionRules[i - 1]);

      for (int j = reductionCol; j < values.length; j++) {

        if (values[j] == null || values[j].trim().length() <= 0) {
          continue;
        }

        HashMap<String, String> dsschema;
        dsschema = schema.get(datasources[j - reductionCol]);
        if (dsschema == null || dsschema.size() <= 0) {
          dsschema = new HashMap<String, String>(); // data source->common
        }
        dsschema.put(values[j], values[0]);
        schema.put(datasources[j - reductionCol], dsschema);
      }
    }

  }

  /**
   * 返回目标schema字段
   * 
   * @param ds
   * @param schema
   * @return
   */
  public String getTargetSchema(DataSource ds, String originschema) {
    if (schema.get(ds) != null) {
      return schema.get(ds).get(originschema);
    }
    return null;
  }

  /**
   * Schema字段个数
   * 
   * @return
   */
  public int size() {
    if (commonSchema.length != fusionRules.length) {
      return 0;
    }
    return commonSchema.length;
  }

  /**
   * 获取第i个schema字段的名称
   * 
   * @param i
   * @return
   */
  public String getSchemaName(int i) {
    if (i > commonSchema.length - 1 || i < 0) {
      return null;
    }
    return commonSchema[i];
  }

  /**
   * 获取第i个schema字段的名称
   * 
   * @param i
   * @return
   */
  public FusionRules getRules(int i) {
    if (i > fusionRules.length - 1 || i < 0) {
      return null;
    }
    return fusionRules[i];
  }

  /**
   * 获取通用的schema数组
   * 
   * @return
   */
  public String[] getCommonSchema() {
    return commonSchema;
  }

  /**
   * 获取通用的融合规则
   * 
   * @return
   */
  public FusionRules[] getFusionRules() {
    return fusionRules;
  }

  /**
   * 获取不同数据源的schema映射规则
   * 
   * @return
   */
  public Map<DataSource, HashMap<String, String>> getSchema() {
    return schema;
  }

  public FusionRules getRulesbySchema(String schemaname) {
    return schemaRules.get(schemaname);
  }


}
