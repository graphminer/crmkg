package edu.ecnu.kg.tools.db;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;

import edu.ecnu.kg.common.DBAccess;

public class Neo4jUtils {

  private static Neo4jUtils dbinstance;
  private GraphDatabaseService graphDb;

  private static String db_path;
  private static DBAccess acmethod;

  private Neo4jUtils(String dbpath, DBAccess method) {
    switch (method) {
      case EMBEDED:
        openEmbededDb();
        break;

      case EMBEDED_READONLY:
        openEmbededReadOnlyDb();
        break;

      case REST:
        break;

      default:
        openEmbededDb();
    }

  }

  void openEmbededDb() {
    graphDb =
        new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(db_path)
            .loadPropertiesFromFile("neo4j.properties").newGraphDatabase();
    registerShutdownHook(graphDb);
  }

  void openEmbededReadOnlyDb() {
    graphDb =
        new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(db_path)
            .loadPropertiesFromFile("neo4j.properties")
            .setConfig(GraphDatabaseSettings.read_only, "true").newGraphDatabase();
    registerShutdownHook(graphDb);
  }

  /**
   * @deprecated unimplemented
   */
  void openRestDb() {
    // TODO no uses
  }

  private static void registerShutdownHook(final GraphDatabaseService graphDb) {
    Runtime.getRuntime().addShutdownHook(new Thread() {
      public void run() {
        graphDb.shutdown();
      }
    });
  }

  public static Neo4jUtils getInstance(String dbpath, DBAccess method) {
    if (dbinstance == null) {
      db_path = dbpath;
      acmethod = method;
      dbinstance = new Neo4jUtils(dbpath, method);
    } else if (dbinstance != null && acmethod != method) {
      dbinstance.getGraphDb().shutdown();
      db_path = dbpath;
      acmethod = method;
      dbinstance = new Neo4jUtils(dbpath, method);
    }
    return dbinstance;
  }


  public String getDb_path() {
    return db_path;
  }


  public DBAccess getAcmethod() {
    return acmethod;
  }

  public GraphDatabaseService getGraphDb() {
    return graphDb;
  }



}
