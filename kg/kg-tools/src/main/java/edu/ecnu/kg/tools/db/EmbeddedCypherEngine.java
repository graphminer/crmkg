package edu.ecnu.kg.tools.db;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.DynamicRelationshipType;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.kernel.impl.util.StringLogger;

import edu.ecnu.kg.common.DBAccess;
import edu.ecnu.kg.common.DatabaseConfig;
import edu.ecnu.kg.common.Direction;
import edu.ecnu.kg.common.SimpleNode;

/**
 * Query Neo4j DB using Embeded method
 * 
 * @author leyi 2014年6月14日
 * @version 1.0.0
 * @modify
 * @Copyright 华东师范大学数据科学与工程研究院版权所有
 */
public class EmbeddedCypherEngine implements Neo4jQueryEngine {

  GraphDatabaseService graphdb;
  ExecutionEngine engine;
  String dbpath;

  public EmbeddedCypherEngine() {
    dbpath = DatabaseConfig.getNeo4jGraphPath();
    Neo4jUtils dbutil = Neo4jUtils.getInstance(dbpath, DBAccess.EMBEDED);
    graphdb = dbutil.getGraphDb();
    engine = new ExecutionEngine(graphdb, StringLogger.lazyLogger(new File("log/testlog")));
  }

  public EmbeddedCypherEngine(GraphDatabaseService graphdb, ExecutionEngine engine) {
    this.graphdb = graphdb;
    this.engine = engine;
  }

  /*
   * （非 Javadoc）
   * 
   * @see edu.ecnu.kg.tools.db.Neo4jQueryEngine#addEdgeProperties(java.lang.String, java.util.Map)
   */
  @Override
  public void addEdgeProperties(String edgeID, Map<String, String> properties) {
    //
    try (Transaction tx = graphdb.beginTx()) {
      Relationship relation = graphdb.getRelationshipById(Long.valueOf(edgeID));
      Iterator<Entry<String, String>> it = properties.entrySet().iterator();
      while (it.hasNext()) {
        Entry<String, String> entry = it.next();
        String key = entry.getKey();
        String value = entry.getValue();
        relation.setProperty(key, value);
      }
      tx.success();
    }

  }

  /*
   * （非 Javadoc）
   * 
   * @see edu.ecnu.kg.tools.db.Neo4jQueryEngine#addEdge(java.lang.String, java.lang.String,
   * java.lang.String)
   */
  @Override
  public String addEdge(String originID, String targetID, String label) {
    String edgeID = null;
    try (Transaction tx = graphdb.beginTx()) {
      Node origin = graphdb.getNodeById(Long.valueOf(originID));
      Node target = graphdb.getNodeById(Long.valueOf(targetID));
      Relationship relation =
          origin.createRelationshipTo(target, DynamicRelationshipType.withName(label));
      edgeID = String.valueOf(relation.getId());
      tx.success();
    }
    return edgeID;
  }

  /*
   * （非 Javadoc）
   * 
   * @see edu.ecnu.kg.tools.db.Neo4jQueryEngine#addNodes(java.lang.Iterable)
   */
  @Override
  public void addNodes(Iterable<SimpleNode> nodelist) {
    //
    for (SimpleNode node : nodelist) {
      addNode(node);
    }
  }

  /*
   * （非 Javadoc）
   * 
   * @see edu.ecnu.kg.tools.db.Neo4jQueryEngine#addEdgeProperty(java.lang.String, java.lang.String,
   * java.lang.String)
   */
  @Override
  public void addEdgeProperty(String edgeID, String propertylabel, String propertyvalue) {
    //
    try (Transaction tx = graphdb.beginTx()) {
      Relationship relation = graphdb.getRelationshipById(Long.valueOf(edgeID));
      relation.setProperty(propertylabel, propertyvalue);
      tx.success();
    }

  }

  /*
   * （非 Javadoc）
   * 
   * @see edu.ecnu.kg.tools.db.Neo4jQueryEngine#addEdgeProperties(java.lang.String,
   * java.lang.String, java.util.Map)
   */
  @Override
  public void addEdgeProperties(String originNodeID, String targetNodeID,
      Map<String, String> properties) {

    try (Transaction tx = graphdb.beginTx()) {
      Node node1 = graphdb.getNodeById(Long.valueOf(originNodeID));
      Node node2 = graphdb.getNodeById(Long.valueOf(targetNodeID));
      Relationship relation = null;
      for (Relationship r : node1.getRelationships()) {
        if (r.getOtherNode(node1).equals(node2)) relation = r;
      }
      if (relation != null) {
        Iterator<Entry<String, String>> it = properties.entrySet().iterator();
        while (it.hasNext()) {
          Entry<String, String> entry = it.next();
          String key = entry.getKey();
          String value = entry.getValue();
          relation.setProperty(key, value);
        }
      }
      tx.success();
    }
  }

  /*
   * （非 Javadoc）
   * 
   * @see edu.ecnu.kg.tools.db.Neo4jQueryEngine#addEdgeProperty(java.lang.String, java.lang.String,
   * java.lang.String, java.lang.String)
   */
  @Override
  public void addEdgeProperty(String originNodeID, String targetNodeID, String propertylabel,
      String propertyvalue) {
    try (Transaction tx = graphdb.beginTx()) {
      Node node1 = graphdb.getNodeById(Long.valueOf(originNodeID));
      Node node2 = graphdb.getNodeById(Long.valueOf(targetNodeID));

      Relationship relation = null;
      for (Relationship r : node1.getRelationships()) {
        if (r.getOtherNode(node1).equals(node2)) relation = r;
      }
      if (relation != null) {
        relation.setProperty(propertylabel, propertyvalue);
      }
      tx.success();
    }
  }

  /*
   * （非 Javadoc）
   * 
   * @see edu.ecnu.kg.tools.db.Neo4jQueryEngine#findRawNodesbyLabelandProperty(java.lang.String,
   * java.lang.String, java.lang.String, int)
   */
  @Override
  public Iterable<Node> findRawNodesbyLabelandProperty(String nodelabel, String propertylabel,
      String propertyvalue, int maxresult) {
    List<Node> result = new ArrayList<Node>();
    Label label = DynamicLabel.label(nodelabel);
    int count = 0;
    try (Transaction tx = graphdb.beginTx();
        ResourceIterator<Node> nodes =
            graphdb.findNodesByLabelAndProperty(label, propertylabel, propertyvalue).iterator()) {
      Node firstNode;
      while (nodes.hasNext()) {
        firstNode = nodes.next();
        result.add(firstNode);
        count++;
        if (count >= maxresult) {
          break;
        }
      }

      nodes.close();
      tx.success();
    }
    return result;
  }

  /*
   * （非 Javadoc）
   * 
   * @see edu.ecnu.kg.tools.db.Neo4jQueryEngine#findSimpleNodesbyLabelandProperty(java.lang.String,
   * java.lang.String, java.lang.String, int)
   */
  @Override
  public Iterable<SimpleNode> findSimpleNodesbyLabelandProperty(String nodelabel,
      String propertylabel, String propertyvalue, int maxresult) {
    //
    List<SimpleNode> result = new ArrayList<SimpleNode>();
    Label label = DynamicLabel.label(nodelabel);
    int count = 0;
    try (Transaction tx = graphdb.beginTx();
        ResourceIterator<Node> nodes =
            graphdb.findNodesByLabelAndProperty(label, propertylabel, propertyvalue).iterator()) {
      Node firstNode;
      while (nodes.hasNext()) {
        firstNode = nodes.next();
        SimpleNode simpleNode = convertToSimplenode(firstNode);
        result.add(simpleNode);
        count++;
        if (count >= maxresult) {
          break;
        }
      }

      nodes.close();
      tx.success();
    }
    return result;
  }

  /**
   * Convert a neo4j node into SimpleNode @see edu.ecnu.kg.common.SimpleNode
   * 
   * @param neoNode
   * @return
   */
  private SimpleNode convertToSimplenode(Node neoNode) {
    SimpleNode node = new SimpleNode();
    for (Label label : neoNode.getLabels()) {
      node.addLabel(label.name());
    }
    node.setId(String.valueOf(neoNode.getId()));
    for (String key : neoNode.getPropertyKeys()) {
      node.addProperty(key, String.valueOf(neoNode.getProperty(key)));
    }
    return node;
  }

  /*
   * （非 Javadoc）
   * 
   * @see edu.ecnu.kg.tools.db.Neo4jQueryEngine#similarRawNodesbyLabelandProperty(java.lang.String,
   * java.lang.String, java.lang.String, int)
   */
  @Override
  public Iterable<Node> similarRawNodesbyLabelandProperty(String nodelabel, String propertylabel,
      String propertyvalue, int maxresult) {

    List<Node> nodelist = new ArrayList<Node>();

    try (Transaction tx = graphdb.beginTx()) {
      Map<String, Object> params = new HashMap<String, Object>();
      params.put("VALUE", ".*" + propertyvalue + ".*");
      params.put("max", maxresult);

      String query =
          "MATCH (n:" + nodelabel + ") where n." + propertylabel
              + " =~ {VALUE} RETURN n limit {max}";

      ExecutionResult result = engine.execute(query, params);

      if (result == null) {
        return nodelist;
      }

      for (Map<String, Object> row : result) {
        nodelist.add((Node) row.get("n"));
      }
      tx.success();
    }

    return nodelist;
  }

  @Override
  public Iterable<SimpleNode> similarSimpleNodesbyLabelandProperty(String nodelabel,
      String propertylabel, String propertyvalue, int maxresult) {
    List<SimpleNode> nodelist = new ArrayList<SimpleNode>();

    try (Transaction tx = graphdb.beginTx()) {
      Map<String, Object> params = new HashMap<String, Object>();
      params.put("VALUE", ".*" + propertyvalue + ".*");
      params.put("max", maxresult);

      String query =
          "MATCH (n:" + nodelabel + ") where n." + propertylabel
              + " =~ {VALUE} RETURN n limit {max}";

      ExecutionResult result = engine.execute(query, params);

      if (result == null) {
        return nodelist;
      }

      for (Map<String, Object> row : result) {
        nodelist.add(convertToSimplenode((Node) row.get("n")));
      }
      tx.success();
    }

    return nodelist;
  }


  @Override
  public Iterable<SimpleNode> similarSimpleNodesbyLabelandProperties(String nodelabel,
      String[] key, String propertyvalue, int maxresult) {
    List<SimpleNode> nodelist = new ArrayList<SimpleNode>();

    try (Transaction tx = graphdb.beginTx()) {
      Map<String, Object> params = new HashMap<String, Object>();
      params.put("VALUE", ".*" + propertyvalue + ".*");
      params.put("max", maxresult);

      String query = "MATCH (n:" + nodelabel + ") where ";
      int count = 0;
      for (String propertylabel : key) {
        if (count > 0) {
          query = query + " or ";
        }
        query = query + "n." + propertylabel + "=~{VALUE} ";
        count++;
      }
      query = query + " RETURN n limit {max}";

      ExecutionResult result = engine.execute(query, params);

      if (result == null) {
        return nodelist;
      }

      for (Map<String, Object> row : result) {
        nodelist.add(convertToSimplenode((Node) row.get("n")));
      }
      tx.success();
    }

    return nodelist;
  }

  @Override
  public Node getRawNodeByID(String nodeID) {
    Node node1;
    try (Transaction tx = graphdb.beginTx()) {
      node1 = graphdb.getNodeById(Long.valueOf(nodeID));
      tx.success();
    }
    return node1;
  }

  @Override
  public SimpleNode getSimpleNodeByID(String nodeID) {
    SimpleNode node1;
    try (Transaction tx = graphdb.beginTx()) {
      node1 = convertToSimplenode(graphdb.getNodeById(Long.valueOf(nodeID)));
      tx.success();
    }
    return node1;
  }

  /*
   * （非 Javadoc）
   * 
   * @see edu.ecnu.kg.tools.db.Neo4jQueryEngine#getRawNeighbourNodes(java.lang.String,
   * java.lang.String, java.lang.String, edu.ecnu.kg.common.Direction)
   */
  @Override
  public Iterable<Node> getRawNeighbourNodes(String nodeID, String relationlabel,
      String targetnodelabel, Direction direction) {
    List<Node> nodelist = new ArrayList<Node>();

    try (Transaction tx = graphdb.beginTx()) {
      Node querynode = graphdb.getNodeById(Long.valueOf(nodeID));

      org.neo4j.graphdb.Direction direc;
      switch (direction) {
        case BOTH:
          direc = org.neo4j.graphdb.Direction.BOTH;
          break;
        case IN:
          direc = org.neo4j.graphdb.Direction.INCOMING;
          break;
        case OUT:
          direc = org.neo4j.graphdb.Direction.OUTGOING;
          break;
        default:
          direc = org.neo4j.graphdb.Direction.BOTH;
      }

      Iterable<Relationship> relations =
          querynode.getRelationships(DynamicRelationshipType.withName(relationlabel), direc);

      for (Relationship rela : relations) {
        Node othernode = rela.getEndNode();
        if (othernode.hasLabel(DynamicLabel.label(targetnodelabel))) {
          nodelist.add(othernode);
        }
      }

      tx.success();
    }

    return nodelist;
  }

  @Override
  public String addNode(SimpleNode node) {
    //
    String nodeID = null;
    try (Transaction tx = graphdb.beginTx()) {
      Node newnode = graphdb.createNode();
      for (String label : node.getLabel()) {
        newnode.addLabel(DynamicLabel.label(label.trim()));
      }
      Iterator<Entry<String, String>> it = node.getAttributes().entrySet().iterator();
      while (it.hasNext()) {
        Entry<String, String> entry = it.next();
        String key = entry.getKey();
        String value = entry.getValue();
        newnode.setProperty(key, value);
      }
      nodeID = String.valueOf(newnode.getId());
      tx.success();
    }

    return nodeID;
  }


  /**
   * ============================== 该类使用示例 =============================
   */
  public static void main(String[] agrs) {
    Neo4jQueryEngine engine = new EmbeddedCypherEngine();
    // add edge property
    String edgeID = "25307";
    String propertylabel = "test";
    String propertyvalue = "test value";
    engine.addEdgeProperty(edgeID, propertylabel, propertyvalue);

    // add edge between nodes
    engine.addEdge("14945", "14925", "test relation");

    // add new node
    SimpleNode node = new SimpleNode();
    node.addLabel("TEST");
    node.addProperty("test", "test value");
    node.addProperty("test2", "test2 value");
    engine.addNode(node);

    // add edge property by node ids
    engine.addEdgeProperty("14945", "14925", "test", "test value");

    // findRawNodesbyLabelandProperty
    List<Node> nodes =
        (List<Node>) engine.findRawNodesbyLabelandProperty("COMPANY", "chinesename", "五征集团", 10);

    // similarRawNodesbyLabelandProperty
    List<Node> nodes2 =
        (List<Node>) engine.similarRawNodesbyLabelandProperty("COMPANY", "companyname", "平安", 10);

    // getRawNeighbourNodes
    List<Node> nodes3 =
        (List<Node>) engine.getRawNeighbourNodes("14945", "test relation", "COMPANY",
            Direction.BOTH);

    System.out.println("Test");
  }



}
