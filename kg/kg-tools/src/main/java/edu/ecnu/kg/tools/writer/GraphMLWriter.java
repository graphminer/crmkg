package edu.ecnu.kg.tools.writer;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import edu.ecnu.kg.common.SimpleEdge;
import edu.ecnu.kg.common.SimpleNode;
import edu.ecnu.kg.common.graphml.GraphMLTokens;
import edu.ecnu.kg.common.graphml.GraphmlKey;

/**
 * Graphml Writer
 * 
 * @author leyi 2014年5月22日
 * @version 0.1.0
 * @modify
 * @Copyright 华东师范大学数据科学与工程研究院版权所有
 */
public class GraphMLWriter {
  final static XMLOutputFactory inputFactory = XMLOutputFactory.newInstance();
  static Logger logger = Logger.getLogger(GraphMLWriter.class.getName());
  XMLStreamWriter writer;

  public GraphMLWriter(final OutputStream graphMLOutputStream) throws XMLStreamException {
    writer = inputFactory.createXMLStreamWriter(graphMLOutputStream, "UTF-8");
  }

  public void writeHeader() throws IOException {
    try {
      writer.writeStartDocument();
      writer.writeStartElement(GraphMLTokens.GRAPHML);
      writer.writeAttribute(GraphMLTokens.XMLNS, GraphMLTokens.GRAPHML_XMLNS);

    } catch (XMLStreamException xse) {
      throw new IOException(xse);
    }
  }

  public void writeTail() throws IOException {
    try {
      writer.writeEndElement(); // graph
      writer.writeEndElement();// graphml
      writer.writeEndDocument();

    } catch (XMLStreamException xse) {
      throw new IOException(xse);
    }
  }

  public void writeVertexKeyset(List<GraphmlKey> keyset) throws IOException {
    try {
      for (GraphmlKey key : keyset) {
        writer.writeStartElement(GraphMLTokens.KEY);
        writer.writeAttribute(GraphMLTokens.ID, key.getId());
        writer.writeAttribute(GraphMLTokens.FOR, GraphMLTokens.NODE);
        writer.writeAttribute(GraphMLTokens.ATTR_NAME, key.getName());
        writer.writeAttribute(GraphMLTokens.ATTR_TYPE, key.getType());
        writer.writeEndElement();
      }
    } catch (XMLStreamException xse) {
      throw new IOException(xse);
    }

  }

  public void writeEdgeKeyset(List<GraphmlKey> keyset) throws IOException {
    try {
      for (GraphmlKey key : keyset) {
        writer.writeStartElement(GraphMLTokens.KEY);
        writer.writeAttribute(GraphMLTokens.ID, key.getId());
        writer.writeAttribute(GraphMLTokens.FOR, GraphMLTokens.EDGE);
        writer.writeAttribute(GraphMLTokens.ATTR_NAME, key.getName());
        writer.writeAttribute(GraphMLTokens.ATTR_TYPE, key.getType());
        writer.writeEndElement();
      }
    } catch (XMLStreamException xse) {
      throw new IOException(xse);
    }
  }

  public void writeGraphStart() throws IOException {
    try {
      writer.writeStartElement(GraphMLTokens.GRAPH);
      writer.writeAttribute(GraphMLTokens.ID, GraphMLTokens.G);
      writer.writeAttribute(GraphMLTokens.EDGEDEFAULT, GraphMLTokens.DIRECTED);

    } catch (XMLStreamException xse) {
      throw new IOException(xse);
    }
  }

  public void writerNodes(Iterable<SimpleNode> nodes) throws IOException {
    try {
      for (SimpleNode node : nodes) {
        writer.writeStartElement(GraphMLTokens.NODE);
        writer.writeAttribute(GraphMLTokens.ID, node.getId().toString());

        writer.writeAttribute(GraphMLTokens.LABELS, node.getLabelsString());
        Set<String> keys = node.getPropertyKeys();

        // write label to data
        writeDataElement("label", node.getLabel());

        for (String key : keys) {
          Object value = node.getProperty(key);
          if (null != value) {
            writeDataElement(key, value);
          }
        }

        writer.writeEndElement();

      }
    } catch (XMLStreamException xse) {
      throw new IOException(xse);
    }


  }

  public void writeEdges(Iterable<SimpleEdge> edges) throws IOException {
    try {
      for (SimpleEdge edge : edges) {
        writer.writeStartElement(GraphMLTokens.EDGE);
        writer.writeAttribute(GraphMLTokens.ID, edge.getId().toString());
        writer.writeAttribute(GraphMLTokens.SOURCE, edge.getSourcenode());
        writer.writeAttribute(GraphMLTokens.TARGET, edge.getTargetnode());
        writer.writeAttribute(GraphMLTokens.LABEL, edge.getLabel());
        Set<String> keys = edge.getPropertyKeys();

        // write label to data
        writeDataElement("label", edge.getLabel());

        for (String key : keys) {
          Object value = edge.getProperty(key);
          if (null != value) {
            writeDataElement(key, value);
          }
        }

        writer.writeEndElement();

      }
    } catch (XMLStreamException xse) {
      throw new IOException(xse);
    }
  }

  public void writeDataElement(String key, Object value) throws IOException {
    try {
      writer.writeStartElement(GraphMLTokens.DATA);
      writer.writeAttribute(GraphMLTokens.KEY, key);
      writer.writeCharacters(value.toString());
      writer.writeEndElement();
    } catch (XMLStreamException xse) {
      throw new IOException(xse);
    }
  }

  public void flush() throws XMLStreamException {
    writer.flush();
  }

  public void close() throws XMLStreamException {
    writer.close();
  }

}
