package edu.ecnu.kg.common;

/** 
 * schema convert types
 * @author leyi  2014年5月18日
 * @version 0.0.5
 * @modify 
 * @Copyright 华东师范大学数据科学与工程研究院版权所有
 */
public class ConvertSchema {
   
  boolean isNode=false;
  
  boolean isAttr=false;
  
  boolean isNodes=false;
  
  int nodekey=0;
    
  
  String label;
  
  String type;
  
  boolean isUnique;

  
  
  public boolean isNode() {
    return isNode;
  }

  public void setNode(boolean isNode) {
    this.isNode = isNode;
  }

  public boolean isAttr() {
    return isAttr;
  }

  public void setAttr(boolean isAttr) {
    this.isAttr = isAttr;
  }

  public boolean isNodes() {
    return isNodes;
  }

  public void setNodes(boolean isNodes) {
    this.isNodes = isNodes;
  }

  public int getNodekey() {
    return nodekey;
  }

  public void setNodekey(int nodekey) {
    this.nodekey = nodekey;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public boolean isUnique() {
    return isUnique;
  }

  public void setUnique(boolean isUnique) {
    this.isUnique = isUnique;
  }
  
  

}
