package edu.ecnu.kg.common;

import edu.ecnu.kg.common.utils.DatabaseProperties;

public class DatabaseConfig {
  
  private static DatabaseProperties dp=new DatabaseProperties();
  
  private static String neo4jgraphpath;
  
  public static String getNeo4jGraphPath(){
    if (neo4jgraphpath == null) {
      String temp = dp.getStringProperty("neo4j.graph.db");
      if (temp == null || "".equals(temp)) {
        temp = "";
      }
      neo4jgraphpath = temp;
    }

    return neo4jgraphpath;
  }

}
