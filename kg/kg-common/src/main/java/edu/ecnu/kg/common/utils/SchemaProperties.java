package edu.ecnu.kg.common.utils;

public class SchemaProperties extends AbstractProperties {

  public SchemaProperties() {
    super();
  }

  @Override
  public String getPropertiesAlias() {
    return "schema-config";
  }

}
