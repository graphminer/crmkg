package edu.ecnu.kg.common;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


public class SimpleNode implements Serializable{
  
  private static final long serialVersionUID = 1L;

  String id;
  
  Set<String> labels=new HashSet<String>();
  
  //key-id : value
  Map<String,String> attributes=new HashMap<String,String>();
  
  public SimpleNode(){
    
  }

//  public SimpleNode(Node neoNode) {
//    for(Label label:neoNode.getLabels()){
//      labels.add(label.name());
//    }
//    id=String.valueOf(neoNode.getId());
//    for(String key : neoNode.getPropertyKeys()){
//      attributes.put(key, String.valueOf(neoNode.getProperty(key)));
//    }
//  }

  public void addProperty(String key, String value){
    attributes.put(key, value);
  }

  public Set<String> getPropertyKeys(){
    return attributes.keySet();
  }
  
  public String getProperty(String key){
    return attributes.get(key);
  }
  
  public String getId() {
    return id;
  }


  public void setId(String id) {
    this.id = id;
  }


  public Set<String> getLabel() {
    return labels;
  }


  public void setLabel(Set<String> label) {
    this.labels = label;
  }
  
  public void addLabel(String label){
    labels.add(label);
  }
  

  public Map<String, String> getAttributes() {
    return attributes;
  }


  public void setAttributes(Map<String, String> attributes) {
    this.attributes = attributes;
  }
  
 
  public String getLabelsString(){
    Iterator<String> it = labels.iterator();
    String labelstr="";
    if(it.hasNext()){
      return labelstr+":"+it.next();
    }
    return labelstr;
  }
}
