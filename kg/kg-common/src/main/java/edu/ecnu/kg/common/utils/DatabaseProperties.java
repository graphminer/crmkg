package edu.ecnu.kg.common.utils;

public class DatabaseProperties extends AbstractProperties{

  public DatabaseProperties() {
    super();
  }

  @Override
  public String getPropertiesAlias() {
    return "db-config";
  }

}
