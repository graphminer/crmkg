package edu.ecnu.kg.common;

import edu.ecnu.kg.common.utils.SchemaProperties;

/** 
 * Configuration of schema path
 * @author leyi  2014年5月18日
 * @version 0.0.5
 * @modify 
 * @Copyright 华东师范大学数据科学与工程研究院版权所有
 */
public class PathCfg {

  private static SchemaProperties sp = new SchemaProperties();

  private static String fusionschema;
  
  private static String convertschema;


  /**
   * 读取schema融合配置
   * @return shema file
   */
  public static String getFusionSchema() {
    if (fusionschema == null) {
      String tempschema = sp.getStringProperty("schema.fusion");
      if (tempschema == null || "".equals(tempschema)) {
        tempschema = "";
      }
      fusionschema = tempschema;
    }

    return fusionschema;
  }

  /**
   * 读取GraphML转换映射文件
   * @return shema file
   */
  public static String getConvertSchema(){
    if(convertschema==null){
      String tempschema = sp.getStringProperty("schema.convert");
      if (tempschema == null || "".equals(tempschema)) {
        tempschema = "";
      }
      convertschema=tempschema;
    }
    return convertschema;
  }

}
