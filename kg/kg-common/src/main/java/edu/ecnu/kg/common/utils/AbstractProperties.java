
package edu.ecnu.kg.common.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class AbstractProperties {

    private Properties props;
 
    public AbstractProperties() {
        super();
        setProperties();
    }

    /**
     * @return PropertiesAlias.
     */
    public abstract String getPropertiesAlias();

    /**
     * @return Properties Object.
     */
    public final synchronized Properties setProperties() {

        if (getPropertiesAlias() == null) {
            throw new RuntimeException("PROPERTIES_ALIAS not specified");
        }

        if (props == null) {

            props = new Properties();

//            InputStream input = Thread.currentThread().getContextClassLoader()
//                    .getResourceAsStream(getPropertiesAlias() + ".properties");
            
            try {
              InputStream input = new FileInputStream(getPropertiesAlias()+".properties");
                props.load(input);
                input.close();
            } catch (IOException ioe) {
                throw new RuntimeException("Error getProperties", ioe);
            }

            // LOG.info("[PropertiesObject.getProperties] PROPERTIES="
            // + props);
        }
        return props;
    }
    /**
     * @param key 
     * @return 
     */
    public final String getStringProperty(final String key) {
        String value = props.getProperty(key);
        if (value == null) {
            throw new Error("ERROR! cannot find [" + key + "] ");
        }
        return value.trim();
    }
    /**
     * @param key
     * @return 
     */
    public final boolean getBooleanProperty(final String key) {
        String value = props.getProperty(key);
        if (value == null) {
            throw new Error("ERROR! cannot find [" + key + "] ");
        }
        return value.trim().equalsIgnoreCase("true");
    }
    /**
     * @param key 
     * @return 
     */
    public final int getIntProperty(final String key) {
        String value = props.getProperty(key);
        if (value == null) {
            throw new Error("ERROR! cannot find [" + key + "] ");
        }
        return Integer.parseInt(value.trim());
    }
    
    /**
     * @param key 
     * @return ֵ
     */
    public final double getDoubleProperty(final String key) {
        String value = props.getProperty(key);
        if (value == null) {
            throw new Error("ERROR! cannot find [" + key + "] ");
        }
        return Double.parseDouble(value.trim());
    }

    /**
     * 
     */
    public final void reset() {
        props = null;
    }
}