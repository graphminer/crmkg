package edu.ecnu.kg.common;

public enum FusionRules {
    VOTE, MERGE, SPLIT_MERGE;
    
    public static FusionRules parse(String string) {
      if(string.toLowerCase().contains("vote")){
        return FusionRules.VOTE;
      }
      else if(string.toLowerCase().contains("s_m") || string.toLowerCase().contains("split_merge")){
        return FusionRules.SPLIT_MERGE;
      }
      else if(string.toLowerCase().contains("m") || string.toLowerCase().contains("merge")){
        return FusionRules.MERGE;
      }
      return FusionRules.VOTE;
    }
}
