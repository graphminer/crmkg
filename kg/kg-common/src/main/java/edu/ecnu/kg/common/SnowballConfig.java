package edu.ecnu.kg.common;

import edu.ecnu.kg.common.utils.SnowballProperties;

public class SnowballConfig {

	private static SnowballProperties sp = new SnowballProperties();

	private static String modelPath;
	private static String stopWordsPath;
	private static String indexPath;
	private static String docPath;
	private static String xmlPath;
	private static String seedPath;
	private static String entityCountPath;
	private static String entityCountFilterPath;
	private static String coEntityCountPath;
	private static String coEntityCountFilterPath;

									
    public static String getModelPath() {
		if (modelPath == null) {
			String temp = sp.getStringProperty("snowball.nlp.model");
			if (temp == null || "".equals(temp)) {
				temp = "";
			}
			modelPath = temp;
		}
		return modelPath;
	}

	public static String getStopWordsPath() {
		if (stopWordsPath == null) {
			String temp = sp.getStringProperty("snowball.filter.stopwords");
			if (temp == null || "".equals(temp)) {
				temp = "";
			}
			stopWordsPath = temp;
		}
		return stopWordsPath;
	}
	
	public static String getIndexPath() {
		if (indexPath == null) {
			String temp = sp.getStringProperty("snowball.lucene.index");
			if (temp == null || "".equals(temp)) {
				temp = "";
			}
			indexPath = temp;
		}
		return indexPath;
	}
	
	public static String getDocPath() {
		if (docPath == null) {
			String temp = sp.getStringProperty("snowball.lucene.doc");
			if (temp == null || "".equals(temp)) {
				temp = "";
			}
			docPath = temp;
		}
		return docPath;
	}
	
	public static String getXmlPath() {
		if (xmlPath == null) {
			String temp = sp.getStringProperty("snowball.xml.folder");
			if (temp == null || "".equals(temp)) {
				temp = "";
			}
			xmlPath = temp;
		}
		return xmlPath;
	}
	
	public static String getSeedPath() {
		if (seedPath == null) {
			String temp = sp.getStringProperty("snowball.bootstrap.seed");
			if (temp == null || "".equals(temp)) {
				temp = "";
			}
			seedPath = temp;
		}
		return seedPath;
	}
	
	public static String getEntityCountPath() {
		if (entityCountPath == null) {
			String temp = sp.getStringProperty("snowball.hadoop.entitycount");
			if (temp == null || "".equals(temp)) {
				temp = "";
			}
			entityCountPath = temp;
		}
		return entityCountPath;
	}
	
	public static String getEntityCountFilterPath() {
		if (entityCountFilterPath == null) {
			String temp = sp.getStringProperty("snowball.hadoop.entitycount.filter");
			if (temp == null || "".equals(temp)) {
				temp = "";
			}
			entityCountFilterPath = temp;
		}
		return entityCountFilterPath;
	}
	
	public static String getCoEntityCountPath() {
		if (coEntityCountPath == null) {
			String temp = sp.getStringProperty("snowball.hadoop.coentitycount");
			if (temp == null || "".equals(temp)) {
				temp = "";
			}
			coEntityCountPath = temp;
		}
		return coEntityCountPath;
	}
	
	public static String getCoEntityCountFilterPath() {
		if (coEntityCountFilterPath == null) {
			String temp = sp.getStringProperty("snowball.hadoop.coentitycount.filter");
			if (temp == null || "".equals(temp)) {
				temp = "";
			}
			coEntityCountFilterPath = temp;
		}
		return coEntityCountFilterPath; 
	}
	
}
