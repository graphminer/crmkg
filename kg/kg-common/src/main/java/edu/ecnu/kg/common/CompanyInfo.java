package edu.ecnu.kg.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/** 
 * Information for one company before node fusion
 * @author leyi  2014年5月21日
 * @version 0.0.5
 * @modify 
 * @Copyright 华东师范大学数据科学与工程研究院版权所有
 */
public class CompanyInfo implements Serializable{
  
  private static final long serialVersionUID = 1L;
  
  public final static String COMPANYKEY="companyname";
  //company id
  String id;
  
  //<schema name, values from multiple datasources>
  HashMap<String,ArrayList<String>> attributes;
  
  public CompanyInfo(){
    
  }
  
  public CompanyInfo(String id){
    this.id=id;
  }
  
  public void put(String schema, String value, int count){
    if(attributes==null){
      attributes=new HashMap<String,ArrayList<String>>();
    }
    ArrayList<String> values=attributes.get(schema);
    if(values==null){
      values=new ArrayList<String>();
    }
    if(values.size()>=count){
      return;
    }
    values.add(value);
    attributes.put(schema, values);  
  }
  
  public void setID(String id){
    this.id=id;
  }

  public void putMap(HashMap<String, String> tempcom, int count) {
    //
    Iterator<Entry<String, String>> it=tempcom.entrySet().iterator();
    while(it.hasNext()){
      Map.Entry<String, String> entry=it.next();
      put(entry.getKey(),entry.getValue(),count);
    }
    
  }
  
  public HashMap<String,ArrayList<String>> getAttributes(){
    return attributes;
  }

}
