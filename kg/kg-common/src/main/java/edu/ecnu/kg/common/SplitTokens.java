package edu.ecnu.kg.common;

public class SplitTokens {
  public static final String LABEL_SPLIT = "##";
  public static final String SOURCE_SPLIT = ",";
  public static final String WEIGHT_SPLIT = ";";
  public static final String VALUE_SPLIT = "&&";
  public static final String COLUMN_SPLIT="\t";
  public static final String COMMA_SPLIT=",";
}
