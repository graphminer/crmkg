package edu.ecnu.kg.common.graphml;

import java.io.Serializable;

/**  
 * @author leyi  2014年5月18日
 * 
 */
public class GraphmlKey implements Serializable{

  private static final long serialVersionUID = 1L;

  String id;
  
  String name;
  
  String type;


  public String getId() {
    return id;
  }


  public void setId(String id) {
    this.id = id;
  }


  public String getName() {
    return name;
  }


  public void setName(String name) {
    this.name = name;
  }


  public String getType() {
    return type;
  }


  public void setType(String type) {
    this.type = type;
  }
  
  

}
