package edu.ecnu.kg.common;

import java.util.ArrayList;

public enum DataSource {
  PUBLIC, IFENG, NTES, SZSE, YP88, WORLDCOMPANY, ALIBABA, OTHER;

  public static DataSource parse(String string) {
    if(string.toLowerCase().contains("public")){
      return DataSource.PUBLIC;
    }
    else if(string.toLowerCase().contains("alibaba")){
      return DataSource.ALIBABA;
    }
    else if(string.toLowerCase().contains("worldcompany")){
      return DataSource.WORLDCOMPANY;
    }
    else if(string.toLowerCase().contains("ifeng")){
      return DataSource.IFENG;
    }
    else if(string.toLowerCase().contains("yp88")){
      return DataSource.YP88;
    }
    else if(string.toLowerCase().contains("ntes")){
      return DataSource.NTES;
    }
    else if(string.toLowerCase().contains("szse")){
      return DataSource.SZSE;
    }
    return DataSource.OTHER;
  }

  public static DataSource[] parse(String[] split) {
    ArrayList<DataSource> ds=new ArrayList<DataSource>();
    for(String i : split){
      DataSource j=parse(i);
      if(DataSource.OTHER != j){
        ds.add(j);
      }
    }
    DataSource[] dss=new DataSource[ds.size()];
    for(int k=0;k<ds.size();k++){
      dss[k]=ds.get(k);
    }
    return dss;
  }
  
  public double getWeight(){
    if(this.equals(DataSource.PUBLIC) || this.equals(DataSource.ALIBABA)){
      return 0.5;
    }else if(this.equals(DataSource.WORLDCOMPANY) || this.equals(DataSource.IFENG) || this.equals(DataSource.NTES)){
      return 0.3;
    }
    return 0.1;
  }
}
