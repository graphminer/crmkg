package edu.ecnu.kg.common;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Set;

public class SimpleEdge implements Serializable{

  private static final long serialVersionUID = 1L;
  String id;
  String sourcenode;
  String targetnode;
  String label;
  HashMap<String,String> attributes=new HashMap<String,String>();

  public Set<String> getPropertyKeys(){
    return attributes.keySet();
  }
  
  public String getProperty(String key){
    return attributes.get(key);
  }
  
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getSourcenode() {
    return sourcenode;
  }

  public void setSourcenode(String sourcenode) {
    this.sourcenode = sourcenode;
  }

  public String getTargetnode() {
    return targetnode;
  }

  public void setTargetnode(String targetnode) {
    this.targetnode = targetnode;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public HashMap<String, String> getAttributes() {
    return attributes;
  }

  public void setAttributes(HashMap<String, String> attributes) {
    this.attributes = attributes;
  }
  
  public void addProperty(String key, String value){
    attributes.put(key, value);
  }

  
  
}
