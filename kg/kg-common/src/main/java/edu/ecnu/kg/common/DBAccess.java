package edu.ecnu.kg.common;

/** 
 * Database access methods
 * @author leyi  2014年6月8日
 * @version 0.0.5
 * @modify 
 * @Copyright 华东师范大学数据科学与工程研究院版权所有
 */
public enum DBAccess {

  EMBEDED, REST, EMBEDED_READONLY;
}
